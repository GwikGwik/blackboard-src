# blackboard-src

web site https://blackboard.anadenn.fr

## installation

cloner le code source :

    git clone https://framagit.org/GwikGwik/blackboard-src.git /path/to/program

installer les dépendences :

    cd /path/to/program
    bash ./install.sh

## configuration

pour utiliser le programme, il faut mettre à jour les variables d'environnement
grace à un script :

    #indiquer le chemin du répertoire au script
    cd /path/to/program

    #importer les variables dans le terminal
    source ./settings.sh

## avoir de l'aide

pour accéder à l'aide, taper dans la console :

    @help


## developpement

voir les objectifs de chaque version dans [la todo list](./TODO.md)



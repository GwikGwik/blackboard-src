//========================================================

class LinkStore extends Store  {

//========================================================

    //----------------------------------------------------
    constructor(...args){
    //----------------------------------------------------

        super(...args);
        this.STORE=LinkStore;
    }
    //----------------------------------------------------
    objects(){
    //----------------------------------------------------
        var result=new Store();

        for(var i in this.elements) {
            result.append(this.elements[i].object);
        }
        return result;
    }
    //----------------------------------------------------
    subjects(){
    //----------------------------------------------------
        var result=new Store();

        for(var i in this.elements) {

                result.append(link.subject);
        }
        return result;
    }
    //----------------------------------------------------
    objects_by_tag(tag){
    //----------------------------------------------------
        var result=new Store();

        for(var i in this.elements) {
            if(this.elements[i].tag==tag){
                result.append(this.elements[i].object);
            }
        }
        return result;
    }
    //----------------------------------------------------
    subjects_by_tag(tag){
    //----------------------------------------------------
        var result=new Store();

        for(var i in this.elements) {
            if(this.elements[i].tag==tag){
                result.append(link.subject);
            }
        }
        return result;
    }
    //----------------------------------------------------
}
//========================================================

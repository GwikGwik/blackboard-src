var COUNT=0;

//========================================================

class Base {

//========================================================
/*

*/

    //----------------------------------------------------
    constructor(name){
    //----------------------------------------------------
        this.msg("NEW",{},1);
        if( name) {
            this.name = name;
        }else{
            this.name = "NODE_"+COUNT;
            COUNT=COUNT+1;
        }

    }
    //----------------------------------------------------
    destroy(){
    //----------------------------------------------------
        this.msg("DESTROY",{},1);
    }
    //----------------------------------------------------
    getClass(){
    //----------------------------------------------------
        return this.constructor.name;
    }
    //----------------------------------------------------
    msg(tag,data,level=9){
    //----------------------------------------------------
        if ( DEBUG && level > DEBUG_LEVEL ) {
            var sep="  -  ";
            var l0=15;
            var string=tag;
            if (string.length>l0){
                string=string.slice(0,l0);
            }else{
                string=string+" ".repeat(l0-string.length);
            }
            string=sep+string+sep;
            console.log(level,string,this.getClass(),this.path(),data);
        }

    }
    //----------------------------------------------------
    tree(level=1){
    //----------------------------------------------------

            var sep="   | ";
   
            var string=sep.repeat(level)+this.getClass()+" - "+this.name;
            console.log(string);
            this.children.execute('tree',level+1)


    }
    //----------------------------------------------------

}
//========================================================

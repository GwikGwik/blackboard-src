
function import_js(url){
    js_list.push(url);
};

function do_js(){
    //console.log(js_i,js_list.length);
    if(js_i<js_list.length){
        var url=js_list[js_i];
        //console.log("LOAD",url);
        var elt=jQuery('<script />', {
            type:"text/javascript",
            src: ip+url,
        });
        elt.ready(do_js);
        elt.appendTo("head");

        //console.log("LOAD DONE",url);
        js_i+=1;

    };
};

function import_css(url){

    jQuery('<link/>', {
        href: ip+url,
        rel: "stylesheet",
    }).appendTo("head");
};

function import_all(){

    //import_js("/__js/settings.js");
    import_js("/__static/externals/w3.js");
    import_css("/__static/externals/w3.css");
    import_css("/__static/main.css");
    import_css("/__static/externals/fontawesome-free-5.11.2.min.css");
    import_js("/__static/externals/fontawesome-free-5.11.2.min.js");
    import_js("/__static/externals/vis.min.js");
    import_css("/__static/externals/vis.min.css");

    import_js("http://cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js");
    import_js("/__static/externals/vis-graph3d.min.js");

    import_js("/__static/externals/vis-network.min.js");
    import_css("/__static/externals/vis-network.min.css");

    import_js("/__static/externals/vis-timeline-graph2d.min.js");
    import_css("/__static/externals/vis-timeline-graph2d.min.css");


    //abstract classes
    //==============================================

    import_js("/__js/core/xml.js");
    import_js("/__js/core/Base.js");
    import_js("/__js/core/Store.js");

    import_js("/__js/core/Atom.js");
    import_js("/__js/core/Link.js");
    import_js("/__js/core/Query.js");
    import_js("/__js/core/LinkStore.js");

    //root classes
    //==============================================

    import_js("/__js/base/Bd_Object.js");

    import_js("/__js/base/Bd_Data.js");
    import_js("/__js/base/Bd_Action.js");

    import_js("/__js/base/Bd_Main.js");
    import_js("/__js/base/Task.js");
    import_js("/__js/base/LoadFile.js");

    import_js("/__js/scripts/Do.js");
    import_js("/__js/scripts/ScriptNode.js");
    import_js("/__js/scripts/Script.js");
    import_js("/__js/scripts/Actions.js");
    import_js("/__js/scripts/Event_Handler.js");

    //main
    //==============================================

    import_js("/__js/Scene/SceneElement.js");
    import_js("/__js/Scene/SceneGroup.js");
    import_js("/__js/Scene/Scene.js");


    import_js("/__js/Scene/node/Call_Scene.js");
    import_js("/__js/Scene/node/ForChildren.js");
    import_js("/__js/Scene/node/SelectFirst.js");
    import_js("/__js/Scene/node/SelectPath.js");
    import_js("/__js/Scene/node/SelectXml.js");
    import_js("/__js/Scene/node/SelectXmlScript.js");
    import_js("/__js/Scene/node/TestAttr.js");
    import_js("/__js/Scene/node/Sequence.js");
    import_js("/__js/Scene/node/SelectLocal.js");

    import_js("/__js/Scene/html/HtmlHandler.js");
    import_js("/__js/Scene/html/Draw.js");
    import_js("/__js/Scene/html/HtmlAnimate.js");
    import_js("/__js/Scene/html/HtmlClass.js");
    import_js("/__js/Scene/html/HtmlCss.js");
    import_js("/__js/Scene/html/HtmlEffect.js");
    import_js("/__js/Scene/html/HtmlMethod.js");
    import_js("/__js/Scene/html/SelectPart.js");
    import_js("/__js/Scene/html/ExecHtml.js");


    import_js("/__js/SceneItems/Div.js");
    import_js("/__js/SceneItems/Part.js");
    import_js("/__js/SceneItems/Text.js");
    import_js("/__js/SceneItems/TextAttr.js");
    import_js("/__js/SceneItems/Media.js");
    import_js("/__js/SceneItems/Icon.js");
    import_js("/__js/SceneItems/Icon_Awesome.js");
    import_js("/__js/SceneItems/Icon_Text.js");
    import_js("/__js/SceneItems/Icon_Image.js");
    import_js("/__js/SceneItems/Layers.js");
    import_js("/__js/SceneItems/Layer.js");
    import_js("/__js/SceneItems/Slideshow.js");
    import_js("/__js/SceneItems/Curve.js");
    import_js("/__js/SceneItems/Curve3d.js");

    import_js("/__js/admin/SceneBar.js");
/*
    //css
    //==============================================
*/
//
};






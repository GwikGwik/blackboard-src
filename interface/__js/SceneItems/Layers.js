//========================================================

class Layers extends Div {

//========================================================
    //----------------------------------------------------
    onSetup(){
    //----------------------------------------------------

        super.onSetup();
        this.args["class"]=this.args["class"]+" w3-container";

    }
    //----------------------------------------------------

    onDraw(div,selected){

    //----------------------------------------------------

        var elt= this.get_html("div",this.args,null,div);
        elt.css("width","100%");
        elt= this.get_html("div",{class:"w3-display-container"},null,elt);//
        elt.css("width","100%");
        return elt;

    }
   //----------------------------------------------------

    activate(div,selected){

    //----------------------------------------------------

        var lst=this.children.by_class(Layer).iter();
        this.msg("LAYER_ACTIVATE",lst,5);
         for(var i in lst ) {
            lst[i].children.by_class(Do).by_attr_value("event","init").execute("make",{div:lst[i].obj,selected:selected});
        }

        div.on("click",{div:div,selected:selected},this.onClick.bind(this));
        div.on("mouseenter",{div:div,selected:selected},this.onMouseEnter.bind(this));
        div.on("mouseleave",{div:div,selected:selected},this.onMouseLeave.bind(this));
     }
    //----------------------------------------------------

    onClick(event){
    //----------------------------------------------------
        this.msg("LAYER_CLICK",event.data,5);

        var lst=this.children.by_class(Layer).iter();
         for(var i in lst ) {
            lst[i].executeEvent("click",this.onClickDone, {div:lst[i].obj,selected:event.data.selected} );
        }

    }
    //----------------------------------------------------

    onMouseEnter(event){
    //----------------------------------------------------
        this.msg("LAYER_ENTER",event.data,5);
        var lst=this.children.by_class(Layer).iter();
         for(var i in lst ) {
            lst[i].executeEvent("enter",this.onClickDone, {div:lst[i].obj,selected:event.data.selected} );
        }
    }
    //----------------------------------------------------

    onMouseLeave(event){
    //----------------------------------------------------
        this.msg("LAYER_LEAVE",event.data,5);
        var lst=this.children.by_class(Layer).iter();
         for(var i in lst ) {
            lst[i].executeEvent("leave",this.onClickDone, {div:lst[i].obj,selected:event.data.selected} );
        }
    }
    //----------------------------------------------------



}
//========================================================



//========================================================

class Script extends ScriptNode {

//========================================================
    //----------------------------------------------------
    onSetup(data){
    //----------------------------------------------------
        this.msg("SCRIPT_SETUP",{});
        this.add_variable("exec",data,"__main__");
        this.add_variable("select",null);
        this.add_variable("tag",null);
    }
    //----------------------------------------------------

    onSetupDone(){

    //----------------------------------------------------

        this.executeEvent("setup",this.onSetupEventDone,{},true);
    }
    //----------------------------------------------------

    onSetupEventDone(){

    //----------------------------------------------------
        this.setup();
        //this.make();

    }
    //----------------------------------------------------
    onMake(){
    //----------------------------------------------------

        var node=this.search_path("__main__");
        console.log(node);
        if(node !=null){
            this.msg("SCRIPT_MAKE",{'node':node.path()});
            node.make()

        /*
            //node.make();
            var selected=null;

            if(this.select!=null){
                selected=this.search_path(this.select);
            }else{
                selected=this.root().content;
            }
            var div=$(this.tag);
            console.log(node,div,selected);
            node.draw(div,selected);*/
        }else{
            console.log(this.path(),"NO MAIN TO START");
        }


    }
    //----------------------------------------------------
    onMakeCall(selected){
    //----------------------------------------------------
        var node=this.search_path("__main__");
        //node.executeEvent("call",this.onMakeDone.bind(this));
        node.make(selected);
    }
    //----------------------------------------------------
    onMakeDone(data){
    //----------------------------------------------------
        this.msg("SCRIPT_DONE",data);
        this.executeEvent("cleanup",this.onCleanupDone,{},true);
    }
    //----------------------------------------------------
    onCleanupDone(){
    //----------------------------------------------------

    }
    //----------------------------------------------------
    call_part(name){
    //----------------------------------------------------

    return this.search_path(name).make();

    }
    //----------------------------------------------------

}
//========================================================




//========================================================

class Call_Scene extends SceneElement {

//========================================================
    //----------------------------------------------------
    onSetup(data){
    //----------------------------------------------------
        this.add_variable("part",null);
        this.add_variable("scene");
        this.add_variable("select");
    }

    //----------------------------------------------------

    onDraw(div,selected){

    //----------------------------------------------------
        var parent;
        if(this.part){
            var parent=$("#"+this.part);
        }else{
            var parent=div;
        }
        if(this.select){
            var node=selected.search_path(this.select);
            if (node==null){console.log("error path",this.select);}
        }else{
            var node=selected;
        }
        $("#"+this.part).empty();
        //console.log("Call_Scene",this.scene,selected.name,parent);
        this.drawScene(this.scene,parent,node);

    }
    //----------------------------------------------------
}
//========================================================



//========================================================

class HtmlHandler extends Do {

//========================================================
    //----------------------------------------------------
    onSetup(data){
    //----------------------------------------------------
        super.onSetup(data);
        this.add_variable("part");
    }

    //----------------------------------------------------

    onMake(data){

    //----------------------------------------------------

        this.msg("HtmlHandler",data,6);
        var parent=null;
        var selected=null;

        if(this.part!=null){
            parent=$("#"+this.part);
            selected=data.selected;
        }else{
            parent=data.div;
            selected=data.selected;
        }

        this.onDraw(parent,selected);

    }

    //----------------------------------------------------
    onDraw(div,selected){

    }
    //----------------------------------------------------
}
//========================================================


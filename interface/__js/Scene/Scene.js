//========================================================

class Scene extends SceneNode {

//========================================================

    //----------------------------------------------------
    onDraw(div,selected){

    //----------------------------------------------------
        return div;
    }
    //----------------------------------------------------
    drawScene(name,div,selected){
    //----------------------------------------------------

        var node=this.parent.search_path(name);
        if(node){
            return node.draw(div,selected);
        }else{
            console.log("draw not found",name,selected,div);
        }

    }
    //----------------------------------------------------

}

//========================================================

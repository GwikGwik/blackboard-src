//========================================================

class Bd_Action extends Bd_Object {

//========================================================


    //----------------------------------------------------
    make(data){
    //----------------------------------------------------

        this.msg("MAKE",data,5);
        return this.onMake(data);
        this.msg("MAKE_DONE",data,5);

    }
    //----------------------------------------------------
    onMake(data){
    //----------------------------------------------------
        return;

    }
    //----------------------------------------------------
    executeEvent(event,callback,data,r=false){
    //----------------------------------------------------
        this.msg("EVENT",{'event':event},4);
        var node = new Event_Handler(this,event,callback.bind(this),data,r);
        node.start();
        return node;
    }
    //----------------------------------------------------
    LoadFile(url,callback,format=null,dt=null,data=null){
    //----------------------------------------------------
        this.msg("EVENT",{'url':url},4);
        var node = new LoadFile(this,callback.bind(this),url,format,dt,data=data);
        node.start();
        return node;
    }
    //----------------------------------------------------
}
//========================================================


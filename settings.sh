#!/bin/bash

export BLACKBOARD_SRC="$(realpath .)"
echo "BLACKBOARD_SRC $BLACKBOARD_SRC"

for lib in $(find "$BLACKBOARD_SRC" -mindepth 1 -type d -name "__bin" | sort -V)
do
    echo "PATH $lib"
    export PATH="$lib:$PATH"

    for file in $(find "$lib" -mindepth 1 -maxdepth 1 -type f | sort -V)
    do
        echo "PROGRAM $file"
        chmod +x $file
    done
done

for lib in $(find "$BLACKBOARD_SRC" -mindepth 1 -type d -name "__python3" | sort -V)
do
    echo "PYTHONPATH $lib"
    export PYTHONPATH="$lib:$PYTHONPATH"
done



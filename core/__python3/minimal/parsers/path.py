from minimal.parser import Parser
#===============================================================================

class PathParser(Parser):

#===============================================================================


    #-----------------------------------------------------------  

    def __init__(self,separator="/",reverse=False,**args):

        self.separator=separator
        self.reverse=reverse

        Parser.__init__(self)

    #-----------------------------------------------------------

    def parseKey(self,path):

        path_list=path.split(self.separator)


        if self.reverse==True:
            path_list.reverse()

        path = ""

        for elt in path_list:

            if elt not in ["",self.separator]:
                if self.reverse ==True:
                    path=elt+self.separator+path
                else:
                    path=path+elt+self.separator
                yield elt,path[:-1]



    #-----------------------------------------------------------

    def getKey(self,elt):


        nodes=[]
        nodes.append(elt)
        for root in elt.ancestors:
            if root != self.parent:
                nodes.append(root)
            else:
                break


        key=u""

        if self.reverse==True:

            for elt in nodes:
                key=key+self.separator+elt.name
        else:

            for elt in nodes:
                key=elt.name+self.separator+key

            key=key[:-1]

        return key

    #-----------------------------------------------------------


#===============================================================================


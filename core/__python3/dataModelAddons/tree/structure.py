# -*- coding: utf-8 -*-
from .model import Tree
from dataModelAddons.container import Container
from minimal.parsers.path import PathParser
#=======================================================================

class Tree_Builder(Container):

#=======================================================================

    PARSER=PathParser
    #---------------------------------------------------
    # DOCUMENTATION
    #---------------------------------------------------

    """
    conteneur de noeuds Tree.
    L'arbre Tree est un dictionnaire de chemin ::

       tree=Tree()
       tree['a']=dict()
       node=node['a']

       tree.keys()
       tree.values()
       tree.items()


    le noeud se comporte comme un dictionnaire de données.
    on combine les comportements de Node et Tree pour avoir accès aux données ::

       tree["a/b/c"].keys()
       tree["a/b/c"].values()
       tree["a/b/c"].items()


    """
    #---------------------------------------------------
    # INIT
    #---------------------------------------------------

    def __init__(self,parent=None,
                        Class=Tree,
                        build=True,
                        parser=None,
                        **args):

        """
        creer un arbre avec Tree ::

           tree=Tree()
           tree=Tree(separator=':')
           tree=Tree(separator='.',reverse=True)

        les parametres :
           - parent : assigner un noeud existant à la racine
           - Class : classe pour construire les chemins (forcement un Tree)
           - build = True : construit le chemin sinon Exception
           - parser : changer de parser
        """

        self.parent=parent
        self.Class=Class
        self.build=build
        self.parser=parser


        if self.parent is None:
            self.parent=Class()#**args)

        if self.parser is None:
            self.parser=self.PARSER(Class=Class,**args)

        self.parser.init(parent=self.parent,Class=self.Class)


    #---------------------------------------------------
    # PATH DICT
    #---------------------------------------------------

    def keys(self):

        """ liste les chemins """


        for elt in self.parent.all():
            yield self.parser.getKey(elt)

    #---------------------------------------------------

    def values(self):

        """ liste noeuds """
        return self.parent.all()

    #---------------------------------------------------

    def items(self):

        """ liste des tuples (chemins,noeud) """

        for elt in self.parent.all():
            yield self.parser.getKey(elt),elt


    #-----------------------------------------------------------

    def __contains__(self,path):

        try:
            node=self.parser.getElement(path,build=False)
            return True

        except:
            return False

    #-----------------------------------------------------------


    def __getitem__(self,key):

        """ 
        retourne le noeud correspondant au chemin.
        pour acceder au noeud, on recherche son chemin ::

           node = tree["a/b/c"]

        lire une donnée ::

           data = tree["a/b/c"]["a"]
        """


        return self.parser.getElement(key,build=self.build)


    #---------------------------------------------------

    def __setitem__(self,key,value):

        """ 
        creer le noeud correspondant au chemin key avec le dictionnaire value
        on cree un nouveau noeud avec son chemin
        et un dictionnaire de données ::

           tree["a/b/c"] = dict(a=1)

        mettre à jour le dictionnaire ::

           tree["a/b/c"] = dict(a=2,b=3)

        modifier une donnée ::

           tree["a/b/c"]["a"] = 2
        """
        if self.build == False:
            raise Exception("can't build %s, build variable to False"%path)

        if type(value)==dict:
            node=self.parser.getElement(key,build=self.build,data=value)
        elif isinstance(value,Tree):
            node=self.parser.getElement(key,build=self.build,data=dict())
            value.parent=node

    #---------------------------------------------------

    def __delitem__(self,key):

        """ 
        pour supprimer un noeud ::

           del tree["a/b/c"]
         """

        elt=self[key]
        elt.destroy()

    #---------------------------------------------------

#=======================================================================








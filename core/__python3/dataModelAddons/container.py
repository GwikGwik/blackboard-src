# -*- coding: utf-8 -*-



#=======================================================================

class Container(object):

#=======================================================================


    #---------------------------------------------------
    # DOCUMENTATION
    #---------------------------------------------------

    """
    classe abstraite

    conteneur de noeuds. permet de structurer un groupe de noeud
    La structure est un dictionnaire de coordonnées ::

       structure=Structure()
       structure['a']=dict()
       node=node['a']

       structure.keys()
       structure.values()
       structure.items()


    le noeud se comporte comme un dictionnaire de données.
    on combine les comportements de Node et Structure pour avoir accès aux données ::

       structure["coord"].keys()
       structure["coord"].values()
       structure["coord"].items()


    """
    #---------------------------------------------------
    # INIT
    #---------------------------------------------------

    def __init__(self,**args):
        """
        creer un arbre avec structure ::

           from pynode import Structure
           structure=structure()
        """
        pass

    #---------------------------------------------------
    # PATH DICT
    #---------------------------------------------------

    def keys(self):

        """ liste les chemins """
        pass

    #---------------------------------------------------

    def values(self):

        """ liste noeuds """
        pass

    #---------------------------------------------------

    def items(self):

        """ liste des tuples (chemins,noeud) """
        pass

    #---------------------------------------------------

    def __getitem__(self,key):

        """ 
        retourne le noeud correspondant au chemin.
        pour acceder au noeud, on recherche son chemin ::

           node = structure["coord"]

        lire une donnée ::

           data = structure["coord"]["a"]
        """
        pass

    #---------------------------------------------------

    def __setitem__(self,key,value):

        """ 
        creer le noeud correspondant au chemin key avec le dictionnaire value
        on cree un nouveau noeud avec son chemin
        et un dictionnaire de données ::

           structure["coord"] = dict(a=1)

        mettre à jour le dictionnaire ::

           structure["coord"] = dict(a=2,b=3)

        modifier une donnée ::

           structure["coord"]["a"] = 2
        """
        pass
    #---------------------------------------------------

    def __delitem__(self,key,value):

        """ 
        pour supprimer un noeud ::

           del structure["coord"]
         """
        pass

    #---------------------------------------------------

    def __contains__(self,key):

        """ 
       
        """
        pass

    #-----------------------------------------------------------

    def valueList(self,*args):

        for elt in args:
            yield self[elt]


    #---------------------------------------------------

#=======================================================================


# -*- coding: utf-8 -*-




from dataModel import Model,Frame




#==========================================================

def network(Frame=Frame,Class=Model):
    
#==========================================================

            
    def decorated(func):

        def wrapper(*args, **kwargs):

            #---------------------------------------------------

            store=Frame()

            for s,o,p in func(*args,**kwargs):

                store.append(Class(s=s,o=o,p=p))
                
            return store

            #---------------------------------------------------    
    
        return wrapper

    return decorated

#==========================================================


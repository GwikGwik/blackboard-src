from .fields import Field


#=============================================================================

class ModelField(Field):

#=============================================================================

    #--------------------------------------------------------------------------

    def get_value(self,value):
        from .model import getClass
        return getClass(value)
    #--------------------------------------------------------------------------
#=============================================================================

class NodeField(Field):

    CLASS=None

class NodeStore(Field):

    CLASS=None

#=============================================================================

class String(Field):

#=============================================================================
    CLASS=str


    #--------------------------------------------------------------------------
#=============================================================================
class Integer(Field):

    CLASS=int

class Float(Field):

    CLASS=float

class Boolean(Field):

    CLASS=bool

class Path(Field):

    CLASS=str




class Url(Field):

    CLASS=str

class Uri(Field):

    CLASS=str


class Time(Field):

    CLASS=float


class Date(Field):

    CLASS=str

class PythonClass(Field):

    CLASS=str

class List(Field):

    CLASS=list

    def get_value(self,value):
        #print("value",value)
        #exit()
        if value is None:
            return self.CLASS()

        if type(value)==str:
            lst=value.split(",")
            if len(lst)>1:
                return st
            else:
                 return [value,]





#=============================================================================


# -*- coding: utf-8 -*-
"""
    dans l'esprit ORM sqlalchemy et django 

    ajouter des modèles aux noeuds de l'arbre :
       - modifier dynamiquement les attributs et leur valeur, grouper les attributs
       - creer un systeme d'évenement/action évolutif sur les objets


"""
#import inspect
#from .frame import Frame
from .functions import Function
from .behaviors import *
from .frame import *
#=============================================================================
#                Manage_Functions,
class Model(Base_Node,Manage_Model,
                metaclass=ModelMeta):

#=============================================================================
    #python2 : __metaclass__ = ModelMeta
    """
    la classe Model est la classe concrète dont chaque structure doit
    hériter pour utiliser le système.

     elle permet de construire des types de données ::

       class SomeModel(Model):
            A=Field(int)
            B=Field(str)

    elle gère la création des instances.

    """
    #TODO:couille avec function et args
    #UPDATE=Function()
    DESTROY=Function()

    #--------------------------------------------------------------------------

    def __init__(self,**args):

        """
        pour toutes les bases de la classe réelle, rechercher des champs
        et création du dictionnaire d'attibut.
        """



        #liste des champs pour le modele
        fields=dict( self.__class__.getFields() )
        functions=dict( self.__class__.getFunctions() )

        Base_Node.__init__(self)

        #creer les attibuts
        for k,v in fields.items():

            v.new_attribute(self)

        #creer les fonctions
        for k,v in functions.items():
            
            v.setFunction(self)


        #assigner les valeurs
        for k,v in args.items():
            self[k]=v

        self.onInit()


    #--------------------------------------------------------------------------
    def onInit(self):
        "do nothing"
        pass


    #--------------------------------------------------------------------------

    def destroy(self):
        for mod in self.models():
            if mod != Model and hasattr(mod,"onDestroy"):
                getattr(mod,"onDestroy")(self)
        Base_Node.destroy(self)

    #--------------------------------------------------------------------------
    def getAbstracts(self):
        for k,v in ALL_CLASSES.items():
            if type(v)==ModelMeta and not issubclass(v,Model):
                yield v
    #--------------------------------------------------------------------------
    def is_instance(self,cls,strict=False):
        if strict==True:
            return self.__class__==cls

        try:
            return isinstance(self,self.getClass(cls))
        except:
            #print("Model : no class",cls)
            return

    #--------------------------------------------------------------------------
    def getClass(self,cls):
        return getClass(cls)

    #--------------------------------------------------------------------------
    def getClasses(self,cls=None):

        if cls is not None:
            cls=self.getClass(cls)
            d=dict()
            for k,v in ALL_CLASSES.items():
                if issubclass(v,cls):
                    d[k]=v
            return d
        else:
            return ALL_CLASSES

    #--------------------------------------------------------------------------

    def validate(self):
        """
        valider les valeurs assignées aux champs
        """

        for k,v in self.attributes():
            if v.validate() == False:
                return False
        return True

    #--------------------------------------------------------------------------

    def check_errors(self):
        """
        valider les valeurs assignées aux champs
        """

        for k,v in self.attributes():
            if v.validate() == False:
                yield k,v

    #--------------------------------------------------------------------------

    def attributes(self):
        """
        lister les attributs de l'objet
        """

        for k,v in list(Base_Node.items(self)):

            if isinstance(v,Attribute):
                yield k,v

    #--------------------------------------------------------------------------

    def getNonAttributeDict(self):
        """
        lister les attributs de l'objet
        """

        for k,v in list(Base_Node.items(self)):
            if not isinstance(v,Attribute):
                yield k,v
    #--------------------------------------------------------------------------

    def getRecordableDict(self):
        """
        lister les attributs de l'objet
        """

        for k,v in list(Base_Node.items(self)):
            if isinstance(v,Attribute) and v.field.record == True:
                yield k,v.access()
            if not isinstance(v,Attribute) and not isinstance(v,Frame):
                yield k,v
    #--------------------------------------------------------------------------

    def __setattr__(self, key, value):

        """
        on assigne une valeur à l'attribut ::

           self.attr=2
        """


        if "_Base_Node__data" in self.__dict__.keys():
            data=self.__dict__["_Base_Node__data"]

            if key in data.keys():
                if isinstance(data[key],Attribute):
                    data[key].modify(value)
                    return
                else:
                    data[key]=value
                    return

        object.__setattr__(self,key, value)



    #--------------------------------------------------------------------------

    def __getattr__(self, key):

        """
        on recupère la valeur de l'attribut ::

           value=self.attr
        """

        if "_Base_Node__data" in self.__dict__.keys():
            data=self.__dict__["_Base_Node__data"]

            if key in data.keys():
                value = self.__dict__["_Base_Node__data"][key]
                if isinstance(value,Attribute):
                    return value.access()
                else:
                    return value

        return self.__getattribute__(key)




    #--------------------------------------------------------------------------

    def __delattr__(self, key):

        """
        on assigne une valeur à l'attribut ::

           del self.attr
        """

        try:
            Base_Node.__delattr__(self,key)
        except:

            del self[key]

    #--------------------------------------------------------------------------
    def values(self):

        """ 
        liste les valeurs des attributs ::

           for elt in  node.values():
               ...
        """

        for elt in self.__data.values():
            if isinstance(elt,Attribute):
                yield elt.access()
            else:
                yield elt

    #--------------------------------------------------------------------------
    def items(self):

        """ 
        liste des tuples (chemins,noeud) ::

           for key,elt in  node.items():
               ...   
        """

        for k,elt in Base_Node.items(self):
            if isinstance(elt,Attribute):
                yield k,elt.access()
            else:
                yield k,elt

    #--------------------------------------------------------------------------

    def __getitem__(self,key):

        """
        lire une donnée, déclenche les évènements attachés::

            a = node["a"]


       Les champs déclarés avec Field sont enregistrés 
        avec Attribute dans le dictionnaire. la lecture fait appel à la fonction access
    On ne récupère jamais l'objet Attribute.
        Les autres sont lus tel quel.

    L'accès aux données
         """


        node=Base_Node.__getitem__(self,key)

        if isinstance(node,Attribute):
            return node.access()
        else:
            return  node

    #--------------------------------------------------------------------------

    def __setitem__(self,key,value):

        """
        modifier une donnée, déclenche les évènements attachés ::

           node["a"] = 2

        Les champs déclarés avec Field sont enregistrés 
        avec Attribute dans le dictionnaire. fait appel à la fonction modify

        Les autres sont enregistrés tel quel.
        """

        if key in self.keys():
            node=Base_Node.__getitem__(self,key)

            if isinstance(node,Attribute):
                return node.modify(value)

        Base_Node.__setitem__(self,key,value)

    #-------------------------------------------------------------------------

    def __delitem__(self,key):

        """
        supprimer une donnée , déclenche les évènements attachés::

            del node["a"]
        """


        if key in self.keys():
            node=Base_Node.__getitem__(self,key)

            if isinstance(node,Attribute):
                return node.delete()

        Node.__delitem__(self,key)

#-------------------------------------------------------------------------

#=============================================================================


# objectifs de version

## branche v_4

* [ ] fusionner les bibliothèques flask
    * ionode IoFlask_
    * gui Api_
* [ ] creer les plugins d'aide dans ./server
* [ ] supprimer lib Atom et xmlscript
* [ ] fusionner IoNode dans mod_program
* [ ] appli de demo
* [ ] intégrer les docs

## branche v_3

* [x] interface web
* [x] lancement des services
    * [x] aide en ligne
    * [x] navigation dans des fichiers

## branche v_2

* [x] ajouter les objets décrits en xml
* [x] ajouter un app store
* [x] ajouter des demos

## branche v_1

* [ ] introspection
    * [x] lister les classes
    * [ ] lister les packages
* [x] ajouter les addons pour le code (timeline,...)
* [x] lancer le serveur pour un fichier .blackboard

## branche v_0

* [x] installation des composants de base pour faire fonctionner le code
    * [x] variables d'environnement pour executables __bin et bibliothèques __python3
    * [x] charger un programme et afficher sa structure
    * [x] charger un programme et l'executer
    * [x] aide pour les commandes

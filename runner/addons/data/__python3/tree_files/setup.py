from .Disc_Directory import Disc_Directory
from .Disc_File import Disc_File
from minimal.path import ls
from dataModel import *
from mod_program import Action


#========================================================

class Import_Disc(Action):

#========================================================
    pass

#========================================================

class Import_DirectoryLevel(Import_Disc):

#========================================================

    EXCEPTIONS=List()
    DEPTH=Integer(default="1")

    #----------------------------------------------------
    def onNextCall(self,node):
        self.make_level(self.depth,node)

    #----------------------------------------------------
    def make_level(self,level,root):

        if level ==0:
            return
        level-=1
        for elt in ls(root.file_path,recursive=False).directories():
            if elt.name() not in self.exceptions:
                if elt.name().startswith("__")!=True:
                    node=Disc_Directory(parent=root,name=elt.name(),file_path=elt.string)
                    self.make_level(level,node)

    #----------------------------------------------------

#========================================================

class Import_Directories(Import_Disc):

#========================================================

    EXCEPTIONS=List()
    RECURSIVE=Boolean(default=True)

    #----------------------------------------------------
    def onNextCall(self,node):
        for elt in ls(node.file_path,recursive=self.recursive).directories():
            if elt.name() not in self.exceptions:
                #if elt.string.startswith( "__") != True:
                path=elt.relative(node.file_path)
                node.append(path,cls=Disc_Directory,file_path=elt.string,recursive=self.recursive)
    #----------------------------------------------------
        
#========================================================

class Import_Files(Import_Directories):

#========================================================
    #EXCEPTIONS=List()
    #RECURSIVE=Boolean(default=True)
    #----------------------------------------------------
    def onNextCall(self,node):

        Import_Directories.onNextCall(self,node)

        for directory in node.all().by_class(Disc_Directory):

            for elt in ls(directory.file_path,recursive=False).files():
                do=True
                for exception in self.exceptions:
                    if exception in elt.string:
                            do=False
                            break

                if do==True:
                    path=elt.relative(node.file_path)
                    node.append(path,cls=Disc_File,file_path=elt.string)
        node.tree()
    #----------------------------------------------------

#========================================================

class Import_Packages(Import_Disc):

#========================================================

    RECURSIVE=Boolean(default=True)
    EXCEPTIONS=List()
    INIT_FILE=String()

    #----------------------------------------------------
    def onNextCall(self,node):

        for elt in ls(node.file_path,recursive=self.recursive).filterdirectories(self.init_file):
            if elt.name() not in self.exceptions:
                path=elt.relative(node.file_path)
                node.append(path,cls=Disc_Directory,file_path=elt.string)
    #----------------------------------------------------
        
#========================================================


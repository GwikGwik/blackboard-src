import shutil,os
from .Disc_File import Disc_File
from .Disc_Element import Disc_Element
from mod_program import command,execute
from minimal.path import ls,filterfiles,filterdirectories,FilePath
from dataModel import *
#========================================================================

class Disc_Directory(Disc_Element):

#========================================================================

    #CLEAN=Function()
    #GET_ROOT=Function()
    #FILE=Function()
    #DIRECTORY=Function()
    #LS=Function()
    #FILTER_FILES=Function()
    #FILTER_DIRECTORIES=Function()
    OPEN_NAUTILUS=Function()



    #-------------------------------------------------------------    
    def elements(self,*ext):
        return filterfiles(self.get_path(),*ext)
    #-------------------------------------------------------------    
    def get_elements(self,*ext):
        return filterfiles(self.get_path(),*ext).filtersystem()
    #-------------------------------------------------------------
    def get_root_element(self,name):
        if self.has_element(name)==True:
            return self.get_element(name)
        elif self.parent!=None and isinstance(self.parent,Disc_Directory)==True:
            return self.parent.get_root_element(name)


    #-------------------------------------------------------------
    @command
    def open_nautilus(self):
        if self.file_path is not None:
            return "nautilus %(file_path)s"%self
    #----------------------------------------------------------------

    def create(self):

        if not self.file_path.exists():
            os.makedirs(self.file_path.string)


    #----------------------------------------------------------------

    def remove(self):

        if self.file_path.exists():
            shutil.rmtree(self.file_path.string)

    #----------------------------------------------------------------

    def clean(self):

        for elt in self.ls().directories():
            node=Files_Directory(file_path=elt)
            node.remove()

        for elt in self.ls().files():
            node=Files_File(file_path=elt)
            node.remove()

    #----------------------------------------------------------------

    def copy(self,destination):

        shutil.copytree(self.file_path.string,str(destination))
        return Files_Directory(destination)

    #               OBJECTS
    #===========================================================

    #----------------------------------------------------------------

    def get_root(self):

        if self.parent is None:
            node = self.__class__(file_path=self.file_path.root())
            self.reparentTo(node)
            return node
        else:
            return self.parent

    #----------------------------------------------------------------

    def new_file(self,name,cls=Disc_File,**args):

        """
        nodes=dict(self.by_attr("name"))

        if name in nodes.keys():
            return nodes[name]
        else:
            node = cls(parent=self,file_path=self.file_path.join(name),**args)
            return node
"""
        node = cls(file_path=self.file_path.join(name),**args)
        node.setup()
        return node

    #----------------------------------------------------------------

    def new_directory(self,name,**args):

        node=self.append(name,cls=self.__class__,file_path=self.file_path.join(name),**args)
        node.setup()
        return node

    #----------------------------------------------------------------

    def ls(self,recursive=True,**args):
        return ls(self.file_path,recursive=recursive,**args)
        
    #----------------------------------------------------------------

    def filter_files(self,*extentions,**args):
        return filterfiles(self.file_path,*extentions,**args)

    #----------------------------------------------------------------
        
    def filter_directories(self,initfile="",**args):
        return filterdirectories(self.file_path,initfile,**args)

    #----------------------------------------------------------------



#========================================================================





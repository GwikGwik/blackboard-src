from IoNode import IoNode_Atom
from dataModel import *
import time
#============================================================

class Classes_Loader(IoNode_Atom):

#============================================================

    SELECT=String()
    #-------------------------------------------------------

    def onExecute(self,**args):
       for elt in self.find(self.select).all().by_class("Python_Manager"):
            print("classes loader",elt.path())
            elt.query(action="gather_classes")
            #elt.query(action="update_heritage")
            time.sleep(0.05)


       self.stop()

    #-------------------------------------------------------
#============================================================

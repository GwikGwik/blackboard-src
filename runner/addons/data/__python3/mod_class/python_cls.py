from atom import Atom,Atom_Link
from mod_program import Object,Node_Handler
from dataModel import *
from dataModel import getClass
import os
from .classes import *


#==========================================================

class Python_Init_Instances(Object,Node_Handler):
    
#==========================================================
    #-------------------------------------------------------------
    def onSetup(self):
        self.select_node()
        self.plugin_update()
    #-------------------------------------------------------------
    def plugin_update(self):

        self.clear()
        for elt in self.selected_node.selected_node.all():
            cls=self.selected_node.get_class(elt.__class__.__name__)
            parent=self.find(cls.name,cls=Atom)
            Classes_InstanceOf(source=elt,target=cls,parent=parent)

                    
    #-------------------------------------------------------------
    def plugin_updateX(self):

        for elt in self.all():
            if isinstance(elt,Classes_Instance)==True:
                if "__cls_scan" not in elt.keys():
                    elt["__cls_scan"]=True
                    cls=self.selected_node.get_class(elt.__class__.__name__)
                    cls.instance(elt,parent=self)


#==========================================================

class Python_Manager(Classes_Manager):
    
#==========================================================
    CLS=String(default="Model")

    #-------------------------------------------------------------
    def onSetup(self):
        self.gather_classes()


    #-------------------------------------------------------------
    def onPostSetup(self):
        self.update_heritage()        

    #-------------------------------------------------------------

    def get_instance(self,classname,**args):

        cls=self.get_class(classname)
        node,link= cls.get_instance(**args)
        return node,link


    #-------------------------------------------------------------
    def gather_classes(self):

        from xmlscript.import_classes import import_all
        import_all(os.environ["HOME"],DEBUG=False)

        root_cls=getClass(self.cls)

        for cls in models():
            if issubclass(cls,root_cls):
                self.get_class(name=cls.__name__,
                                cls=Python_Class,
                                python_classname=cls.__name__)

    #-------------------------------------------------------------
    def update_heritage(self):

        for cls in self.classes():
            self.init_class_heritage(cls)


    #------------------------------------------------------------- 
    def init_class_heritage(self,cls):

        if "__cls_heritage" not in cls.keys():
            cls["__cls_heritage"]=True
            if hasattr(cls.python_class,"__bases__"):
                for elt in cls.python_class.__bases__:
                    if elt != cls.python_class and isinstance(elt,ModelMeta)==True:
                        node=self.get_class(name=elt.__name__,cls=Python_Class)
                        Classes_SubClassOf(source=cls,target=node)#,parent=self)
                        #self.init_class_heritage(node)
            else:
                pass#print(dir(cls))
    #-------------------------------------------------------------

                       
#========================================================================

class Python_Attribute(Classes_Attribute):

#========================================================================
    pass

#========================================================================

class Python_Method(Classes_Method):

#========================================================================
    pass

#========================================================================

class Python_Class(Classes_Class):

#========================================================================
    DOC=String(default="")
    PYTHON_CLASSNAME=String()
    PYTHON_CLASS=Field(default=None)
    PYTHON_PATH=String(default=None)
    PYTHON_MODULE=String(default=None)
    CLS_OBJ=Field()
    #-------------------------------------------------------------
    def onSetup(self):


        #print(self.python_class,self.parent.path())
        if self.python_class is None:
            #print(self.python_classname)
            self.python_class=getClass(self.python_classname)
        #self.python_classname=self.python_class.__name__

        if self.python_class is not None:
            self.python_module=self.python_class.__module__
            self.python_path=self.python_module+"."+self.python_class.__name__
            self.doc=self.python_class.__doc__
            #print(self.python_path)

            for k,elt in self.python_class._fields.items():
                Python_Attribute(parent=self,name=k,elt=elt,record=elt.record)
                
            for k,elt in self.python_class._functions.items():
                Python_Method(parent=self,name=k,elt=elt)


    #-------------------------------------------------------------

    def get_instance(self,instance=None,parent=None,**args):

        if instance is None:

            node= self.python_class(parent=parent,**args)
            link =self.instance(node)
        return node,link
    
    #-------------------------------------------------------------

    def get_instances(self):
        return self.inputs.by_class(Mods_Mapper_InstanceOf)


    #-------------------------------------------------------------


                           
#========================================================================





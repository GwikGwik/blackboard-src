from dataModel import *
from dataModelAddons.tree.model import ChildrenFrame
#===============================================================================

def store(function):

#===============================================================================
    """
    decorator to merge results of a function into a single list :
       @store
       def function(self,a=1):
           yield obj1
           yield obj2

       @store
       def function(self,a=1):
           yield obj1
           yield [obj2,obj3]

       @store
       def function(self,a=1):
           yield obj0
           yield [obj1,obj4]
           yield [obj2,obj3]
           yield obj5
    """

    def execute(self,*args,**kwargs):

        #create a list of node
        lst=Frame()

        #function returns list or iterator of lists and objects
        for elt in function(self,*args,**kwargs):

            #manage when function returns a list
            if isinstance(elt,Frame) or isinstance(elt,list):

                # merge with current list
                for subelt in elt:
                    if subelt is not None: 
                        lst.append(subelt)

                #elt.destroy()
            elif elt is not None:
                
                lst.append(elt)
        return lst

    return execute

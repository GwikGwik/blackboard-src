#model:Class.py
"""
module
---------------------------------------------------

description

"""
from mod_program import Object
from dataModel import *
from pathlib import Path
import os,sys
#=====================================================================

class Python_File(Object):

#=====================================================================
    PYTHON_PATH=String()
    FILE_PATH=String()
    IMPORTED=Boolean()
    #-----------------------------------------------------------------
    def import_modules(self):
        self.import_module()

        for node in self.children.by_class(Python_File):
            node.import_modules()

    #-----------------------------------------------------------------
    def import_module(self):

        if  self.python_path is not None:
            try:
                __import__(self.python_path)
                self.imported=True
            except:
                self.imported=False


    #-----------------------------------------------------------------
    def submodules(self):
        for node in self.children.by_class(Python_File):
            if  node.python_path is not None:
                yield node

    #-----------------------------------------------------------------
    def all_modules(self):
        for node in self.all().by_class(Python_File):
            if node.python_path is not None:
                yield node

    #-----------------------------------------------------------------
    def all_modules_pathes(self):
        for node in self.all_modules():
            yield node.python_path

    #-----------------------------------------------------------------
    def list_dirs(self):

        for elt in os.listdir(self.file_path):

            elt_path=os.path.join(self.file_path,elt)
            if os.path.isdir(elt_path) and os.path.isfile(os.path.join(elt_path,"__init__.py")):
                yield elt,elt_path

    #-----------------------------------------------------------------
    def list_files(self):
        for elt in os.listdir(self.file_path):
            elt_path=os.path.join(self.file_path,elt)
            if os.path.isfile(elt_path):
                if elt.endswith(".py") and elt != "__init__.py":
                    yield elt.replace(".py",""),elt_path

    #-----------------------------------------------------------------
#=====================================================================

class Python_Module(Python_File):

#=====================================================================


    #-----------------------------------------------------------------
    def onSetup(self):
        pass
    #-----------------------------------------------------------------
    def onCleanup(self):
        pass
    #-----------------------------------------------------------------
#=====================================================================

class Python_Package(Python_File):

#=====================================================================


    #-----------------------------------------------------------------
    def onSetup(self):
        for elt,elt_path in self.list_dirs():
            Python_Package(parent=self,name=elt,
                    python_path=self.python_path+"."+elt,
                    file_path=elt_path)

        for elt,elt_path in self.list_files():
            Python_Module(parent=self,name=elt,
                    python_path=self.python_path+"."+elt,
                    file_path=elt_path)


    #-----------------------------------------------------------------
    def onCleanup(self):
        pass
    #-----------------------------------------------------------------
#=====================================================================

class Python_Directory(Python_File):

#=====================================================================


    #-----------------------------------------------------------------
    def onSetup(self):
        for elt,elt_path in self.list_dirs():
            Python_Package(parent=self,name=elt,
                    python_path=elt,
                    file_path=elt_path)

    #-----------------------------------------------------------------
    def onCleanup(self):
        pass
    #-----------------------------------------------------------------
#=====================================================================

class Python_Installation(Python_File):

#=====================================================================


    #-----------------------------------------------------------------
    def onSetup(self):
        done=[]
        path=os.environ["BLACKBOARD_SRC"]

        for elt in sys.path:
            if elt not in done:
                done.append(elt)
                if path+"/" in elt:
                    p=Path(elt)
                    if not "__test" in p.parts :
                        Python_Directory(parent=self,name=p.parent.name,
                                        file_path=str(elt))
    #-----------------------------------------------------------------

#=====================================================================





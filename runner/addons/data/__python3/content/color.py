from .numeric import *
from atom import Atom
from dataModel import *

#============================================================

class RGB(Vector):

#============================================================
    DIM=3
    def onSetup(self):
        self.r=self.set_data(name="0")
        self.g=self.set_data(name="1")
        self.b=self.set_data(name="2")

#============================================================

class RGBA(Vector):

#============================================================
    DIM=4
    def onSetup(self):
        self.r=self.set_data(name="0")
        self.g=self.set_data(name="1")
        self.b=self.set_data(name="2",)
        self.a=self.set_data(name="3")


"""
simple interface avec le module mimetype

* crée un arbre mime type pour la résolution des formats
* instancié dans main
"""

import mimetypes
from atom import Atom,Atom_Link
from dataModelAddons.tree import tree

from mod_program import *

#===============================================================

class MimeType(Data):

#===============================================================

    def onSetup(self,**args):

        if "fullname" not in self.keys():
            self.fullname=self.name
        self.extentions=mimetypes.guess_all_extensions(self.fullname,strict=False)


#===============================================================

class MimeTypes(Data):

#===============================================================

    #--------------------------------------------------------- 
    @tree(Class=MimeType,separator="/")
    def onSetup(self,**args):

        mimetypes.init()
        for k,v in mimetypes.types_map.items():
            yield v,dict(extention=k,fullname=v)

    #---------------------------------------------------------
    def get_type(self,url=None):

        t,o= mimetypes.guess_type(url, strict=1)
        if t is not None:

            node=self.append(t,cls=MimeType_Type)
            return node
        return
    #---------------------------------------------------------

##===============================================================




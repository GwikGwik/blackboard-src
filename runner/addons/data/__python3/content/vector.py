from mod_program import Data
from atom import Atom
from dataModel import *

#============================================================

class Dict(Data):

#============================================================



    def onSet(self,value):

        for k,v in value.items():
            self.set_data(name=k,value=v)

    def onGet(self,value):

        return self.getContent()




    def getContent(self,cls=Data):

        value=dict()
        for child in self.children.by_class(self.getClass(cls)):
            value[child.name]=child.value
        return value

#============================================================

class List(Dict):

#============================================================

    INDEX_N=Integer(default=0)

    def set_data(self,name=None,**args):

        if not name:
            name=str(self.index_n)
            self.index_n+=1

            node= Dict.set_data(self,
                    name=name,index=self.index_n,
                    **args)

        else:
            node= Dict.set_data(self,
                    name=name,
                    **args)

        return node

    def onSet(self,value):

        if type(value) is list:
            for elt in value:
                self.set_data(value=elt)

        else:
            self.set_data(value=value)


    def getContent(self,cls=Data):

        result=list()
        for child in self.children.by_class(cls):

            result.append(child.value)
        return result

#============================================================


from mod_program import *
#from mod_program import *
from dataModel import *
import requests, json
#==========================================================

class OpenApi_Node(Object):

#==========================================================

    LABEL=String()

    def get_server(self):
        for elt in self.ancestors:
            if isinstance(elt,OpenApi_Server):
                return elt

    def onSetup(self):
        if "json" in self.keys():
            self.make(self.json)

    def make(self,data):
        self.onMake(data)

    def onMake(self,data):
        pass


#==========================================================

class OpenApi_Property(OpenApi_Node):

#==========================================================

    def onMake(self,data):
        pass

#==========================================================

class OpenApi_Model(OpenApi_Node):

#==========================================================

    def onMake(self,data):
        pass
        #self.make_subdict(data,"properties",OpenApi_Property)


#==========================================================

class OpenApi_Tag(OpenApi_Node):

#==========================================================

    def onMake(self,data):
        pass

#==========================================================

class OpenApi_Parser(OpenApi_Node):

#==========================================================

    def onMake(self):
        return

#==========================================================

class OpenApi_ModelParser(OpenApi_Parser):

#==========================================================

    def onMake(self,data):
        node=self.get_server().find(data["$ref"][2:])
        Atom_Link(source=node,target=self.parent).show()
#==========================================================

class OpenApi_ModelList(OpenApi_Parser):

#==========================================================

    def onMake(self,data):
        node=self.get_server().find(data["items"]["$ref"][2:])
        Atom_Link(source=node,target=self.parent).show()

#==========================================================

class OpenApi_TypeList(OpenApi_Parser):

#==========================================================
    CLS=String()

    def onMake(self,data):
        self.cls=data["items"]["type"]
#==========================================================

class OpenApi_TypeFile(OpenApi_Parser):

#==========================================================
    CLS=String()

    def onMake(self,data):
        pass

#==========================================================

class OpenApi_Response(OpenApi_Node):

#==========================================================

    def onMake(self,data):
        if "schema" in data.keys():
            node=None
            schema=data["schema"]
            if '$ref' in schema.keys():
                node=OpenApi_ModelParser(parent=self,json=schema)
            elif 'items' in schema.keys() and 'type' in schema.keys() and schema["type"] == "array":
                if "$ref" in schema["items"].keys():
                    node=OpenApi_ModelList(parent=self,json=schema)
                elif "type" in schema["items"].keys():
                    node=OpenApi_TypeList(parent=self,json=schema)
            elif 'type' in schema.keys() and schema["type"] == "file":
                    node=OpenApi_TypeFile(parent=self)
            if node:
                node.setup()
            else:
                pass#print(self.parent.name,self.parent.parent.name,data["schema"])

#==========================================================

class OpenApi_Parameter(OpenApi_Node):

#==========================================================

    def onMake(self,data):
        pass

#==========================================================

class OpenApi_Request(OpenApi_Node):

#==========================================================

    def onMake(self,data):

        if "tags" in data.keys():
            tags=self.get_server().find("tags")
            for elt in data["tags"]:
                node=tags.find(elt,error=False)
                if node:
                    Atom_Link(source=node,target=self)

#==========================================================

class OpenApi_Path(OpenApi_Node):

#==========================================================

    def onMake(self,data):
        pass#print(self.name)
        #print(data)


#==========================================================

class OpenApi_Server(OpenApi_Node):

#==========================================================

    URL=String()

    def get_server(self):
        return self

#==========================================================



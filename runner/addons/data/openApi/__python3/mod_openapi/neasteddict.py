
from atom import *
from mod_program import *
from dataModel import *
import requests, json
#==========================================================

class JsonReader(Action):

#==========================================================

    def onCall(self,node):
        content = requests.get(node.url)
        data = json.loads(content.content)
        self.onMake(node,data)


    def onMake(self,root,data):
        for child in self.children.by_class(NeastedDict_Node):
            child.make(root,data)


#==========================================================

class NeastedDict_Node(Atom):

#==========================================================

    def make(self,root,data,**args):
        self.onMake(root,data,**args)

    def onMake(self,root,data,**args):
        for child in self.children.by_class(NeastedDict_Node):
            child.make(root,data,**args)

#==========================================================

class NeastedDict_Select(NeastedDict_Node):

#==========================================================

    LABEL=String()

    def onMake(self,root,data,**args):
        if self.label in data.keys():
            NeastedDict_Node.onMake(self,root,data[self.label],**args)

#==========================================================

class NeastedDict_List(NeastedDict_Node):

#==========================================================

    def onMake(self,root,data,**args):
        if type(data)== list:
            for elt in data:
                NeastedDict_Node.onMake(self,root,elt,**args)

#==========================================================

class NeastedDict_Dict(NeastedDict_Node):

#==========================================================
    KEY=String(default="name")
    AVOID=String()
    def onMake(self,root,data,**args):
        avoid_list=[]
        if self.avoid:
            avoid_list=self.avoid.split(",")
        if type(data)== dict:
            for k,elt in data.items():
                if k not in avoid_list:
                    data=dict(args)
                    data[self.key]=k
                    NeastedDict_Node.onMake(self,root,elt,**data)


#==========================================================

class NeastedDict_Item(NeastedDict_Node):

#==========================================================

    CLS=ModelField(default="Atom")

    def onMake(self,root,data,**args):
        root=self.cls(parent=root,json=data,**args)
        root.setup()
        NeastedDict_Node.onMake(self,root,data)

#==========================================================

class NeastedDict_Value(NeastedDict_Node):

#==========================================================
    KEY=String(default="name")
    VALUE=String()
    CHECK=String(default=None)

    def onMake(self,root,data,**args):
        if self.check:
            if not self.check in data.keys():
                print("no key",self.key)
                return
            root[self.key]=self.value%data
        else:
            root[self.key]=self.value%data
#==========================================================



from atom import Atom,Atom_Link
from dataModel import Function,Boolean
from mod_program import Action
import time

#==========================================================

class Grafcet_Main(Action):
    
#==========================================================

    #---------------------------------------------------

    def onSetup(self,**args):

        try:
            self.__init=self.find("__init__")
        except:
            self.__init=Grafcet_EtapeInitiale(parent=self,name="__init__")
        self.current=self.__init


    def onCall(self,node):
        yield self.onNext(node)
        while self.hasNext():
            yield self.onNext(node)

    def hasNext(self):
        return self.current is not None and self.current !=  self.__init

    def onNext(self,node):
        self.current.set_node(node)
        result= self.current.call()
        self.current=self.current.next()
        return result
    #---------------------------------------------------


#==========================================================

class Grafcet_Etape(Action):

#==========================================================
 
    #continue to explore
    has_next=True
    NEXT=Function()

    #---------------------------------------------------
    
    def onCall(self,**args):


        for child in self.children.by_class(Action):
            yield child()

    #---------------------------------------------------
    
    def next(self):
        
        
        #if no output
        if len(self.outputs)==0:
            raise Exception("graph needs to be closed",self.path())
        
        #more than one output is not allowed for basic GrafcetEtape
        elif len(self.outputs)>1:
            raise Exception("more than one link",self.path())

        #else  retry
        else:
            return self.outputs[0]
        
    #---------------------------------------------------

#==========================================================

class Grafcet_EtapeInitiale(Grafcet_Etape):

#==========================================================
    #stop to explore
    has_next=False


    def next(self):
        if hasattr(self,"isrunning"):
            delattr(self,"isrunning")
            return None
        else:
            setattr(self,"isrunning",True)
            return Grafcet_Etape.next(self)

#==========================================================

class Grafcet_Et(Grafcet_Etape):pass

#==========================================================

class Grafcet_Ou(Grafcet_Etape):

#==========================================================
    

    def next(self):
        
        self()

        
        #if no output
        if len(self.outputs)==0:
            raise Exception("graph needs to be closed",self.path())

        lst=list()
        for link in self.outputs:
            if link.test()==True:
                lst.append(link)

        if len(lst)==0:
            return self
        elif len(lst)==1:
            return lst[0].next()
        
        raise Exception("only one link ok for or loop",self.path())
    

#==========================================================

class Grafcet_Transition(Atom_Link):

#==========================================================

    TEST_VAR=Boolean()

    def next(self):
        if self.test_var:
            return self.target
        else:
            return self.source
            
            

    def onCall(self,**args):
        self.test_var= True
        yield self.onTest(**self.getDict())


    def onTest(self,**args):

        for child in self.children.by_class(Grafcet_Condition):
            yield dict(name="test_"+child.name)
            if child.test()==False:
                self.test_var= False
                break

    

                

#==========================================================
    
class Grafcet_Condition(Action):

#==========================================================


    def test(self):
        
        return self.onTest(**self.getDict())

    def onTest(self,**args):
        return True


        
#==========================================================


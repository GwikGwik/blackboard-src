#model:Class.py
"""
module
---------------------------------------------------

description

"""
from atom import Atom
from dataModel import *
from mod_program import *
#from tree_files import Disc_Files,Disc_Directories
import os
import jinja2


#=====================================================================

class Builder_Node(Action):

#=====================================================================
    FILE_PATH=String(default=None)
   #-----------------------------------------------------------------
    def command(self,string,**args):
        #print(string)
        r= execute(string,**args)
        #r.show()
        return r
   #-----------------------------------------------------------------
    def get_path(self):
        if "file_path" in self.keys():
            return self.file_path
        if isinstance(self.parent,Builder_Disc):
            return self.parent.get_path()



    #-----------------------------------------------------------------
    def onNextCall(self,node):
        self.build(node)

    #-----------------------------------------------------------------
    def build(self,node):
        self.onBuild(self.get_path()%node,node)
        for child in self.children.by_class(Builder_Node):
            child.build(node)

    #-----------------------------------------------------------------
    def onBuild(self,path,node):
        pass
    #-----------------------------------------------------------------
#=====================================================================

class Source(Object):

#=====================================================================

    FILE_PATH=String()
    RECURSIVE=Boolean(default=False)
    TYPE=String(default="d")
#=====================================================================

class Template_Dir(Object):

#=====================================================================

    FILE_PATH=String()

#=====================================================================

class Template_Tool(Object):

#=====================================================================

    VARIABLE=String(default="blackboard_templates")

    #-----------------------------------------------------------------
    def onSetup(self):

        #print(self.template_path)
        templates=os.environ[self.variable].split(":")
        for elt in templates:
            if elt !="":
                Template_Dir(parent=self,name=elt.split('/')[-2],file_path=elt)
        loader = jinja2.FileSystemLoader(templates)
        #loader = jinja2.FileSystemLoader(self.template_path)
        self.environment = jinja2.Environment(loader=loader)


   #-----------------------------------------------------------------
    def get_template(self,template,**args):
        template = self.environment.get_template(template)
        return template.render(**args)
    #-----------------------------------------------------------------

#=====================================================================

class Builder(Builder_Node):

#=====================================================================

    
    OUTPUT_PATH=String(default=".")
    TEMPLATES=String(default=".")

    #-----------------------------------------------------------------
    def onSetup(self):
        self.setEnviron()
        self.setCommandLine()
        self.processTree()
        self.selected_node=Atom()
        for child in self.children.by_class(Source):
            """
            if child.type=="d":
                Disc_Directories(parent=self.selected_node,
                            name=child.name,
                            file_path=child.file_path,
                            recursive=child.recursive)

            elif child.type=="f":
                Disc_Files(parent=self.selected_node,
                            name=child.name,
                            file_path=child.file_path,
                            recursive=child.recursive)

              """                
        self.templates_node=self.find(self.templates)
        self.selected_node.setup()
        #self.selected_node.tree()


   #-----------------------------------------------------------------
    def get_path(self):
        return self.output_path
   #-----------------------------------------------------------------
    def get_template(self,template,**args):
        return self.templates_node.get_template(template,**args)
    #-----------------------------------------------------------------

#=====================================================================

class Builder_Disc(Builder_Node):

#=====================================================================
    SUDO=Boolean(default=False)

   #-----------------------------------------------------------------
    def get_path(self):
        r=Builder_Node.get_path(self)
        if r is not None:
            return r
        if isinstance(self.parent,Builder_Disc):
            return self.parent.get_path()+"/"+self.name
        
    #-----------------------------------------------------------------
    def onBuild(self,path,node):
        pass#print(path)
    #-----------------------------------------------------------------
    def command(self,string):
        if self.sudo==True:
            return Builder_Node.command(self,"sudo "+string)
        return Builder_Node.command(self,string)
    #-----------------------------------------------------------------

#=====================================================================

class Dir(Builder_Disc):

#=====================================================================

    #-----------------------------------------------------------------
    def onBuild(self,path,node):
        if os.path.exists(path) != True:
            self.command("mkdir -p "+path)

    #-----------------------------------------------------------------
#=====================================================================

class File(Builder_Disc):

#=====================================================================


    #-----------------------------------------------------------------
    def onBuild(self,path,node):
        content=""
        for child in self.children.by_class(Render):
            content+=child.render(node)

        if self.sudo==True:
            return Builder_Node.command(self,"sudo tee %s"%path,stdin=content)
        else:

            f=open(path,'w')
            f.write(content)
            f.close()
    #-----------------------------------------------------------------
#=====================================================================

class SymLink(Builder_Disc):

#=====================================================================

    SOURCE=String()

    #-----------------------------------------------------------------
    def onBuild(self,path,node):
        if os.path.exists(path) != True:
            source= os.path.abspath(self.source)
            r=self.command("ln -s %s %s"%(source,path))

    #-----------------------------------------------------------------

#=====================================================================

class Copy(Builder_Node):

#=====================================================================

    SOURCE=String()

    #-----------------------------------------------------------------
    def onBuild(self,path,node):
        pass#print(path)
    #-----------------------------------------------------------------


#=====================================================================

class Render(Atom):

#=====================================================================

    #-----------------------------------------------------------------
    def render(self,node):
        content=""
        for child in self.children.by_class(Render):
            content+=child.render(node)
        content=self.onRender(node=node,content=content)
        return content

    #-----------------------------------------------------------------
    def onRender(self,**args):
        return ""
    #-----------------------------------------------------------------
#=====================================================================

class AddTemplate(Render):

#=====================================================================
    SELECT=String(default=None)
    MANAGER=String()
    SRC=String()

    #-----------------------------------------------------------------
    def onRender(self,node=None,**args):
        manager=node.find(self.manager)
        src=self.src%node
        if self.select:
            node=node.find(self.select)

        #print(self.src,self.select,node)
        return manager.get_template(src,node=node,templates=manager,**args)

    #-----------------------------------------------------------------
#=====================================================================

class AddFile(Render):

#=====================================================================

    SOURCE=String()

    #-----------------------------------------------------------------
    def onRender(self,**args):

        source= os.path.abspath(self.source)
        if os.path.exists(source) != True:
            f=open(source,'w')
            content=f.read()
            f.close()
            return content
        return "error "+source
    #-----------------------------------------------------------------
#=====================================================================



from dataModel import *
from .tree import *
from mod_curve import *
import numpy as np
import matplotlib.pyplot as plt

#=================================================================

class Curve_Distance(Curve):

#=================================================================



    #------------------------------------------------------------
    def onSetup(self):
        pass

    #------------------------------------------------------------
    def infos(self):
        points=self.parent.all().by_class(Planet_Place)
        done=list()
        print(len(points))
        for point in points:
            for child in self.parent.all().by_class(Planet_Place):
                if child == point or (child,point) in done or (point,child) in done:
                    pass
                else:
                    Planet_Line(parent=self,source=point,target=child)
                    done.append((point,child))

        d=list()
        for elt in self.children.by_class(Planet_Line):
            d.append(elt.distance())

        print(len(d))
        print(d[:10])
        return d


    #------------------------------------------------------------
    def plot(self):
        d=self.infos()

        n, bins, patches = plt.hist(d, bins=200)
        plt.show()

        #n, bins, patches = plt.hist(d, bins=200, range=(0,20000))
        #plt.show()

#=====================================================================


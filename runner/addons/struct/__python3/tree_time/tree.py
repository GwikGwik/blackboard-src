from atom import Atom,Atom_Link
from dataModel import *
from content import Data
from datetime import datetime,date
import calendar
from mod_program import Action
import matplotlib.pyplot as plt
#==============================================================

class Time_Instant(Data):

#==============================================================

    def onSet(self,value):
        if type(value) in [str,int,float]:
            return datetime.fromtimestamp(value)
        else:
            return value

    def period(self,node):
        if self.is_upper(node)==True:
            return Time_Period(source=node,target=self)
        else:
            return Time_Period(source=self,target=node)

    def is_upper(self,node):
        return node.value < self.value

    def is_lower(self,node):
        return node.value > self.value

     #---------------------------------------------------------

    def now(self,**args):

        return Time_Instant(value=datetime.now(),parent=self,**args)
     #---------------------------------------------------------

    def today(self,**args):
        return Time_Instant(value=date.today(),parent=self,**args)
     #---------------------------------------------------------
    def is_inside(self,node):#TimePeriod
        return (not self.upper(node.target)) and (not self.lower(node.source))

#==============================================================

class Time_Period(Atom_Link):

#==============================================================


    def lenght(self):
        return self.target.value-self.source.value

    def is_inside(self,node):
        return (not node.upper(self.target)) and (not node.lower(self.source))

    def get_bounds(self):
        return (self.source.value,self.target.value)
#==============================================================

class Time_Clock(Data,Node):

#==============================================================

    ATTR=""

    #---------------------------------------------------------
    def onSet(self,value):
        if type(value)==int:
            d=dict()
            d[self.ATTR]=value
            value=self.parent.value.replace(**d)

        self.name=str(getattr(value,self.ATTR))
        if len(self.name)==1:self.name="0"+self.name
        return value

    #------------------------------------------------------------
    def infos(self):
        x,y=list(),list()
        lst=list()
        for elt in self.children.by_class(Time_Clock):
            lst.append(elt.name)
        lst.sort()

        for elt in lst:
            x.append(elt)
            y.append(self.find(elt).count_links())
        return x,y

    #------------------------------------------------------------
    def count_links(self):
        i=0
        for elt in self.all().by_class(Time_Clock):
            i+=len(elt.outputs)
        return i
    #------------------------------------------------------------
    def plot(self):
        x,y=self.infos()
        plt.bar(x,y)
        plt.show()
    #------------------------------------------------------------

    #---------------------------------------------------------

#==============================================================

class Time_Micros(Time_Clock):

#==============================================================
    ATTR="microsecond"

#==============================================================

class Time_Second(Time_Clock):

#==============================================================
    ATTR="second"

#==============================================================

class Time_Minute(Time_Clock):

#==============================================================
    ATTR="minute"

#==============================================================

class Time_Hour(Time_Clock):

#==============================================================
    ATTR="hour"
#==============================================================

class Time_Day(Time_Clock):

#==============================================================
    ATTR="day"
    MONTH_DAY=Integer()
    WEEK_DAY=Integer()

    #---------------------------------------------------------
    def make(self):
        for i in range(24):
            Time_Hour(parent=self,value=i)
    #---------------------------------------------------------

#==============================================================

class Time_Month(Time_Clock):

#==============================================================
    ATTR="month"
    #---------------------------------------------------------
    def make(self):
        cal=calendar.Calendar()
        for i,j in cal.itermonthdays2(parent.name,self.name):
            Time_Day(parent=self,month_day=i,week_day=j,value=i)
    #---------------------------------------------------------
#==============================================================

class Time_Year(Time_Clock):

#==============================================================
    ATTR="year"
    #---------------------------------------------------------
    def make(self):
        for i in range(12):
            Time_Month(parent=self,value=i)
    #---------------------------------------------------------


#==============================================================

class Time_Tree(Time_Clock):

#==============================================================

    #---------------------------------------------------------
    CLASSES=dict(
                year=Time_Year,
                month=Time_Month,
                day=Time_Day,
                hour=Time_Hour,
                minute=Time_Minute,
                second=Time_Second,
                microsecond=Time_Micros
            )

    CALENDAR=("year","month","day")
    DAY=("hour","minute","second")
    CHRONO=("hour","minute","second","microsecond")
    DAILY_CHRONO=("hour","minute","second")
    DATE_HOURS=("year","month","day","hour","minute")
    FULL=("year","month","day","hour","minute","second","microsecond")

    OPTION=String(default="FULL")

    #---------------------------------------------------------

    def onSetup(self):
        self.value=datetime(1,1,1,0,0,0)
        self.precision=getattr(self,self.option)
    #---------------------------------------------------------

    def onSet(self,value):
        return value
     #---------------------------------------------------------

    def now(self,**args):

        return self.get_date(datetime.now()).path()

     #---------------------------------------------------------

    def today(self,**args):
        return self.get_date(date.today()).path()


     #---------------------------------------------------------
    def get_date(self,date=None):
        node=self
        for k in self.precision:
            value=getattr(date,k)
            x=None

            for elt in node.children.by_class(Time_Clock):

                if getattr(elt.value,k)==value:
                    x=elt
                    break
            if x is not None:
                node=x
            else:
                node=self.CLASSES[k](parent=node,value=value)
            #print(k,node.path())
        return node

     #---------------------------------------------------------
    def search_dates(self,**args):

       for elt in self.children.by_class(TimeTree_Atom):
            elt.is_inside(**args)


#==============================================================

class Time_Add(Action):

#==============================================================
    ATTR=String()
    Format=String(default=None)

    #---------------------------------------------------------
    def get_tree(self):
        for elt in self.ancestors:
            if isinstance(elt,Time_Tree):
                return elt

    #---------------------------------------------------------
    def onNextCall(self,node):
        tree=self.get_tree()
        value=getattr(node,self.attr)

        if self.format=="timestamp":
            value=datetime.fromtimestamp(value)
        elif self.format=="iso":
            value=datetime.fromisoformat(value)

        d=tree.get_date(value)
        Atom_Link(tag="time",source=d,target=node)

    #---------------------------------------------------------


#==============================================================

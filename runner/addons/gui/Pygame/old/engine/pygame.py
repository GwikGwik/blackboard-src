from .main import Game_Module
import pygame
from pygame.locals import *

#===================================================================

class Game_Player(Game_Module):

#===================================================================

    #----------------------------------------------------------------
    def onStart(self):
        pygame.init()
        pygame.font.init()
        for child in self.children:
            child.setup()
    #----------------------------------------------------------------

    def onExecute(self):

        events = pygame.event.get()
        for event in events:
            print(event)
            if event.type == QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE ):
                self.root.stop()


    #----------------------------------------------------------------
    def onStop(self):
        pass

    #----------------------------------------------------------------
    def push_event(self,msg):
        self.send(msg)

    #----------------------------------------------------------------
    def process_queue(self):

        for msg in self.messages():
            self.process_message(msg)

    #----------------------------------------------------------------
    def process_message(self,msg):
        print("/",msg)
        
    #----------------------------------------------------------------

#===================================================================
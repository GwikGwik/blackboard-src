
import pygame
from pygame.locals import *
VERSION = "0.4"

try:
    import sys
    import random
    import math
    import os
    import getopt
    import pygame
    from socket import *
    from pygame.locals import *
except ImportError(err):
    print( "couldn't load module. %s" % (err))
    sys.exit(2)

from atom import Atom,Action
from dataModel import Integer

#=========================================================

class PyGame_Main(Action):

#=========================================================

    #------------------------------------------------

    def onSetup(self):

        pygame.init()
        pygame.font.init()

    #------------------------------------------------

    def onCall(self):

        while 1:
            for elt in self.children.by_class(Action):
                #print elt.name
                yield elt.call()

    #------------------------------------------------

    def onCleanup(self):

        pygame.font.quit()
        pygame.quit()


    #------------------------------------------------

#=========================================================










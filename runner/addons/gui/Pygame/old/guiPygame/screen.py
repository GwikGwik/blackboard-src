import pygame
from atom import Atom,Action
from dataModel import Integer
#=========================================================

class PyGame_Sprite(Action):

#=========================================================

    X=Integer()
    Y=Integer()

    W=Integer()
    H=Integer()

    #------------------------------------------------

    def onSetup(self):
        print( " "*50,self.n,type(self.n) )

        self.obj=pygame.joystick.Joystick(self.n)
        self.obj.init()

    #------------------------------------------------

    def onCleanup(self):

        self.obj.quit()

    #------------------------------------------------

#=========================================================

class PyGame_Screen(Action):

#=========================================================

    W=Integer()
    H=Integer()

    #------------------------------------------------

    def onSetup(self):

        self.obj = pygame.display.set_mode((self.w,self.h))

    #------------------------------------------------

    def onCleanup(self):

        pass

    #------------------------------------------------

#=========================================================





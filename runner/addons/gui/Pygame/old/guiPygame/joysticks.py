import pygame
from atom import Atom,Action
from dataModel import Integer
#=========================================================

class PyGame_Joystick(Action):

#=========================================================
    N=Integer()

    #------------------------------------------------

    def onSetup(self):
        print(" "*50,self.n,type(self.n) )

        self.obj=pygame.joystick.Joystick(self.n)
        self.obj.init()

    #------------------------------------------------

    def onCleanup(self,**args):

        self.obj.quit()

    #------------------------------------------------

#=========================================================

class PyGame_JoystickManager(Action):

#=========================================================

    #------------------------------------------------

    def onSetup(self):

        pygame.joystick.init()

        for x in range(pygame.joystick.get_count()):
            PyGame_Joystick(parent=self,name=str(x),n=x)


    #------------------------------------------------

    def onCleanup(self):

        for elt in self.children:
            elt.cleanup()
        pygame.joystick.quit()

    #------------------------------------------------

#=========================================================





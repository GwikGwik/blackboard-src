import pygame
import pygame.camera
from atom import Atom,Action
from dataModel import Integer
#=========================================================

class PyGame_Camera(Action):

#=========================================================

    #------------------------------------------------

    def onSetup(self):

        self.obj=pygame.camera.Camera(self.n)
        #self.obj.init()

    #------------------------------------------------

    def onCleanup(self):

        pass

    #------------------------------------------------

#=========================================================

class PyGame_CameraManager(Action):

#=========================================================

    #------------------------------------------------

    def onSetup(self):

        pygame.camera.init()
        for x in  pygame.camera.list_cameras():
            PyGame_Camera(parent=self,name=str(x),n=x)


    #------------------------------------------------

    def onCleanup(self):

        pass

    #------------------------------------------------

#=========================================================





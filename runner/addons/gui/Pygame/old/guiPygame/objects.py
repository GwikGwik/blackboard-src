import pygame
import os,math
#========================================================

def load_png(name):

#========================================================

    """ Load image and return image object"""

#========================================================

    fullname = os.path.join('data', name)

    try:
        image = pygame.image.load(fullname)
        if image.get_alpha() is None:
            image = image.convert()
        else:
            image = image.convert_alpha()
    except pygame.error, message:
        print 'Cannot load image:', fullname
        raise SystemExit, message
    return image, image.get_rect()

#========================================================

class Ball(pygame.sprite.Sprite):

#========================================================

    """A ball that will move across the screen
    Returns: ball object
    Functions: update, calcnewpos
    Attributes: area, vector"""

#========================================================

    def __init__(self, vector):

        pygame.sprite.Sprite.__init__(self)
        self.image, self.rect = load_png('ball.jpg')
        screen = pygame.display.get_surface()
        self.area = screen.get_rect()
        self.vector = vector

    def update(self):
        return
        newpos = self.calcnewpos(self.rect,self.vector)
        self.rect = newpos

    def calcnewpos(self,rect,vector):

        (angle,z) = vector
        (dx,dy) = (z*math.cos(angle),z*math.sin(angle))
        return rect.move(dx,dy)

#========================================================




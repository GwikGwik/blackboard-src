from .surface import Pygame_Surface
import pygame
from dataModel import Field
#==========================================================================

class Pygame_Text(Pygame_Surface):

#==========================================================================
    
    SIZE=Field(default=(0, 0),eval_expr=True)
    
    def onRender(self,surface):
        x,y=self.get_position()
        size=(x,y,self.size[0],self.size[1])


        self.font = pygame.font.SysFont("Arial", self.width)
        self.textSurf = self.font.render(self.text, 1, self.color)
        W = self.textSurf.get_width()
        H = self.textSurf.get_height()
        self.size=(W,H)
        surface.blit(self.textSurf,(x,y-H)  )
        
#==========================================================================

class Pygame_Button(Pygame_Surface):

#==========================================================================
    
    def onSetup(self):
        Pygame_Text(parent=self,text=self.text)
    
#==========================================================================


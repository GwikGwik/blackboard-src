from .surface import Pygame_Surface
from ..modules.screen import Pygame_Area
from dataModel import Field,Float,Integer,Boolean
import pygame.draw


#==========================================================================

class Pygame_Rectangle(Pygame_Surface):

#==========================================================================
    
    SIZE=Field(default=(0, 0),eval_expr=True)
    
    def onRender(self,surface):
        x,y=self.get_position()
        size=(x,y,self.size[0],self.size[1])
        return pygame.draw.rect(surface, self.color, size, self.width) 


#==========================================================================

class Pygame_Square(Pygame_Surface):
    
#==========================================================================
    
    SIZE=Integer(default=10)
    
    def onRender(self,surface):
        x,y=self.get_position()
        size=(x,y,self.size,self.size)
        return pygame.draw.rect(surface, self.color, size, self.width) 

#==========================================================================

class Pygame_Line(Pygame_Surface):

#==========================================================================
    
    SIZE=Field(default=(  0,   0,   0, 0),eval_expr=True)
    CLOSED=Boolean(default=False)

    def onRender(self,surface):
        pointlist=list()
        
        for elt in self.children.by_class(Pygame_Surface):
            pointlist.append(elt.get_position())

        if len(pointlist)>1:
            return pygame.draw.lines(surface, self.color, self.closed, pointlist, self.width) 

#==========================================================================
    
class Pygame_Circle(Pygame_Surface):

#==========================================================================
    
    RADIUS=Integer(default=1)
    
    def onRender(self,surface):
        position=self.get_position()
        return pygame.draw.circle(surface,self.color, position,self.radius, self.width) 

#==========================================================================
    
class Pygame_Grid(Pygame_Surface):

#==========================================================================
    
    REPEAT=Field(default=(10,10),eval_expr=True)
    SIZE=Field(default=(10,10),eval_expr=True)
    
    def onSetup(self):
        for i,j in self.iter_pos():
            position=self.get_pos(i,j)
            Pygame_Rectangle(parent=self,position=position,size=self.size,color=self.color,grid=(i,j),clickable=self.clickable)

    def onRender(self,surface):

        for elt in self.children.by_class(Pygame_Surface):
            elt.position=self.get_pos(*elt.grid)
            elt.size=self.size
            elt.render(surface)
            
        x,y=self.get_position()
        sx,sy=self.repeat
        i,j=self.size
        size=(x,y,sx*i,sy*j)
        return pygame.draw.rect(surface, self.color, size, self.width) 
        
        
    def iter_pos(self):
        for i in range(self.repeat[0]):
            for j in range(self.repeat[1]):
                yield (i,j)
                
    def get_pos(self,i,j):
        sx,sy=self.size
        return (i*sx,j*sy)
#==========================================================================

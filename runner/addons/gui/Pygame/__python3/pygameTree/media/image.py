import pygame
import os
from .surface import Pygame_Surface

class Pygame_Image(Pygame_Surface):
    
    def onSetup(self):
        self.image = pygame.image.load(self.filename)
        if self.image.get_alpha() is None:
            self.image = self.image.convert()
        else:
            self.image = self.image.convert_alpha()

    def onRender(self,surface):
        if self.active == True:
            surface.blit(self.image, self.get_position())
            return self.image
            #self.image=image
            #return image.get_rect()

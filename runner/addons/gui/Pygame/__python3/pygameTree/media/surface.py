from ..modules.screen import Pygame_Area
from dataModel import *
from mod_program import *
import pygame.draw

#==========================================================================

class Pygame_Surface(Pygame_Area):

#==========================================================================
    COLOR=Field(default=(255,255,255),eval_expr=True)
    WIDTH=Integer(default=1)
    


    def render(self,surface):
        if self.active==True:
            self.box=self.onRender(surface)
            for elt in self.children.by_class(Pygame_Surface):
                elt.render(surface)
            
    def onRender(self,surface):
        pass

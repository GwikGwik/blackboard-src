from IoNode import IoNode

import pygame
from pygame.locals import *
from mod_program import *


#=============================================================

class Pygame_Task(Code):

#=============================================================
    pass

#=============================================================

class Pygame_Listener(Object):

#=============================================================
        
    def onCheck(self,event):
        return True
    
    def onCall(self,event):
        for handler in self.children.by_class(Code):
            handler.call()

#=============================================================

class Pygame(IoNode):

#=============================================================

    def onSetup(self):

        #self.setEnviron()
        #self.processTree()
        pygame.init()
        pygame.font.init()

        return IoNode.onSetup(self)


    def onDo(self):

        events = pygame.event.get()
        for e in events:
            self.onProcessEvent(e)

        for task in self.children.by_class(Code):
            task.call()

    def onProcessEvent(self,event):
        #print(event)
        for listener in self.all().by_class(Pygame_Listener):
            if listener.onCheck(event)==True:
                print(listener.path())
                listener.onCall(event)


#=============================================================


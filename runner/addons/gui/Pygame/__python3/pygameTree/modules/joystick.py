import pygame.joystick
#from ..module import Pygame_Module,Pygame_Device,Pygame_DeviceOutput
from atom import Atom
#=========================================================

class Pygame_JoystickAxe(Atom):

#=========================================================
    pass
   
#=========================================================

class Pygame_JoystickButton(Atom):

#=========================================================
    pass
         
#=========================================================

class Pygame_Joystick(Atom):

#=========================================================
    
    def onSetup(self):
        self.obj =pygame.joystick.Joystick(self.elt)
        self.onStart()

        for i in range( self.obj.get_numaxes() ):
            Pygame_JoystickAxe(parent=self,name="axe_"+str(i),elt=self.obj.get_axis(i))
     
        for i in range( self.obj.get_numbuttons() ):
            Pygame_JoystickButton(parent=self,name="button_"+str(i),elt=self.obj.get_button( i ))
            
    def onCleanup(self):
        self.onStop()

        
    def onStart(self):
        self.obj.init()
        
    def onStop(self):
        self.obj.quit()

#=========================================================

class Pygame_Joysticks(Atom):

#=========================================================
    
    def onSetup(self):
        pygame.joystick.init()

        for elt in range(pygame.joystick.get_count()):
            Pygame_Joystick(parent=self,name=str(elt),elt=elt)

    def setup(self):
        Atom.setup(self)
        self.tree()

    def onCleanup(self):
        pygame.joystick.quit()



#=========================================================


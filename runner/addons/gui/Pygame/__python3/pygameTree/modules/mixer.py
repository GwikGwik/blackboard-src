
import pygame
from mod_program import *
from pygame.locals import *
from dataModel import *
#==================================================================

class Pygame_Sound(Action):
#==================================================================
    def onSetup(self):
        print("ok",self.file_path)
        self.sound=pygame.mixer.Sound(file=self.file_path)
        #self.sound.set_volume(self.volume)
        
    def onCall(self):
        self.play()
          
    def stop(self):
        self.sound.stop()

    def play(self):
        self.sound.play()

    def fadeout(self,t=1.0):
        self.sound.fadeout(t)

#==================================================================

class Pygame_Channel(Action):
#==================================================================
    #NUM
    def onSetup(self):
        self.channel=pygame.mixer.Channel(self.num)

    def stop(self):
        self.sound.stop()

    def play(self):
        self.sound.play()
        
#==================================================================

class Pygame_Mixer(Action):
#==================================================================
  
    FPS=Integer(default=30)
    
    def onSetup(self):
        pygame.mixer.init(frequency=44100, size=-16, channels=2, buffer=4096)
        for i in range(pygame.mixer.get_num_channels()):
            Pygame_Channel(parent=self,name=str(i),num=i)
        self.tree()
    def onCleanup(self):
        self.stop()
        pygame.mixer.quit()
        
    def stop(self):
        pygame.mixer.stop()

    def pause(self):
        pygame.mixer.pause()

    def unpause(self):
        pygame.mixer.unpause()

    def fadeout(self,t=1.0):
        pygame.mixer.fadeout(t)


import pygame
from ..main import *
from mod_program import Action
from pygame.locals import *
from dataModel import Integer

    
class Pygame_Time(Pygame_Task):
    
    FPS=Integer(default=30)
    
    def onSetup(self):
        self.clock = pygame.time.Clock()

    def onCall(self):
        self.clock.tick(self.fps)

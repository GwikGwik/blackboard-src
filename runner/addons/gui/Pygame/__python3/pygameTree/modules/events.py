
import pygame
from mod_program import *
from pygame.locals import *
import pygame.locals
from ..main import *

            
#=============================================================

class Pygame_PressKey(Pygame_Listener):

#=============================================================
    KEY=String()

    def onSetup(self):
        self.key_code=getattr(pygame.locals,"K_"+str(self.key))
            
    def onCheck(self,event):

        if event.type == pygame.KEYDOWN:
            if event.key == self.key_code:
                return True

#=============================================================
 
class Pygame_Exit(Pygame_Listener):

#=============================================================


    def onCheck(self,event):
        if event.type == QUIT:
            return True
        if event.type == KEYDOWN and event.key == K_ESCAPE:
            return True
    
    def onCall(self,event):
        self.root.stop()
#=============================================================
 

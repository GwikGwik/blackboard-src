import os
import pygame
from pygame.locals import *
from ..main import *
from ..module import Pygame_Object
from mod_program import *
from dataModel import *

from .events import Pygame_Listener


#==================================================================

class Pygame_Click(Pygame_Listener):

#==================================================================

    
    BUTTON=Integer(default=1)
    
    def onCheck(self,event):
        if event.event.type == pygame.MOUSEBUTTONDOWN: # is some button clicked
            if event.event.button == self.button: # is left button clicked
                self.current=event.event.pos
                return True
            
    def get_position(self):
        return self.current

#==================================================================

class Pygame_SetPosition(Action):

#==================================================================
    SOURCE=String()
    OFFSET=Field(default=(0,0),eval_expr=True)           
    def onSetup(self):
        self.src=self.find(self.source)

    def onNextCall(self,node):
        x0,y0=node.parent.get_position()
        x,y=self.src.get_position()
        self.selected_node.position=(x-x0+self.offset[0],y-y0+self.offset[1])
        
#==================================================================

class Pygame_Collision(Pygame_Task):

#==================================================================
    SOURCE=String()   
    def onCall(self):
        source=self.find(self.source)
        for elt in self.parent.all().by_class(Pygame_Area).by_attr_value("clickable",True):
            if elt.active == True and elt.box.collidepoint(source.get_position()): # is mouse over button
                for handler in self.children.by_class(Action):
                    handler.set_node(elt)
                    handler.call()


#==================================================================
                    
class Pygame_Area(Object):
    
#==================================================================
    POSITION=Field(default=(  0,   0),eval_expr=True)
    CLICKABLE=Boolean(default=False)
    COLLISION=Boolean(default=False)

    def get_position(self):
        x,y=self.position
        if self.parent and isinstance(self.parent,Pygame_Area):
                x0,y0=self.parent.get_position()
                x+=x0
                y+=y0
        return x,y
    
    def render(self,surface):
        pass
    


#==================================================================
        
class Pygame_Screen(Pygame_Task):

#==================================================================
    TITLE=String(default="")
    FULLSCREEN=Boolean(default=False)
    SIZE=Field(eval_expr=True)
    
    def onSetup(self):
        """
        "Ininitializes a new pygame screen using the framebuffer"
        # Based on "Python GUI in Linux frame buffer"
        # http://www.karoltomala.com/blog/?p=679
        disp_no = os.getenv("DISPLAY")
        if disp_no:
            print ("I'm running under X display = {0}".format(disp_no))
        
        # Check which frame buffer drivers are available
        # Start with fbcon since directfb hangs with composite output
        drivers = ['fbcon', 'directfb', 'svgalib','dummy']
        found = False
        for driver in drivers:
            # Make sure that SDL_VIDEODRIVER is set
            if not os.getenv('SDL_VIDEODRIVER'):
                os.putenv('SDL_VIDEODRIVER', driver)
            try:
                pygame.display.init()
            except:
                print( 'Driver: {0} failed.'.format(driver))
                continue
            found = True
            break
    
        if not found:
            raise Exception('No suitable video driver found!')
        """

        import pygame.display
        pygame.display.init()

        if self.fullscreen==True:
            infoObject = pygame.display.Info()
            #print(        pygame.display.list_modes())
            print(infoObject.current_w, infoObject.current_h)
            self.surface = pygame.display.set_mode((infoObject.current_w, infoObject.current_h),FULLSCREEN)
        else:
            self.surface = pygame.display.set_mode(self.size)
            
        #self.surface = pygame.display.set_mode((infoObject.current_w, infoObject.current_h))
        pygame.display.set_caption(self.title)
        background = pygame.Surface(self.surface.get_size())
        self.background = background.convert()
        self.background.fill((0, 0, 0))


    def onCleanup(self):
        pygame.display.quit()
        self.surface=None

    def onCall(self):
        self.surface.blit(self.background, (0, 0))
        for elt in self.children.by_class(Pygame_Area):
            if elt.active==True:
                elt.render(self.surface)
        pygame.display.flip()

#==================================================================


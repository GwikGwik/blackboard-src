import pygame.camera
from ..module import *

class Pygame_Camera(Pygame_Device):
    
    def onSetup(self):
        self.cam = pygame.camera.Camera(self.elt,self.size)
        Pygame_DeviceOutput(name="stream",parent=self)
        
    def onStart(self):
        self.cam.start()
        
    def onStop(self):
        self.cam.stop()

class Pygame_Cameras(Pygame_Device):
    
    def onSetup(self):
        pygame.camera.init()
        self.refresh()

    def onCleanup(self):
        pass

    def refresh(self):
        camlist = pygame.camera.list_cameras()
        if camlist:
            for elt in camlist:
                cam = Pygame_Camera(parent=self,elt=elt,size=(640,480))




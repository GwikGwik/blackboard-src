from mod_program import *
from dataModel import *

class Pygame_Object(Object):

    def render(self):
        pass

    def onRender(self):
        pass


class Pygame_DeviceInput(Action):
    pass

class Pygame_DeviceOutput(Action):
    pass

class Pygame_Device(Action):


    START=Function()
    STOP=Function()

    def onCleanup(self):
        self.stop()
        
    def start(self):
        if self.alive==False:
            self.onStart()
    
    def stop(self):
        if self.alive==True:
            self.alive=False
            self.onStop()
      
    def onStart(self):pass
    def onStop(self):pass


        

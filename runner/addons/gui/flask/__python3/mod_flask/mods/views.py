from ..api import *
from ..node import *
from ..modules import *
from atom import *
from dataModel import *

import magic

#============================================================

class Api_Render(Api_Method):

#============================================================
    #-------------------------------------------------------
    def onResponse(self,request,**args):

        content=""
        for elt in self.children.by_class(Api_View):
            content+=elt.render(request=request,**args)
        return content
    #-------------------------------------------------------

#============================================================

class Api_View(Api_Element):

#============================================================

    TEMPLATE=String(default=None)
    #-------------------------------------------------------
    def get_node(self,**args):
        for elt in self.ancestors:
            if "select" in elt.keys() and elt.select is not None:
                return elt.find(elt.select)

    #-------------------------------------------------------
    def render(self,**args):

        content=""
        for elt in self.children.by_class(Api_View):
            content+=elt.render(**args)

        return self.onRender(content=content,**args)

    #----------------------------------------------------------------
    def onRender(self,content="",**args):
        if self.template:
            return self.render_template(self.template,view=self,content=content,**args)
        return content
    #-------------------------------------------------------


#============================================================

class ViewCall(Api_View):

#============================================================

    VIEW=String(default=None)

    #-------------------------------------------------------
    def onRender(self,**args):

        view=self.find(self.view)
        return view.render(**args)

    #-------------------------------------------------------

#============================================================


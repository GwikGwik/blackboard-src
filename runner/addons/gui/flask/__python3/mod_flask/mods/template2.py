
class Data_DirTemplates(Api_Method):

#============================================================
    ROOT_DIR=String(default="")
    #-------------------------------------------------------
    def onSetup(self):
        Api_Argument(parent=self,name="template")

    #-------------------------------------------------------
    def onResponse(self,template=None,**args):
        #print(self.file_path,path)
        #print(template)
        if template is None:
            return "no template"

        return self.make_template(self.root_dir+"/"+template,args=args)

    #-------------------------------------------------------

#============================================================

class Http_Api_NodeTemplates(Http_Api):

#============================================================
    ROOT_DIR=String(default="")
    #-------------------------------------------------------
    def onSetup(self):
        Api_Argument(parent=self,name="template")
        Api_Argument(parent=self,name="node")

    #-------------------------------------------------------
    def onResponse(self,template=None,node=None,**args):

        #print(template,node)

        if template is None:
            return "no template"

        if node is None:
            return "no node"

        node=self.root.find(node)
        return self.make_template(self.root_dir+"/"+template,node=node,**args)

    #-------------------------------------------------------


#============================================================

class Http_Api_ClassTemplates(Http_Api):

#============================================================
    ROOT_DIR=String(default="")
    EXT=String(default="html")
    #-------------------------------------------------------
    def onSetup(self):
        Api_Argument(parent=self,name="node")

    #---------------------------------------------------------
    def onResponse(self,node=None,**args):

        node=self.root.find(node)

        cls=node.__class__
        template=self.returnClassTemplate(cls)

        if template:
            return self.make_template(template,node=node,args=args)
        return "template not exists "+cls.__name__
    #---------------------------------------------------------
    def returnClassTemplate(self,cls):
        #print(cls)
        template_short=os.path.join(self.root_dir,cls.__name__+"."+self.ext)
        template_path=os.path.join(self.get_server().templates,template_short)
        #print(template_path)
        if os.path.exists(template_path):

            return template_short
        
        elif hasattr(cls,"__bases__"):
            for subcls in cls.__bases__:

                t=self.returnClassTemplate(subcls)
                return t
        return None

    #-------------------------------------------------------

#============================================================

class Http_Api_ClassDirsTemplates(Http_Api):

#============================================================
    ROOT_DIR=String(default="")

    #-------------------------------------------------------
    def onSetup(self):
        Api_Argument(parent=self,name="node")
        Api_Argument(parent=self,name="view")

    #---------------------------------------------------------
    def onResponse(self,node=None,view=None,**args):

        node=self.root.find(node)

        cls=node.__class__
        template=self.returnClassTemplate(cls,view)

        if template:
            return self.make_template(template,node=node,**args)
        return "template not exists "+view

    #---------------------------------------------------------
    def returnClassTemplate(self,cls,view):
        #print(cls)
        template_path=os.path.join(self.get_server().templates,self.root_dir,cls.__name__,view)
        #print(template_path)
        if os.path.exists(template_path):

            return template_path
        
        elif hasattr(cls,"__bases__"):
            for subcls in cls.__bases__:

                t=self.returnClassTemplate(subcls,view)
                if t is not None:
                    return t
        return None

    #-------------------------------------------------------
#============================================================
#============================================================

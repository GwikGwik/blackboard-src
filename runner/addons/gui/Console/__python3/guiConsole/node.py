import os
from mod_program import Action,Object
from atom import Atom

DEBUG=False

#==========================================================

class Console_Element(Object):

#==========================================================

    #-------------------------------------------------------------

    def render(self):
        for elt in self.gather_lines():
            print(elt)

    #-------------------------------------------------------------

    def gather_lines(self,**args):
        
        for elt in self.onRender(**args):
            yield elt

    #-------------------------------------------------------------

    def onRender(self,**args):
        
        for child in self.children.by_class(Console_Element):
            for string in child.gather_lines(): 
                yield string

       
    #-------------------------------------------------------------


#==========================================================

class Console_Select(Action):

#==========================================================

    FrameElement=Console_Element

    #-------------------------------------------------------------

    def onCall(self):
        for elt in self.render():
            print(elt)

    #-------------------------------------------------------------

    def render(self,**args):
        
        for elt in self.onRender(selected=self.selected_node,**args):
            yield elt
    #-------------------------------------------------------------

    def onRender(self,selected=None,**args):
        pass
       
    #-------------------------------------------------------------


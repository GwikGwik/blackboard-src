# -*- coding: utf-8 -*-
from console.node import RenderNode

#=====================================================================

class Title(RenderNode):

#=====================================================================

    def onRender(self,root=None,title_line="-",text=None,**args):
        yield ""
        yield " "*self.console_space()+text                
        yield " "*self.console_space()+title_line*self.console_size()                
        yield ""



#=====================================================================

class Text(RenderNode):

#=====================================================================

    def onRender(self,root=None,text="",**args):

        size=self.console_size()

        for elt in self.cut(text,size):
            yield " "*self.console_space()+elt               
    
    def cut(self,line,size):
        i=0
        l=len(line)
        while i+size<l:

            yield line[i:i+size]
            i+=size
        yield line[i:]


#=====================================================================

class Line(RenderNode):

#=====================================================================

    def onRender(self,root=None,title_line="-",**args):

        yield title_line*self.console_size()

#=============================================================





















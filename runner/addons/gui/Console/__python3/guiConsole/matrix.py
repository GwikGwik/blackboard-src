#========================================================================

class TermRender_Matrix(dict):

#========================================================================

    def __init__(self,size,default=" "):
        self.size=size
        for i in range(size[0]):
            for j in range(size[1]):
                self[(i,j)]=default


    def show(self):

        for i in range(self.size[0]):

            line=self.getLine(i)
            print(line)

    def getLines(self):
        string=""
        for i in range(self.size[0]):
            yield self.getLine(i)

    def setStringLine(self,i,string,offset=(0,0)):

        if i < (self.size[0]-offset[0]):
            l=len(string)
            for j in range(l):
                self[(offset[0]+i,offset[1]+j)]=string[j]

    def getLine(self,i):
        string=""
        for j in range(self.size[1]):
            string+=self[(i,j)]
        return string

    def setLine(self,i,char):
        for j in range(self.size[1]):
            self[(i,j)]=char

    def getColumn(self,j):
        string=""
        for i in range(self.size[1]):
            string+=self[(i,j)]
        return string

    def setColumn(self,j,char):
        for i in range(self.size[0]):
            self[(i,j)]=char



    def borders(self,char):
        self.setLine(0,char)
        self.setLine(self.size[0]-1,char)
        self.setColumn(0,char)
        self.setColumn(self.size[1]-1,char)


    def add_matrix(self,sub_matrix,position):
        i=0
        for line in sub_matrix.getLines():
            self.setStringLine(i,line,offset=position)
            i+=1
                
#========================================================================


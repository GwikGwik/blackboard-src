from mod_program import Action,SimpleCommand
from dataModel import *
#========================================================

class Dialog(Action):
    
#========================================================

    TITLE=String()
    ATTR=String(default="__dialog__")

    def onSetup(self):
        pass

    def onNextCall(self,node):
        node[self.attr]=self.ask(node)


    def ask(self,node):
        pass

import os
#==========================================================================

class Clear(SimpleCommand):

#==========================================================================
    """
    clear console screen
    """
    def onCall(self):
        os.system("clear") 

#==========================================================================

class PressKey(SimpleCommand):

#==========================================================================
    """
    wait for Enter key to continue
    """
    TITLE=String()

    def onCall(self):
        input(self.title)
      
#==========================================================================

#import Tkinter
from tkinter import filedialog
from .model import Dialog
from dataModel import *
#========================================================

class Dialog_DiscObject(Dialog):
    
#========================================================

    FILE_PATH=String()
    TITLE=String()



#========================================================

class Dialog_Directory(Dialog_DiscObject):
    
#========================================================


    def ask(self):
        
        return filedialog.askdirectory(initialdir=self.file_path,title=self.title)

            
            
#========================================================

class Dialog_OpenFile(Dialog_DiscObject):
    
#========================================================
    "return a local file object directly"

    def ask(self):
        return filedialog.askopenfile(initialdir=self.file_path,mode="rb",title=self.title)

#========================================================

class Dialog_SaveAsFile(Dialog_DiscObject):
    
#========================================================
    """
    plot a message when file exists
    """



    def addFileExt(self,name,ext):
        "ext with *.ext format"
        self.filefilter.append((name,ext))

    def ask(self):
        return  filedialog.asksaveasfilename(initialdir=self.file_path,filetypes=self.filefilter,title=self.title)

#========================================================

import os

from guiConsole.elements import Console_Render
#==========================================================================

class Console_Screen(Console_Render):

#==========================================================================


    def onRender(self,selected=None,**args):

        self.menu.title(selected.path())
        #args.update(self.data)
        os.system("clear")

        while self.onShow(node=self.selected,**args):
            self.menu.strtitle=self.selected.path()

            pass#self.message(**args)
            os.system("clear")
        os.system("clear")


    def onRenderChildren(self,**args):
        pass


    def onShow(self,**args):
        self.menu.header()
        self.menu.footer()
        self.menu.enter_string("press key")
        return False



#==========================================================================

class Console_ScreenSequence(Console_Screen):

#==========================================================================

    def onShow(self,**args):

        for screen in self.children:
            screen()
        return False
    #----------------------------------------------------------------

#==========================================================================

class Console_ScreenSelector(Console_Screen):

#==========================================================================

    def onShow(self,**args):
        lst=['..','quit']
        lst.extend(self.keys())

        name=self.menu.menu(lst)

        if name=="quit":
            return False
        elif name=="..":
            return False


        elif name in self.keys():
            screen=self[name]
            screen()
            
        else:
            return False

        return True
    #----------------------------------------------------------------

#==========================================================================


from .objects import * 
from tkinter import ttk
import tkinter

#=============================================================

class Tk_Canvas(Tk_Object):

#=============================================================

    HEIGHT=Integer(default=500)
    WIDTH=Integer(default=300)
    BG=String(default="black")

    def onRender(self,root,**args):

        obj=tkinter.Canvas(root,height=self.height, width=self.width, bg=self.bg,**args)
        self.plot(obj)
        return obj

    def plot(self,root):
        self.children.by_class(Tk_CanvasObject).execute("plot",root)

#=============================================================

class Tk_CanvasObject(Object):

#=============================================================

    """
     width = épaisseur du contour, 
    fill = couleur de remplissage, 
    outline = couleur de contour, 
    activefill = couleur de remplissage au survol de la souris, 
    state = NORMAL, DISABLED, HIDDEN (un objet peut être caché, désactivé).
    """

    WIDTH=Integer(default=1)
    FILL=String()
    OUTLINE=String(default="white")
    ACTIVEFILL=String()

    def plot(self,root):

        self.obj=self.onPlot(root,
                        width=self.width,
                        fill=self.fill,
                        outline=self.outline,
                        activefill=self.activefill)

#=============================================================

class Tk_CanvasArc(Tk_CanvasObject):

#=============================================================

    def onPlot(self,root,**options):
        coord=[10, 50, 240, 210]
        self.obj=root.create_arc(coord, start=0, extent=150,**options)

#=============================================================

class Tk_CanvasImage(Tk_CanvasObject):

#=============================================================

    IMAGE=String()

    def onPlot(self,root,**options):
        filename = tkinter.PhotoImage(file = self.image)
        self.obj= root.create_image(50, 300, anchor="nw", image=filename)


#=============================================================

class Tk_CanvasLine(Tk_CanvasObject):

#=============================================================

    def onPlot(self,root,outline=None,**options):
        coords=[100,100,100,500]
        self.obj=root.create_line(*coords, fill=outline)

#=============================================================

class Tk_CanvasOval(Tk_CanvasObject):

#=============================================================

    def onPlot(self,root,**options):
        coords=[200,200,400,400]
        self.obj= root.create_oval(*coords, **options)

#=============================================================

class Tk_CanvasArcPolygon(Tk_CanvasObject):

#=============================================================

    def onPlot(self,root,**options):
        coords=[0,0,1,1,0,2]
        self.obj=root.create_polygon(*coords, **options)




#=============================================================

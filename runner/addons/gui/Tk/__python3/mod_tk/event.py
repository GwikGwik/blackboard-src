from dataModel import *
from mod_program import *


#=============================================================

class Tk_Event(Object):

#=============================================================
    #https://effbot.org/tkinterbook/tkinter-events-and-bindings.htm
    EVENT=None
    DETAIL=String(default=None)

    def onBind(self):
        if self.detail:
            name="<"+self.EVENT+"-"+self.detail+">"
        else:
            name="<"+self.EVENT+">"
        print("bind",self.path())
        self.parent.obj.bind(name,self.onCall)

    def onCall(self,event):
        print("event",self.path(),event)
        self.children.by_class(Code).execute("call")

#=============================================================

class Tk_KeyPress(Tk_Event):EVENT="KeyPress"
class Tk_KeyRelease(Tk_Event):EVENT="KeyRelease"
class Tk_ButtonPress(Tk_Event):EVENT="Button"
class Tk_ButtonRelease(Tk_Event):EVENT="ButtonRelease"
class Tk_Enter(Tk_Event):EVENT="Enter"
class Tk_Leave(Tk_Event):EVENT="Leave"
class Tk_Return(Tk_Event):EVENT="Return"
class Tk_TreeviewSelect(Tk_Event):EVENT="<TreeviewSelect>"
class Tk_TreeviewOpen(Tk_Event):EVENT="<TreeviewOpen>"
class Tk_TreeviewClose(Tk_Event):EVENT="<TreeviewClose>"
 

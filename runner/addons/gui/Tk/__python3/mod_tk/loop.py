from dataModel import *
from mod_program import *
from tkinter import ttk
import tkinter
from ttkthemes import ThemedTk
from .objects import * 
#=============================================================

class Tk_Loop(Action):

#=============================================================
    """
    https://forums.wikitechy.com/question/tkinter-python-maximize-window/
    """

    WIDTH=Integer(default=1000)
    HEIGHT=Integer(default=400)

    def onSetup(self):
        self.obj = None

    def onCall(self):
        #self.root_obj = tkinter.Tk()
        self.obj = ThemedTk(theme="equilux",themebg=True,toplevel=True)
        self.obj.title(self.name)
        self.full()
        #self.normal()
        #self.ajust()

        self.children.by_class(Tk_Object).execute("render")
        self.obj.mainloop() 
        #self.obj.destroy()

    def max_size(self,w,h):
        self.obj.maxsize(w,h)

    def geometry(self,w,h):
        self.obj.geometry("%sx%s"%(str(w),str(h)))

    def ajust(self):
        self.obj.state('normal')

    def full(self):
        """fill one screen with bar window on top"""
        self.obj.attributes('-zoomed', True)

    def fullscreen_on(self):
        self.obj.attributes('-fullscreen', True)

    def fullscreen_off(self):
        self.obj.attributes('-fullscreen', False)

    def wide(self):
        """fill all screens with bar window on top"""
        w, h = self.obj.winfo_screenwidth(), self.obj.winfo_screenheight()
        self.obj.geometry("%dx%d+0+0" % (w, h))



#============================================================= 

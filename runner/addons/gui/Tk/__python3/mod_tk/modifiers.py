
from dataModel import *
from mod_program import *

#=============================================================

class Tk_Modifier(Object):

#=============================================================
    
    def onPack(self):
        pass



#=============================================================

class Tk_Pack(Tk_Modifier):

#=============================================================

    SIDE=String(default=None)#"top"
    FILL=String(default=None)#"both" x y
    EXPAND=Integer(default=None)#0 1

    def onPack(self):
        self.parent.obj.pack(side=self.side,fill=self.fill, expand=self.expand)

#=============================================================

class Tk_Grid(Tk_Modifier):

#=============================================================

    COLUMNS=Integer(default="12")
    ROWS=Integer(default="12")

    def onPack(self):

        for column in range(self.columns):
            self.parent.obj.columnconfigure(column, weight=1)

        for row in range(self.rows):
            self.parent.obj.rowconfigure(row, weight=1)

#=============================================================

class Tk_PackGrid(Tk_Modifier):

#=============================================================
    COLUMN=Integer(default=None)
    COLUMNSPAN=Integer(default=None)
    ROW=Integer(default=None)
    ROWSPAN=Integer(default=None)
    PADX=Integer(default=None)
    PADY=Integer(default=None)
    IPADX=Integer(default=None)
    IPADY=Integer(default=None)
    STICKY=String(default=None)#"nsew""nw"

    def onPack(self):
        self.parent.obj.grid(column=self.column,columnspan=self.columnspan,rowspan=self.rowspan,row=self.row,
                    padx=self.padx,pady=self.pady,ipady=self.ipady,ipadx=self.ipadx,sticky=self.sticky)

#=============================================================

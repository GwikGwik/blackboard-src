

from dataModel import *
from mod_program import *
from tkinter import ttk
import tkinter
from PIL import Image, ImageTk
from .modifiers import *
from .event import *


#=============================================================

class Tk_Object(Object):

#=============================================================

    PACK=True
    TK_CLASS=String(default="Label")


    def onSetup(self):
        pass

    def render(self):
        args=dict()

        self.obj=self.onRender(self.parent.obj,**args)
        if len(self.children.by_class(Tk_Modifier))>0:
            self.children.by_class(Tk_Modifier).execute("onPack")
        elif self.PACK:
            self.obj.pack()

        self.children.by_class(Tk_Event).execute("onBind")
        self.children.by_class(Tk_Object).execute("render")


    def onRender(self,root,**args):
        return getattr(tkinter,self.tk_class)(root,**args)


    def destroy_tk(self):
        self.obj.destroy()

#=============================================================

class Tk_Label(Tk_Object):

#=============================================================

    TEXT=String(default="")

    def onRender(self,root,**args):

        args["text"]=self.text
        obj=ttk.Label(root,**args)
        return obj
# Menubutton, Radiobutton, Scale and Scrollbar
#=============================================================

class Tk_Image(Tk_Object):

#=============================================================
    """
    #https://stackoverflow.com/questions/10133856/how-to-add-an-image-in-tkinter
    """
    IMAGE=String(default="")
    X=Integer(default=200)
    Y=Integer(default=200)

    #def onSetup(self):
    #    Tk_Pack(parent=self,side = "bottom", fill = "both", expand = 1)

    def onRender(self,root,**args):

        load = Image.open(self.image)
        load=load.resize((self.x,self.y), Image.ANTIALIAS)
        render = ImageTk.PhotoImage(load)
        args["image"]=render
        args["compound"]="image"
        obj=ttk.Label(root,**args)
        obj.image = render
        #obj.place(x=0, y=0)
        return obj
#=============================================================

class Tk_Button(Tk_Object):

#=============================================================

    TEXT=String(default="")

    def onRender(self,root,**args):

        args["text"]=self.text
        obj=ttk.Button(root,command=self.onPush,**args)
        return obj

    def onPush(self):
        r=self.children.by_class(Code).execute("call")

#=============================================================

class Tk_MenuObject(Object):

#=============================================================
    def onRender(self,root,**args):
        pass
#=============================================================

class Tk_MenuCommand(Object):

#=============================================================
    TEXT=String(default="")
    def onRender(self,root,**args):
        root.add_command ( label=self.text,command=self.onPush)

    def onPush(self):
        r=self.children.by_class(Code).execute("call")

#=============================================================

class Tk_MenuButton(Tk_Object):

#=============================================================

    TEXT=String(default="")

    def onRender(self,root,**args):

        args["text"]=self.text
        obj=ttk.Menubutton(root,**args)
        menu=tkinter.Menu(obj,bg="dimgrey",fg="coral")
        obj["menu"] = menu
        for child in self.children.by_class(Tk_MenuCommand):
            child.onRender(menu)
        return obj

#=============================================================

class Tk_Field(Tk_Object):

#=============================================================

    def onRender(self,root,**args):

        self.value = None
        obj  =None
        return obj

    def get_value(self):
        return self.value.get()

    def set_value(self,v):
        return self.value.set(v)

#=============================================================

class Tk_Entry(Tk_Field):

#=============================================================

    def onRender(self,root,**args):

        self.value = tkinter.StringVar(root)
        obj  = ttk.Entry(root, textvariable=self.value,**args)
        return obj

#=============================================================

class Tk_Check(Tk_Field):

#=============================================================

    ONVALUE=Field()
    OFFVALUE=Field()

    def onRender(self,root,**args):
        args["onvalue"]=self.onvalue
        args["offvalue"]=self.offvalue
        self.value = tkinter.BooleanVar(root,'1')
        obj  = ttk.Checkbutton(root, variable=self.value,command=self.onCheck,**args)
        return obj

    def onCheck(self):
        r=self.children.by_class(Code).execute("call")

#=============================================================

class Tk_Tree(Tk_Field):

#=============================================================

    SELECT=String()


    def onRender(self,root,**args):
        
        tree=ttk.Treeview(root)#,style="mystyle.Treeview")
        #tree.tag_configure('odd', background='#E8E8E8')
        #tree.tag_configure('even', background='#DFDFDF')

        #tree["columns"]=("one","two","three")
        #tree.column("#0", width=150, minwidth=100, stretch=False)
        #tree.column("one", width=150, minwidth=150, stretch=ttk.NO)
        #tree.column("two", width=400, minwidth=200)
        #tree.column("three", width=80, minwidth=50, stretch=tkinter.NO)
        tree.heading("#0",text=self.name,anchor="w")
        #tree.heading("one", text="Date modified",anchor=tkinter.W)
        #tree.heading("two", text="Type",anchor=tkinter.W)
        #tree.heading("three", text="Size",anchor=tkinter.W)

        # Level 1
        #folder1=tree.insert("",8, None, text="Folder 1", values=("23-Jun-17 11:05","File folder",""))
        #tree.insert("", 8, None, text="text_file.txt", values=("23-Jun-17 11:25","TXT file","1 KB"))
        # Level 2
        #tree.insert(folder1, 8, None, text="photo1.png", values=("23-Jun-17 11:28","PNG file","2.6 KB"))
        #tree.insert(folder1, 8,None, text="photo2.png", values=("23-Jun-17 11:29","PNG file","3.2 KB"),tags = ('even',))
        #tree.insert(folder1, 8, None, text="photo3.png", values=("23-Jun-17 11:30","PNG file","3.1 KB"),tags = ('odd',))


        self.build_tree(tree,"",self.find(self.select))


        return tree

    def build_tree(self,tree,obj,node):
        obj2=tree.insert(obj,40000, "/"+node.path(), text=node.name)
        for child in node.children:
            self.build_tree(tree,obj2,child)

#=============================================================
from ttkwidgets import DebugWindow
class Tk_Debug(Tk_Field):

#=============================================================

    def onRender(self,root,**args):

        obj  = DebugWindow(root,**args)
        obj.pack(side="top",fill='both', expand=1)
        return obj
#=============================================================
from ttkwidgets import TimeLine
class Tk_TimeLine(Tk_Field):

#=============================================================

    def onRender(self,root,**args):



        obj = TimeLine(root, categories={str(key): {"text": "Category {}".format(key)} for key in range(0, 5)}, extend=True        )
        menu = tk.Menu(root, tearoff=False)
        menu.add_command(label="Some Action", command=lambda: print("Command Executed"))
        obj.tag_configure("1", right_callback=lambda *args: print(args), menu=menu, foreground="green",
                               active_background="yellow", hover_border=2, move_callback=lambda *args: print(args))
        obj.create_marker("1", 1.0, 2.0, background="white", text="Change Color", tags=("1",), iid="1")
        obj.create_marker("2", 2.0, 3.0, background="green", text="Change Category", foreground="white", iid="2",
                               change_category=True)
        obj.create_marker("3", 1.0, 2.0, text="Show Menu", tags=("1",))
        obj.create_marker("4", 4.0, 5.0, text="Do nothing", move=False)
        obj.draw_timeline()
        obj.grid()
        return obj
#=============================================================






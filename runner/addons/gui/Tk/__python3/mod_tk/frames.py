from .objects import * 
from tkinter import ttk
import tkinter
#=============================================================

class Tk_Pane(Tk_Object):

#=============================================================

    ORIENTATION=String(default=None)#"horizontal")#vertical
    def render(self):
        print(self.path(),"render")
        args=dict()

        self.obj=self.onRender(self.parent.obj,**args)
        self.children.by_class(Tk_Event).execute("onBind")

        for child in self.children.by_class(Tk_Object):
            if isinstance(child,Tk_Pane)==True:
                child.render()
                self.obj.add(child.obj)
            else:
                child.obj=child.onRender(self.obj)
                self.obj.add(child.obj)
                child.children.by_class(Tk_Object).execute("render")
                child.children.by_class(Tk_Event).execute("onBind")
        if isinstance(self.parent,Tk_Pane) !=True:
            if len(self.children.by_class(Tk_Modifier))>0:
                self.children.by_class(Tk_Modifier).execute("onPack")
            else:
                self.obj.pack()

    def onRender(self,root,**args):

        obj=ttk.PanedWindow(root,orient=self.orientation,**args)
        return obj
#=============================================================

class Tk_Tabs(Tk_Object):

#=============================================================

    def onRender(self,root,**args):

        obj=ttk.Notebook(root,**args)

        return obj
#=============================================================

class Tk_Tab(Tk_Object):

#=============================================================

    PACK=False
    def onRender(self,root,**args):

        obj=ttk.Frame(root,**args)
        self.parent.obj.add(obj,text=self.name)
        return obj
#=============================================================

class Tk_LabelFrame(Tk_Object):

#=============================================================
    BORDERWIDTH=Integer(default=10)
    PADDING=Integer(default=5)
    def onRender(self,root,**args):

        obj=ttk.LabelFrame(root,text=self.name,borderwidth=self.borderwidth,padding=self.padding,**args)

        return obj
#=============================================================

class Tk_Frame(Tk_Object):

#=============================================================
    BORDERWIDTH=Integer(default=10)
    PADDING=Integer(default=5)
    def onRender(self,root,**args):

        obj=ttk.Frame(root,borderwidth=self.borderwidth,padding=self.padding,**args)

        return obj

#============================================================= 

APT python3-tk

#https://ttkthemes.readthedocs.io/en/latest/
PIP3 "ttkthemes"

#https://ttkwidgets.readthedocs.io/en/latest/ttkwidgets/ttkwidgets.html#ttkwidgets
PIP3 "ttkwidgets"
#timeline, calendar

#https://github.com/Dogeek/tkinter-pp
PIP3 "tkinterpp"
#DirTree


#https://tkcanvasgraph.readthedocs.io/en/latest/index.html
PIP3 "tkCanvasGraph"

#https://github.com/bauripalash/tkhtmlview
PIP3 "tkhtmlview"

#https://github.com/DeflatedPickle/pkinter
PIP3 "pkinter"

#https://pypi.org/project/supercanvas/
PIP3 "supercanvas"

#http://www.leandromattioli.com.br/witkets/
PIP3 "witkets"

#https://pypi.org/project/AwesomeTkinter/
PIP3 "AwesomeTkinter" # (2020.9.22)     - Pretty tkinter widgets

#https://pypi.org/project/tkMagicGrid/
PIP3 "tkMagicGrid" # (1.0.3)            - Table layout widget for Tkinter

#https://pypi.org/project/tkDataCanvas/
PIP3 "tkDataCanvas"

#https://github.com/revarbat/belfrywidgets
PIP3 "belfrywidgets"

#http://www.xavierdupre.fr/app/tkinterquickhelper/helpsphinx/index.html
#http://www.xavierdupre.fr/app/tkinterquickhelper/helpsphinx/README.html#l-readme
#http://www.xavierdupre.fr/app/pyquickhelper/helpsphinx/i_ex.html#l-ex2
#http://www.xavierdupre.fr/app/pyquickhelper/helpsphinx/all_notebooks.html#l-notebooks
PIP3 "tkinterquickhelper" #(1.5.72)    - Helpers for tkinter, extra windows.

#https://github.com/samyzaf/xcanvas/blob/master/test.py
PIP3 "xcanvas"

#https://pypi.org/project/pyopengltk/
PIP3 "pyopengltk"

#https://github.com/bentlema/tkshapes
PIP3 "tkshapes"




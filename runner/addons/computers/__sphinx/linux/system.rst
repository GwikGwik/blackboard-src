##################################################
explorer le systeme
##################################################




.. autoclass:: linux.system.LinuxSystem

la machine
====================================

.. automethod:: linux.system.LinuxSystem.computer
.. automethod:: linux.system.LinuxSystem.server
.. automethod:: linux.system.LinuxSystem.discs
.. automethod:: linux.system.LinuxSystem.files


les composants
====================================

.. automethod:: linux.system.LinuxSystem.users
.. automethod:: linux.system.LinuxSystem.groups
.. automethod:: linux.system.LinuxSystem.processes
.. automethod:: linux.system.LinuxSystem.services
.. automethod:: linux.system.LinuxSystem.aptitude



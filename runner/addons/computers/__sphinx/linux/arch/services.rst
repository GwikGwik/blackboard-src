##################################################
gerer les services
##################################################

lister les services
=======================

.. literalinclude:: process.py
   :language: python

.. autoclass:: linux.arch.services.Linux_ServiceManager


la classe Service
=======================

.. autoclass:: linux.arch.services.Linux_Service

cycle de vie
----------------------------------

.. automethod:: linux.arch.services.Linux_Service.install
.. automethod:: linux.arch.services.Linux_Service.uninstall

.. automethod:: linux.arch.services.Linux_Service.activate
.. automethod:: linux.arch.services.Linux_Service.unactivate

.. automethod:: linux.arch.services.Linux_Service.start
.. automethod:: linux.arch.services.Linux_Service.stop
.. automethod:: linux.arch.services.Linux_Service.restart

##################################################
explorer le systeme
##################################################




.. autoclass:: linux.system.LinuxSystem

le serveur
====================================

.. automethod:: linux.system.LinuxSystem.computer

les fichiers
====================================

.. automethod:: linux.system.LinuxSystem.discs
.. automethod:: linux.system.LinuxSystem.files

les éléments linux
====================================

.. automethod:: linux.system.LinuxSystem.users
.. automethod:: linux.system.LinuxSystem.groups
.. automethod:: linux.system.LinuxSystem.processes
.. automethod:: linux.system.LinuxSystem.services



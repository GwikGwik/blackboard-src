##################################################
utilisateurs et groupes
##################################################

les utilisateurs
==========================

.. automethod:: linux.arch.user.get_users

.. autoclass:: linux.arch.user.Linux_User

.. automethod:: linux.arch.user.Linux_User.__init__
.. automethod:: linux.arch.user.Linux_User.create
.. automethod:: linux.arch.user.Linux_User.remove


les groupes
==========================

.. automethod:: linux.arch.group.get_groups

.. autoclass:: linux.arch.group.Linux_Group

.. automethod:: linux.arch.group.Linux_Group.__init__
.. automethod:: linux.arch.group.Linux_Group.create
.. automethod:: linux.arch.group.Linux_Group.remove

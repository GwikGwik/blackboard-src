##################################################
voir les processus
##################################################


lister les processus
=======================

.. literalinclude:: process.py
   :language: python

.. autoclass:: linux.arch.process.LinuxProcesses

la classe LinuxProcess
====================================

.. autoclass:: linux.arch.process.Linux_Process

.. automethod:: linux.arch.process.Linux_Process.kill

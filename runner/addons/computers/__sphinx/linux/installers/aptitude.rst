##################################################
aptitude
##################################################

la classe Aptitude
==================================

.. autoclass:: linux.installers.aptitude.Aptitude

rechercher dans les paquets
------------------------------

.. automethod:: linux.installers.aptitude.Aptitude.search
.. automethod:: linux.installers.aptitude.Aptitude.installed
.. automethod:: linux.installers.aptitude.Aptitude.aviable
.. automethod:: linux.installers.aptitude.Aptitude.cache

outils pour l'os
------------------------------

.. automethod:: linux.installers.aptitude.Aptitude.update
.. automethod:: linux.installers.aptitude.Aptitude.dist_upgrade
.. automethod:: linux.installers.aptitude.Aptitude.safe_upgrade
.. automethod:: linux.installers.aptitude.Aptitude.ull_upgrade
.. automethod:: linux.installers.aptitude.Aptitude.repare
.. automethod:: linux.installers.aptitude.Aptitude.clean
.. automethod:: linux.installers.aptitude.Aptitude.autoclean
.. automethod:: linux.installers.aptitude.Aptitude.cleanAlt


la classe AptitudePackage
==================================

.. autoclass:: linux.installers.aptitude.AptitudePackage

.. automethod:: linux.installers.aptitude.AptitudePackage.is_installed
.. automethod:: linux.installers.aptitude.AptitudePackage.infos
.. automethod:: linux.installers.aptitude.AptitudePackage.install
.. automethod:: linux.installers.aptitude.AptitudePackage.remove
.. automethod:: linux.installers.aptitude.AptitudePackage.download


la classe AptitudePackageGroup
==================================

.. autoclass:: linux.installers.aptitude.AptitudePackageGroup

.. automethod:: linux.installers.aptitude.AptitudePackage.infos
.. automethod:: linux.installers.aptitude.AptitudePackage.install
.. automethod:: linux.installers.aptitude.AptitudePackage.remove
.. automethod:: linux.installers.aptitude.AptitudePackage.download


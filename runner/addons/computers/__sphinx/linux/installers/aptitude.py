import settings
from informatics.system.linux.aptitude import Aptitude
from system.menu import Menu

menu=Menu(settings.local_libs_path)
installer=Aptitude()
classes=dict(menu.help_instance(installer))

for k,v in classes.items():
    print k

action = classes["ListElements"]()
action.select(installer)
result=action()
print result.data
node=result.getContent()
node.show()

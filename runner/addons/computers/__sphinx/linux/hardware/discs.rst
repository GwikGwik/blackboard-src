##################################################
les disques
##################################################


parcourir les disques
==================================

.. literalinclude:: computer.py
   :language: python

.. autoclass:: linux.files.discs.DiscManager


la classe Disc
==================================

.. autoclass:: linux.files.discs.Disc

voir les fichiers
-----------------------

.. automethod:: linux.files.discs.Disc.getFiles

montage / demontage
-----------------------

.. automethod:: linux.files.discs.Disc.mount
.. automethod:: linux.files.discs.Disc.unmount

la classe DiscElement
==================================

.. autoclass:: linux.files.discs.DiscElement

voir les infos
-----------------------

.. automethod:: linux.files.discs.DiscElement.infos

modif
-----------------------

.. automethod:: linux.files.discs.DiscElement.create
.. automethod:: linux.files.discs.DiscElement.remove
.. automethod:: linux.files.discs.DiscElement.copy



#####################################
les caractéristiques de l'ordinateur
#####################################


appeler l'objet
==================================

.. literalinclude:: computer.py
   :language: python

.. autoclass:: linux.hardware.computer.Computer

la classe Computer
==================================

.. autoclass:: linux.hardware.computer.ComputerInfos

renomer la machine
-----------------------

.. automethod:: linux.hardware.computer.Computer.rename

controler la machine
-----------------------

.. automethod:: linux.hardware.computer.Computer.shutdown
.. automethod:: linux.hardware.computer.Computer.cancelShutdown
.. automethod:: linux.hardware.computer.Computer.reboot

hardware
-----------------------

.. automethod:: linux.hardware.computer.Computer.cpus
.. automethod:: linux.hardware.computer.Computer.memory
.. automethod:: linux.hardware.computer.Computer.devices
.. automethod:: linux.hardware.computer.Computer.pci
.. automethod:: linux.hardware.computer.Computer.usb

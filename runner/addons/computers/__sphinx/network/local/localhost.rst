#####################################
inspecter sa machine
#####################################


exemple
==================================

.. literalinclude:: localhost.py
   :language: python



Localhost
==================================

.. autoclass:: network.localhost.Net_Localhost

.. automethod:: network.localhost.Net_Localhost.publicip
.. automethod:: network.localhost.Net_Localhost.scan
.. automethod:: network.localhost.Net_Localhost.network_interfaces
.. automethod:: network.localhost.Net_Localhost.active_connections



##################################################
explorer une machine
##################################################

exemple
==================================

.. literalinclude:: machine.py
   :language: python



NetworkMachine
==================================

.. autoclass:: network.machines.Net_Machine

accessibilité
------------------------------

.. automethod:: network.machines.Net_Machine.state
.. automethod:: network.machines.Net_Machine.ports

environement
------------------------------

.. automethod:: network.machines.Net_Machine.whois
.. automethod:: network.machines.Net_Machine.ping
.. automethod:: network.machines.Net_Machine.traceroute


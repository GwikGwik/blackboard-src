##################################################
trouver les machines
##################################################


exemple
==================================

.. literalinclude:: machines.py
   :language: python

Network
==================================

.. autoclass:: network.machines.Network

LocalNetwork
==================================

.. autoclass:: network.localnetwork.LocalNetwork
.. automethod:: network.localnetwork.LocalNetwork.machines






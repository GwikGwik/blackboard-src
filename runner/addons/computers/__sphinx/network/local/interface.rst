#####################################
trouver les interfaces réseau
#####################################


exemple
==================================

.. literalinclude:: interface.py
   :language: python

.. autoclass:: network.interfaces.NetworkInterfaces

la classe NetworkInterface
==================================

.. autoclass:: network.interfaces.NetworkInterface

informations
-----------------------

.. automethod:: network.interfaces.NetworkInterface.config
.. automethod:: network.interfaces.NetworkInterface.infos

utiliser la connection
-----------------------

.. automethod:: network.interfaces.NetworkInterface.start
.. automethod:: network.interfaces.NetworkInterface.stop
.. automethod:: network.interfaces.NetworkInterface.status



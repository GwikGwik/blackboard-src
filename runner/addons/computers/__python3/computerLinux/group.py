

from dataModel import *
from atom import Atom,Atom_Link,Atom_Manager_List
from mod_program import *

#========================================================================

class Linux_Group(Object):

#========================================================================
    """
    correspond a un groupe du systeme linux
    les commandes linux associes sont dans les fonctions
    """

    ID=String()
    GROUPNAME=String()
    CHAMP_SPECIAL=String()
    USERS=Field()

    @command
    def create(self):
        return "groupadd $(groupname)s"

    @command
    def remove(self):
        return "groupdel $(groupname)s"

#========================================================================

class Linux_Groups(UpdateTree):

#========================================================================



    CLS_DEFAULT="Linux_Group"



    def onIterData(self,node):

        for string in open("/etc/group","r"):
            string=string.replace("\n","")

            infos=dict()
            nom_de_groupe,champ_special,numero_de_groupe,membres = string.strip().split (":")
            name=nom_de_groupe
            infos["groupname"]=nom_de_groupe
            infos["id"]=numero_de_groupe
            infos["champ_special"]= champ_special
            infos["users"]= membres.strip().split(",")

            #print membres,infos["users"]
            yield self.name+"/"+name,infos

#========================================================================



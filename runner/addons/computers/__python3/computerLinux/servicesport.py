
from atom import Atom

from mod_program import Object


#====================================================================

class Linux_ServicePortList(Object):

#====================================================================


    #-------------------------------------------------
    def on_update(self,**args):
        
        f=open("/etc/services","r")
        lines=f.readlines()
        f.close()
        return self.parse(lines)

    #-------------------------------------------------

    def parse(self,lines):

        group=Object()
        yield group
        lst=[]
        g=True

        for line in lines:

            line=line.replace("\n","")
            line=line.replace("\t"," ")
            line=line.strip()

            #ligne vide
            if line =="":
                pass

            #if comment
            elif line.startswith("#"):

                line=line[1:]

                #empty comment
                if line=="":
                    pass

                #head comment
                elif "="*10 in line :
                    pass

                #first comment
                elif g == False:

                    #if has comment before
                    if len(lst)>0:

                        #first comment passed
                        g=True

                        #content
                        group.update(content="\n".join(lst))

                        #name
                        name=lst[0]
                        name=name=name.strip()
                        name=name.replace(" ","_")
                        name=name.split(",")[0]
                        name=name.replace("/","_")
                        name=name.replace(":","")
                        name=name.replace('"',"")
                        name=name.replace("'","")
                        name=name.replace("`","")
                        group.update(name=name)

                        #new group
                        group=Net_ServicePort_Group()
                        yield group
                        lst=[]
                        lst.append(line)

                #other comments
                else:

                    lst.append(line)

            #port infos
            else:

                #end of comment
                if g == True:g=False


                #comment at the end of line
                if "#" in line:
                    l,comment=line.split("#")
                else:
                    l,comment=line,None

                ls=l.split(" ")
                ls=[txt for txt in ls if txt.strip() !=""]

                if len(ls)==2:
                    user,port,service=ls[0].strip(),ls[1].strip(),None

                elif len(ls)>2:
                    user,port,service=ls[0].strip(),ls[1].strip()," ".join(ls[2:])


                port,protocole=port.split("/")
                Object(name=user,user=user,port=port,protocole=protocole,service=service,comment=comment,parent=group)

        if len(lst)>0:
            group.update(content="\n".join(lst))
            name=lst[0]
            name=name=name.strip()
            name=name.replace(" ","_")
            name=name.split(",")[0]
            name=name.replace("/","_")
            group.update(name=name)

#====================================================================

class Linux_ServicePortSearch(Object):

#====================================================================
    """
    """


    def OnCall(self,node=None,msg=None,port=22,**args):

        result=Node(name="result")

        for elt in node.by_class(ServicePort):

            if str(port) == str(elt.port):
                r=elt.copy()
                r.reparentTo(result)
        return result





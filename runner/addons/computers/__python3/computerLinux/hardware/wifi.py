
from modules import *

from dataModel import String
#========================================================================

class EthernetConnection(Object):

#========================================================================
    pass

#========================================================================

class WifiConnection(Object):

#========================================================================

    @command
    def device(self):
        return "iw dev"


    @command
    def scan(self):
        return "sudo iw %s scan"%self.name

    @command
    def config(self):
        return "iwconfig"



#========================================================================

class WifiSpot(Object):

#========================================================================

    PASSWORD=String()
    PASSFILE=String()

    @command
    def connect(self,passfile):
        return "sudo wpa_supplicant -B  -i %(name)s -c %(passfile)s"%self

    @command
    def password(self,password,passfile):

        return "wpa_passphrase %(name)s %(password)s >> %(passfile)s"%self

#========================================================================

from atom import Atom,Atom_Link
from mod_program import *
from dataModel import *

#========================================================================

class PciDevice(Object):

#========================================================================
    ADRESS=String()
    CLASSE=String()
    DESCRIPTION=String()
#========================================================================

class PciDevices(UpdateTree):

#========================================================================
    """
    info from lspci in a formated dict
    """
    CLS_DEFAULT="PciDevice"

    #----------------------------------------------------------------

    def onIterData(self,node):

        result=execute("lspci")
        for line in result.out:
            if line != "":

                adrss=line[:7]
                cls,description=line[8:].split(":")

                result={}
                result["adress"]=adrss
                result["classe"]=cls
                result["description"]=description.strip()

                yield self.name+"/"+adrss,result

    #----------------------------------------------------------------

#========================================================================

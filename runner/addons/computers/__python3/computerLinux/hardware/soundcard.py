
from atom import Atom,Atom_Link
from mod_program import *
from dataModel import *
#========================================================================

class SoundCard(Object):

#========================================================================

    ID=String()
    BUS=String()
    DEVICE=String()
    DESCRIPTION=String()

#========================================================================

class SoundCards(UpdateTree):

#========================================================================
    """
    """
    CLS_DEFAULT="SoundCard"
    #----------------------------------------------------------------

    def onIterData(self,node):

        result=execute("aplay -l").out
        for line in result:
            if line.startswith("carte"):
                line=line.replace("carte ","")
                id_=line[0]
                description=line[2:].strip()

                result={}
                result["id"]=id_
                result["description"]=description
                yield self.name+"/"+id_,result

    #----------------------------------------------------------------
#========================================================================


from .pci import *
from .usb import *
from .discs  import *
from .soundcard  import *
from .machine import *


# -*- coding: utf-8 -*-
#http://latarteauchips.free.fr/linux-programmer-arret-systeme.php


from mod_program import *

from dataModel import *

#========================================================================

def clean_name(name):

#========================================================================

    name=name.strip()
    name=name.replace(" ","")
    name=name.replace("(","")
    name=name.replace(")","")
    name=name.replace("-","")
    name=name.replace("_","")
    return name

#========================================================================

class MountedDevice(Object):

#========================================================================

    DISC=String()
    MOUNT=String()
    TYPE=String()
    OPTIONS=String()

#========================================================================

class MountedDevices(UpdateTree):

#========================================================================
    CLS_DEFAULT="MountedDevice"

    #----------------------------------------------------------------

    def onIterData(self,node):
        node=execute("mount")

        for line in node.out:
            pos_on=line.find(" on ")
            pos_type=line.find(" type ")
            pos_opt=line.find(" (")
            pos_end=line.find(")")

            name=line[:pos_on]

            result={}
            result["disc"]=name
            result["mount"]=line[pos_on+4:pos_type]
            result["type"]=line[pos_type+6:pos_opt]
            result["options"]=line[pos_opt+2:pos_end]

            yield self.name+result["mount"],result

    #----------------------------------------------------------------


#========================================================================
class Memory(UpdateChildren):

#========================================================================

    #----------------------------------------------------------------
    def onIterData(self,node):
        r=dict()
        for line in open ("/proc/meminfo", "r").readlines ():
            k,v= line.strip().split (":")
            k=clean_name(k)
            r[ k ]=v
        yield self.name,r
    #----------------------------------------------------------------

#========================================================================

class Computer_Use(UpdateChildren):

#========================================================================

    def onIterData(self,node):
        r=dict()
        line= open ("/proc/uptime", "r").readlines ()[0]
        start_time,unactive_time= line.strip().split()
        r["start_time"]=start_time
        r["unactive_time"]=unactive_time
        yield self.name,r
    #----------------------------------------------------------------

#========================================================================

class Computer_Loads(UpdateChildren):

#========================================================================
    def onIterData(self,node):

        """
        Les trois premières colonnes mesurent l'utilisation de l'unité centrale 
        en fonction des dernières périodes de 1, 5 et 10 minutes. 
        La quatrième colonne indique le nombre de processus en cours d'exécution 
        et le nombre total de processus. 
        La dernière colonne affiche le dernier ID de processus utilisé. 
        """
        node=dict()
        line= open ("/proc/loadavg", "r").readlines ()[0]
        l_1,l_5,l_10,proc,last_id= line.strip().split()
        node["l_1"]=l_1
        node["l_5"]=l_5
        node["l_10"]=l_10
        proc_used,proc_tot=proc.split("/")
        node["proc_used"]=proc_used
        node["proc_tot"]=proc_tot
        node["last_id"]=last_id
        yield self.name,node
    #----------------------------------------------------------------
#========================================================================

class Cpu(Object):

#========================================================================

    pass

#========================================================================

class Cpus(UpdateTree):

#========================================================================
    CLS_DEFAULT="Cpu"
    #----------------------------------------------------------------
    def onIterData(self,node):

        result=dict()
        f=open ("/proc/cpuinfo", "r")
        string= f.read()
        f.close()

        string=string.replace("\t","")
        lst=string.split('\n')

        i=0
        for line in lst:


            split= line.strip().split (":")

            if line =="":
                pass
            if len(split)<2 and len(result)>0:

                yield self.name+"/Proc_"+str(i),result
                result={}

            elif len(split)>1:

                name=clean_name(split[0])
                value=split[1].strip()
                if name ==0:
                    #print value
                    pass
                else:
                    result[name]=value
            i+=1
    #----------------------------------------------------------------


#========================================================================

class Computer_Infos(UpdateChildren):

#========================================================================

  
    #----------------------------------------------------------------

    def onIterData(self,node):
        r=dict()
        r[ "hostname" ]= execute("hostname").out
        r[ "version" ]=  open("/etc/issue", "r").readlines()[0][:-8]
        r[ "nodename" ]= execute("uname -n").out
        r[ "kernel_name" ]= execute("uname -s").out
        r[ "kernel_realease" ]= execute("uname -r").out
        r[ "kernel_version" ]= execute("uname -v").out
        r[ "machine" ]= execute("uname -m").out
        r[ "processor" ]= execute("uname -p").out
        r[ "hardware" ]= execute("uname -i").out
        r[ "os" ]= execute("uname -o").out  
        yield self.name,r

    #----------------------------------------------------------------

#========================================================================

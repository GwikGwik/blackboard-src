# -*- coding: utf-8 -*-
#http://latarteauchips.free.fr/linux-programmer-arret-systeme.php
import os
from mod_program import *
from dataModel import *
#========================================================================

class Disc(Object):

#========================================================================
    """
    
    """
    SCAN=Function()
    MOUNT=Function()
    UNMOUNT=Function()

    #----------------------------------------------------------------
    def mount(self):
        """
        https://doc.ubuntu-fr.org/fsck
        https://www.system-linux.eu/index.php?post/2009/01/15/Scan-/-Checkup-dun-HDD-sous-Linux
        """
        yield "sudo unmount %(filePath)s"
        yield "sudo fsck %(filePath)s"

        #----------------------------------------------------------------

    def unmount(self):
        yield "sudo unmount %(filePath)s"

    #----------------------------------------------------------------
    def scan(self,**args):
        """
        principales caractéristiques :

        - si le disque est monté, renvoie l'arbre des fichiers.
        - sinon renvoie None
        - pas d'execption si accès refusé

        les arguments sont ceux de la fonction ls du module pylinux.files.path :

        - recursive
        - ...

        """

        from computer.maps.files.builders import FileTree,DirectoryTree
        if "filePath" in self.keys():

            return DirectoryTree(filePath=self.filePath,recursive=True,**args)()


    #----------------------------------------------------------------

#========================================================================

class Discs(UpdateTree):

#========================================================================

    """
    parse le contenu de la command df et construit un arbre
    avec les chemins de montage.

    df est une commande UNIX utilisée pour afficher la valeur d'espace disque disponible 
    des systèmes de fichier dont l'utilisateur possède l'accès en lecture.

    les infos stoquées sont :

    - fs-name : 
        Nom du fichier système, dans un format défini par 
        l'implémentation

    - total-space :
        Taille totale du fichier système en unités de 512 octets.
        Ce chiffre doit inclure : "<espace utilisé>, <espace libre>", plus des 
        espaces réservés par le système non disponibles pour l'utilisateur.

    - space-used : 
        Valeur totale d'espace alloué à des fichiers existants
        dans le système de fichier en unités de 512 octets.

    - space-free : 
        Valeur totale d'espace disponible dans le système de fichiers
        réservés pour la création de nouveaux fichiers par des utilisateurs non privilégiés, 
        en unités de 512 octets. 

    - percentage-used : 
        Pourcentage d'espace normalement disponible qui est actuellement 
        alloué à tous fichiers du système de fichiers.

    - filePath : 
        Répertoire où apparaît l'arborescence du système de fichiers.


    pour plus d'informations voir : https://fr.wikipedia.org/wiki/Df_%28Unix%29
    """

    CLS_DEFAULT="Disc"
    #----------------------------------------------------------------
    def onIterData(self,node):

        keys=["fs-name","total-space","space-used",
            "space-free","percentage-used","filePath"]

        result=execute("df")#.split("\n")

        i=0
        for line in result.out[1:]:
            if line != "":
                data=line.strip().split()
                infos=dict(zip(keys,data))
                yield self.name+"/disc_"+str(i),infos
                i+=1
    #----------------------------------------------------------------

"""
#========================================================================

class LinuxFileSystem(TreeBuilder):

#========================================================================
    #TODO:Marche plus
    #----------------------------------------------------------------
    def onSearchElements(self,**args):


        for line in execute("dirhelp -a")[1:-1]:
            
            i=line.find("]")
            yield line[1:i],dict(comment=line[i+1:])
    #----------------------------------------------------------------

#========================================================================
 """




from atom import Atom,Atom_Link
from mod_program import *
from dataModel import *
#========================================================================

class UsbDevice(Object):

#========================================================================

    ID=String()
    BUS=String()
    DEVICE=String()
    DESCRIPTION=String()

#========================================================================

class UsbDevices(UpdateTree):

#========================================================================
    """
    info from lsusb in a formated dict
    """
    CLS_DEFAULT="UsbDevice"
    #----------------------------------------------------------------

    def onIterData(self,node):

        result=execute("lsusb")
        for line in result.out:
            bus=line[4:7]
            device=line[15:18]
            id_=line[23:32]
            description=line[33:].strip()

            result={}
            result["id"]=id_
            result["bus"]=bus
            result["device"]=device
            result["description"]=description
            yield self.name+"/"+id_,result

    #----------------------------------------------------------------
#========================================================================

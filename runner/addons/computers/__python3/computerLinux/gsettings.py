


from atom import Atom,Atom_Link,Atom_Manager_List
from mod_program import command,execute,Object
from subprocess import call


#========================================================================

class GSettings_Parameter(Object):

#========================================================================
    """
    correspond a un utilisateur du systeme linux
    les commandes linux associes sont dans les fonctions
    """

    pass

#========================================================================

class GSettings_Scheme(Object):

#========================================================================

    pass

#========================================================================

class GSettings_Schemes(Atom_Manager_List):

#========================================================================
    "generateur pour les utilisateurs du systeme hote"

    CLS_DEFAULT="GSettings_Scheme"
    SEPARATOR_DEFAULT='/'

    def onUpdate(self):


        for path in execute("gsettings list-schemas").out:
            #print(path)
            yield path,dict(fullpath=path)


#========================================================================

class GSettings_Parameters(Atom_Manager_List):

#========================================================================
    "generateur pour les utilisateurs du systeme hote"

    CLS_DEFAULT="GSettings_Parameter"
    SEPARATOR_DEFAULT='/'

    def onUpdate(self):
        cmd=execute("gsettings list-recursively")

        for string in cmd.out:
            print(string)
            if string.strip() !="":
                data=string.split(" ")
                print(data)
                scheme=data[0]
                attr=data[1]
                value=" ".join(data[2:])
                path=scheme+"/"+attr
                print(path)
                yield path,dict(fullpath=path,value=value)

#========================================================================

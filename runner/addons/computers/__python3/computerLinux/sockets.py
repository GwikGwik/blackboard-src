



from atom import Atom
from actions import command,execute

from dataModelAddons.tree import TreeBuilder
from dataModel import Field,String

#========================================================================

class Net_Socket(Node):

#========================================================================

    pass
#========================================================================

def splitheaders(string,separator=" "):

#========================================================================

    for elt in string.split(separator):
        elt=elt.strip()
        if elt != "":
            yield elt,string.find(elt)

#========================================================================

def splitline(string,headers):

#========================================================================

    l=len(headers)

    for pos in range(l):

        name,i=headers[pos]

        if pos+1 < l:
            Xname,j=headers[pos+1]
            j=j-1
            v=string[i:j].strip()
        else:
            v=string[i:].strip()

        yield name,v

#========================================================================

class SocketServersBuilder(TreeBuilder):

#========================================================================
    "generateur pour les utilisateurs du systeme hote"

    FIRST_HEADER="Proto Recv-Q Send-Q Adresse_locale          Adresse_distante        Etat"

    def __call__(self):

        content=execute("netstat")

        node=Node()

        ok=True
        i=0
        group=Node(parent=node,description=content[0])
        headers=list(splitheaders(self.FIRST_HEADER))
        content=content[2:]
        #print headers
        while ok:

            line=content[i]

            if line.strip() == "":
                pass
            elif not line.startswith("Sockets"):

                elts= dict(splitline(line,headers))
                Net_Socket(parent=group,**elts)

            else:
                group=Node(parent=node,description=content[i])
                i+=1
                headers=list(splitheaders(content[i]))


            i+=1
            if i==len(content):ok=False

        return node



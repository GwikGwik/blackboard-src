
from SemanticNode import SemanticNode_Protocole

#=========================================================

class Protocoles_Apt(SemanticNode_Protocole):

#=========================================================
    PROTOCOLE="apt"

    pass

#=========================================================

class Protocoles_Ppa(SemanticNode_Protocole):

#=========================================================
    PROTOCOLE="ppa"

    pass

#=========================================================

class Protocoles_Pip(SemanticNode_Protocole):

#=========================================================
    PROTOCOLE="pip"

    pass

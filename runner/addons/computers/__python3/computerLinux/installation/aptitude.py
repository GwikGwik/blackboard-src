# -*- coding: utf-8 -*-
"""
http://algebraicthunk.net/~dburrows/projects/aptitude/doc/en/rn01re01.html
http://wiki.debian.org/fr/ListInstalledPackages
"""


#from computer.maps.files.builders import FileTree

from atom import Atom,Atom_Link,Atom_Manager_List
from dataModel import *
from workflow_commands import command,execute

#====================================================================

class Linux_Aptitude_Ppa(Atom):

#====================================================================
    pass
#====================================================================

class Linux_Aptitude_Package(Atom):

#====================================================================
    IS_INSTALLED=Function()
    INFOS=Function()
    INSTALL=Function()
    REMOVE=Function()
    DOWNLOAD=Function()

    def is_installed(self):
        #TODO:
        return None

    #---------------------------------------------------------------
    def infos(self,**args):
        r=execute("apt-cache show %s"%self.name)
        #print r
        #print dir(r)
        result=dict()

        for elt in r:
            
            elt=unicode(elt.decode('utf8'))
            if ":" in elt:
                i=elt.find(":")
                name,value=elt[:i],elt[i+1:]
                result[name]=value

        return Node(**result)

    #---------------------------------------------------------------
    #@command
    def install(self,silent=False):
        """
        install an apt package.

            * silent = False :
                ask for password and options
                requires terminal

            * silent = True :
                silently install package
        """

        if silent==False:
            return "sudo apt-get install %s"%self.package
        else:
            return "sudo apt-get install -y %s"%self.package


    #---------------------------------------------------------------
    #@command
    def remove(self,dependancies=False,config=False):
        """
        remove an apt package.

        severals option for config and dependancies :

            * config = False + dependancies = False : 
                    only uninstall package
                    keep config and dependancies
     
            * dependancies = True : 
                    remove  package and  dependancies

            * config = True : 
                    uninstall + remove configuration file for package

            * config = True + dependancies = True : 
                    uninstall + remove configuration file for package and dependancies
 
        """

        if dependancies==True and config==True:
            return "sudo apt-get autoremove --purge %s"%self.package

        elif dependancies==True:
             return  "sudo apt-get autoremove %s"%self.package

        elif config==True:
             return "sudo apt-get purge %s"%self.package

        else:
             return "sudo apt-get remove %s"%self.package



    #---------------------------------------------------------------
    #@command
    def download(self):
        """
        download .deb into current directory
        """
        return "sudo apt-get download %s"%node.package

    #---------------------------------------------------------------


#====================================================================

class Linux_Aptitude_PackageGroup(Atom):

#====================================================================
    """
    http://doc.ubuntu-fr.org/apt-get
    http://www.tecmint.com/useful-basic-commands-of-apt-get-and-apt-cache-for-package-management/
    """
    INFOS=Function()
    INSTALL=Function()
    REMOVE=Function()
    DOWNLOAD=Function()
    #---------------------------------------------------------------

    def infos(self):
        """
        """
        for elt in self.children.children.by_class(Linux_Aptitude_Package):
            yield elt.infos()

    #---------------------------------------------------------------

    def install(self):
        """
        """
        for elt in self.children.by_class(Linux_Aptitude_Package):
            yield elt.install()

    #---------------------------------------------------------------

    def remove(self,**args):
        """
        """
        for elt in self.children.by_class(Linux_Aptitude_Package):
            yield elt.remove(**args)


    #---------------------------------------------------------------

    def download(self):
        """
        """
        for elt in self.children.by_class(Linux_Aptitude_Package):
            yield elt.download()


    #---------------------------------------------------------------



#====================================================================

class Linux_Aptitude_InstalledPackages(Atom_Manager_List):#ex TreeBuilder

#====================================================================
    """

    """


    CLS_DEFAULT="Linux_Aptitude_Package"
    SEPARATOR_DEFAULT='-'


    def onUpdate(self):

        for line in execute("dpkg --get-selections"):
            name=line[:-8].strip()
            yield name,dict(package=name)

#====================================================================

class Linux_Aptitude_PackagesInfos(Atom):#ex (NodeBuilder):

#====================================================================


    def onCall(self,**args):
        d=AptitudeInstalledPackages(parent=self)
        d.setup()
        result=d()
        return result.infos()


#====================================================================


class Linux_Aptitude_AviablePackages(Atom_Manager_List):#ex (TreeBuilder):

#====================================================================

    """

    """

    CLS_DEFAULT="Linux_Aptitude_Package"
    SEPARATOR_DEFAULT='XXX'


    def onUpdate(self):
        for line in execute("apt-cache pkgnames"):

            #print line[:-8].strip()
            yield line[:-8].strip(),dict(installed=True)

#====================================================================

class Linux_Aptitude_Cache(Atom):#ex (NodeBuilder):

#====================================================================


    def onCall(self,**args):
        d=FileTree(filePath='/var/cache/apt/archives',parent=self,recursive=True,reverse=True)
        d.setup()
        result=d()
        return result

#=====================================================================

class Linux_Aptitude_Main(Atom):#ex (Node):

#=====================================================================


    UPDATE=Function()
    DIST_UPGRADE=Function()
    SAFE_UPGRADE=Function()
    FULL_UPGRADE=Function()
    CLEAN=Function()
    AUTO_CLEAN=Function()
    CLEAN_ALT=Function()

    #---------------------------------------------------------------
    # OS
    #---------------------------------------------------------------
    #@command
    def update(self):
        """
        met à jour la liste des fichiers disponibles dans les dépôts APT présents dans le fichier de configuration /etc/apt/sources.list
        """
        return "sudo apt-get update"

    #---------------------------------------------------------------
    #@command
    def dist_upgrade(self):
        """
        met à jour tous les paquets installés vers les dernières versions en installant de nouveaux paquets si nécessaire,
        """
        return "sudo apt-get dist-upgrade"
    #---------------------------------------------------------------
    #@command
    def safe_upgrade(self):
        return "aptitude safe-upgrade"

    #---------------------------------------------------------------
    #@command
    def full_upgrade(self):
        return "aptitude full-upgrade"


    #---------------------------------------------------------------
    #@command
    def repare(self):
        """

        """
        return "sudo apt-get install -f"

    #---------------------------------------------------------------
    #@command
    def clean(self):
        return "aptitude clean"

    #---------------------------------------------------------------
    #@command
    def auto_clean(self):
        return "aptitude autoclean"

    #---------------------------------------------------------------
    #@command
    def clean_alt(self,installed=False):
        """

        """
        if installed==True:
            return "sudo apt-get autoclean"
        else:
            return "sudo apt-get clean"

    #---------------------------------------------------------------
    # PACKAGES
    #---------------------------------------------------------------
    #@command
    def search(self,name):
        return "aptitude search %s"%name



#====================================================================




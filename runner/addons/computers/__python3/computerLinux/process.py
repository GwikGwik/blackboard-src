# -*- coding: utf-8 -*-
from dataModel import *
from atom import Atom,Atom_Link,Atom_Manager_List
from mod_program import *



import psutil,os

#import glob
#lister les processus linux
#http://stackoverflow.com/questions/1705077/python-library-for-linux-process-management
#http://code.google.com/p/psutil/wiki/Documentation
#def autre():
#    glob.glob('/proc/[0-9]*/')
#
#    import psutil
#    pid = 1034  # some pid
#    p = psutil.Process(pid)
#    p.is_running()

#http://linuxlookup.com/howto/view_running_processes_linux_system
#========================================================================

class Linux_Process(Object):

#========================================================================
    USER=String()
    PID=String()
    CPU=String()
    MEM=String()
    VSZ=String()
    RSS=String()
    TTY=String()
    STAT=String()
    START=String()
    TIME=String()
    COMMAND=String()

    IS_ALIVE=Function()
    KILL=Function()

    def is_alive(self):
        return os.path.exists(os.path.join("/run",self.pid))

    @command
    def kill(self):
        return "kill %(pid)s"%self

#========================================================================

class Linux_Processes(UpdateTree):

#========================================================================
    CLS_DEFAULT="Linux_Process"
    SEPARATOR_DEFAULT='/'
#USER       PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND


    def onIterData(self,node):

        infos_file=execute("ps -aux")

        for line in infos_file.out[1:]:
            line=str(line)
            infos=line.replace("\n","")
            #infos=line.split(" ")
            if len(infos)>0:
                result=dict()

                result["user"]=infos[0:11].strip()
                result["pid"]=infos[12:15].strip()
                result["cpu"]= infos[16:20].strip()
                result["mem"]= infos[21:24].strip()
                result["vsz"]= infos[25:31].strip()
                result["rss"]= infos[32:37].strip()
                result["tty"]= infos[38:46].strip()
                result["stat"]= infos[47:52].strip()
                result["start"]= infos[53:57].strip()
                result["time"]= infos[58:64].strip()
                result["command"]=infos[65:].strip()
                #print(result)
                #exit()

                path=result["command"].split(" ")[0]


                if path.startswith("["):
                    path="daemons/"+path[1:-1]

                elif path.startswith("/"):
                    pass

                else:
                    path="running/"+result["pid"]

                result["command"]=" ".join(result["command"])
                print(path,result)#,result["command"])
                yield  path,result

#==========================================================================

class Linux_ProcessDetails(UpdateTree):

#==========================================================================
    SEPARATOR_DEFAULT='/'

    #---------------------------------------------------------------
    def onIterData(self,node):

        for process in psutil.process_iter():
            d=process.as_dict()
            d["base_name"]=d["name"]
            #d["name"]=process.name().replace("/","_")
            yield d["name"],d


#==========================================================================


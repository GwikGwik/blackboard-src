# -*- coding: utf-8 -*-

from dataModel import *
from mod_program import *




#======================================================

class Linux_Service(Object):

#======================================================

    FULLPATH=String()
    STATUS=String()
    SCRIPT=String()

    INSTALL=Function()
    UNINSTALL=Function()
    ACTIVATE=Function()
    UNACTIVATE=Function()
    START=Function()
    STOP=Function()
    RESTART=Function()

    def onSetup(self,name=None,status=None,**args):

        self.status=status
        self.script="/etc/init.d/%s"%name

    @command
    def install(self):
        return ""%self

    @command
    def uninstall(self):
        return ""%self


    @command
    def activate(self):
        return "update-rc.d %(name)s defaults"%self

    @command
    def unactivate(self):
        return "update-rc.d -f %(name)s remove"%self

    @command
    def start(self):
        return "service %(name)s start"%self

    @command
    def stop(self):
        return "service %(name)s stop"%self

    @command
    def restart(self):
        return "service %(name)s restart"%self

    #commands don't work without sudo
#======================================================

class Linux_Services(UpdateChildren):

#======================================================

    CLS_DEFAULT="Linux_Service"



    def onIterData(self,node):
        infos_file=execute("sudo service --status-all")

        for line in infos_file.out:

            line=str(line)
            line=line.replace("\n","")

            i=line.find("[")
            j=line.find("]")
            name=line[j+2:-3].strip()
            yield name,dict(fullpath=name,status=line[i+1:j-1])


#======================================================

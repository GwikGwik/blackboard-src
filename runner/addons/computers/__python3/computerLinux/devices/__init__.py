#from internet from InternetConnexion

#from arduino from ARDUINOConnexion

from .arduino import *
from .cdrom import *
from .internet import *
from .keyboard import *
from .mididevice import *
from .mouse import *
from .node import *
from .scanner import *
from .screen import *
from .sound import *


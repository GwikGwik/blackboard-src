try:
    import sane   
except:
    pass

from .node import Linux_Device

class Devices_Scanner(Linux_Device):


    def onSetup(self,description=None,**args):
        self.name=description[0]
        self.description=description

    def login(self):
        self.scan_obj=sane.open(self.name)

    def logout(self):
        self.scan_obj.close()
        
    #ACTIONS
    def get_parameters(self):
        return self.scan_obj.get_parameters()
    
    def get_options_keys(self):
        return self.scan_obj.optlist
    
    def get_options(self):
        return self.scan_obj.get_options()

    def start_scan(self):
        return self.scan_obj.start()
  
    def scan(self):
        return self.scan_obj.scan()
          
    def snap(self):
        return self.scan_obj.snap()

    def arr_snap(self, multipleOf=1):
        return self.scan_obj.arr_snap(multipleOf=multipleOf)
        
    def arr_scan(self, multipleOf=1):
        return self.scan_obj.arr_scan(multipleOf=multipleOf)
    
    def cancel(self):
        return self.scan_obj.cancel()       
     
    def fileno(self):
        return self.scan_obj.fileno()
    

        
class Devices_ScannerManager(Linux_Device):


    def onsetup(self):

        sane.init()
        scanners= sane.get_devices()
        for scanner in scanners:
            Scanner(parent=self,name=scanner[0],description=scanner)

    def logout(self):
        sane.exit()
        
        

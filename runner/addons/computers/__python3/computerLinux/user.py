

from atom import Atom,Atom_Link,Atom_Manager_List
from mod_program import *



from dataModel import Field,String,Function


class Linux_Link_UserGroup(Atom_Link):pass
class Linux_Link_UserProcess(Atom_Link):pass
class Linux_Link_UserService(Atom_Link):pass

#========================================================================

class Linux_User(Object):

#========================================================================
    """
    correspond a un utilisateur du systeme linux
    les commandes linux associes sont dans les fonctions
    """

    ID=String()
    GID=String()
    USERNAME=String()
    PASSWORD=String()
    PAS=String()
    OTHERARGS=Field()
    HOME=String()
    SHELL=String()

    CREATE=Function()
    REMOVE=Function()
    ADD_TO_GROUP=Function()
    FILES=Function()

    #--------------------------------------------------------------------
    @command
    def create(self):
        return "useradd --create-home --password $(password)s $(username)s"

    #--------------------------------------------------------------------
    @command
    def remove(self):
        return "userdel -r $(username)s"

    #--------------------------------------------------------------------
    @command
    def add_to_group(self,group):
        return ""

    #--------------------------------------------------------------------
    def files(self):
        from computer.maps.files.builders import FileTree,DirectoryTree
        if "home" in self.keys():

            return FileTree(filePath=self.home,recursive=True)()


#========================================================================

class Linux_Users(UpdateTree):

#========================================================================
    "generateur pour les utilisateurs du systeme hote"

    CLS_DEFAULT="Linux_User"


    GROUPS=Function()
    PS=Function()
    SERVICES=Function()


    def onIterData(self,node):

        for string in open("/etc/passwd","r"):
            #string=string.decode("utf8")
            infos=dict()
            name, user_id, gid, pas, otherargs, home, shell = string.strip().split (":")

            infos["id"]=user_id
            infos["gid"]= gid
            infos["username"]=name
            infos["pas"]= pas
            infos["otherargs"]= otherargs
            infos["home"]= home
            infos["shell"]= shell

            #print(name,infos)
            yield self.name+"/"+name,infos


    #--------------------------------------------------------------------

    def groups(self):

        groups=Linux_Groups(parent=self.root)

        for user in self.children.by_class(Linux_User):
            try:
                Linux_UserGroup(source=user,target=groups.find(user.name))
            except:
                pass

        for group in groups.children.by_class(Linux_Group):

            for username in group.users:
                if username !="":
                    Linux_UserGroup(source=self.find(username),target=group)

                """
                try:
                    Link(source=self.find(username),target=group)
                except:
                    pass
"""
        return groups

    #--------------------------------------------------------------------

    def ps(self):

        ps=Linux_Processes()

        for p in ps.all().by_class(Linux_Process):
            if p.user is not None:
                if p.user.endswith("+"):

                    for elt in self.children:
                        if elt.name.startswith(p.user[:-1]):
                            user=elt
                            break
                    Linux_UserProcess(source=user,target=p)
                    
                else:
                    try:
                        Linux_UserProcess(source=self.find(p.user),target=p)
                    except:
                        p.show()

        return ps
    #--------------------------------------------------------------------

    def services(self):

        ps=Linux_Services()

        for p in ps.children.by_class(Linux_Service):

            try:
                Linux_UserService(source=self.find(p.name),target=p)
                #print(p.name)
            except:
                pass

        return ps
    #--------------------------------------------------------------------

#========================================================================






from mod_program import command,execute,Object,Action


import socket,os
import urllib.request
import nmap
from dataModel import *
#==========================================================================

class Network_PublicIp(Object):

#==========================================================================
    IP=String()
    #DONE

    #----------------------------------------------------------------
    
    def on_update(self):
        self.ip=None
        self.done=False
        texte = urllib.request.urlopen("http://www.monip.org").read()
        ip_public = texte.split("IP : ")[1].split("<")[0]
        self.ip= ip_public
           

        self.done=True
            
    #----------------------------------------------------------------


#=====================================================================

class Network_Whois(Object):

#=====================================================================
    DOMAIN=String()
    CREATION_DATE=Field()
    DOMAIN_NAME=String()
    EMAILS=String()
    EXPIRATION_DATE=Field()
    TEXT=String()

    #---------------------------------------------------------------
    def on_update(self):
        import whois
        w   = whois.whois(self.domain)

        self.creation_date=w.creation_date
        self.domain_name=w.domain_name
        self.emails=w.emails
        self.expiration_date=w.expiration_date
        self.text=w.text


        """
        response = execute("whois " + domain)
        root=Element(response=response)
        return root
        """
    #---------------------------------------------------------------

#=====================================================================

class Network_route(Object):

#=====================================================================
    #DOMAIN
    #---------------------------------------------------------------
    def on_update(self):
        root=Element()
        for line in execute("tracepath "+self.domain):
            Element(parent=root,line=line)
        return root

        dest_addr = socket.gethostbyname(self.domain)
        port = 33434
        max_hops = 30
        icmp = socket.getprotobyname('icmp')
        udp = socket.getprotobyname('udp')
        ttl = 1
        while True:
            recv_socket = socket.socket(socket.AF_INET, socket.SOCK_RAW, icmp)
            send_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, udp)
            send_socket.setsockopt(socket.SOL_IP, socket.IP_TTL, ttl)
            recv_socket.bind(("", port))
            send_socket.sendto("", (domain, port))
            curr_addr = None
            curr_name = None
            try:
                _, curr_addr = recv_socket.recvfrom(512)
                curr_addr = curr_addr[0]
                try:
                    curr_name = socket.gethostbyaddr(curr_addr)[0]
                except socket.error:
                    curr_name = curr_addr
            except socket.error:
                pass
            finally:
                send_socket.close()
                recv_socket.close()

            if curr_addr is not None:
                curr_host = "%s (%s)" % (curr_name, curr_addr)
            else:
                curr_host = "*"
            Element(parent=self,ttl=ttl,host= curr_host)

            ttl += 1
            if curr_addr == dest_addr or ttl > max_hops:
                break


    #--------------------------------------------------------

#==========================================================================

class Network_Scan(Object):

#==========================================================================

    parallelism="--max-hostgroup=5 --max-parallelism=10"

    scan_intense="T5 -A -v"
    scan_quick="-T5-F"
    scan_quick_plus="-sV -T5 -O -F --version-light"
    scan_quick_traceroute="-sn --traceroute"
    scan_ping="-sn"

    port_0='1-21'
    port_small='22-443'
    port_medium='444-12000'
    port_large='22-24000'
    port_all='22-65535'

    HOSTS=String(default='192.168.1.0-255')
    MODE=String(default="quick")
    PORTS=String(default="small")
    #---------------------------------------------------------------
    def scan(self):

        arguments=getattr(self,"scan_"+self.mode)
        arguments+=" "+self.parallelism
        port=getattr(self,"port_"+self.ports)

        nm = nmap.PortScanner()
        nm.scan(self.hosts,port, arguments=arguments)
        return self.parse(nm)

    #---------------------------------------------------------------
    def parse(self,nm,**args):

        for host in nm.all_hosts():

            if nm[host]['status']['state']=="up":

                data=dict()

                try:
                    data['adress']=nm[host].hostnames()[0]['name']
                except:
                    for k in nm[host]['addresses']:
                        data['adress']=nm[host]['addresses'][k]
                data.update(nm[host])
                #data.update(nm[host]['vendor'])
                #data.update(nm[host]['addresses'])

                #print(data['adress'],host)
                if data['adress']:
                    name=data['adress']
                else:
                    name=host

                machine=self.append(name,cls=self.getClass("Network_Machine"),ip=host,**data)

                self.parse_machine(machine,nm[host])



    #---------------------------------------------------------------
    def parse_machine(self,machine,host,**args):

        for proto in host.all_protocols():

            lport = host[proto].keys()
            #lport.sort()
            #print(lport)
            for port in lport:

                infos=host[proto][port]
                if type(infos) == dict:
                    machine.append(str(port),cls=self.getClass("Network_Port"),port=port,proto=proto,**infos)
                else:
                    machine.append(str(port),cls=self.getClass("Network_Port"),port=port,proto=proto,infos=infos)

    #---------------------------------------------------------------




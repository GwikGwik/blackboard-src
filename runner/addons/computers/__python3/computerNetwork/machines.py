from dataModel import *


from mod_program import command,execute,Object


import socket,os
import urllib
import nmap
from .networktools import *

#==========================================================================

class Network_Port(Object):

#==========================================================================

    #---------------------------------------------------------------
    def connect(self):

        return 

    #---------------------------------------------------------------        


#===============================================================

class Network_Machine(Object):
    
#===============================================================

    # local data
    #-----------------------------------------------------------

    FILE_PATH=String()
    VAR_PATH=String()

    #ssh
    #-----------------------------------------------------------
    SSH_PATH=String()
    SSH_PREFIX=String()

    #network
    IP=String()
    HOST=String()

    STATE=Function(TreeLogs())


    #--------------------------------------------------------------
    def state(self):
        try:
            socket.gethostbyaddr(self.ip)
            return True
        except :
           return False

    #--------------------------------------------------------------
    def command(self,text=None):
        print(self.ssh_prefix,self.ssh_path,text)

    #--------------------------------------------------------------
    def sudo(self,command):
        self.command("sudo "+command)

    #---------------------------------------------------------------
    def ping(self):
        response = execute("ping -c 1 " + self.ip)

        if response == 0:
            self.result= True
        else:
            self.result= False

    #---------------------------------------------------------------
    def scan(self):
        print(50*"#",self.name,self.ip)
        node=Network_Scan(parent=self,hosts=self.ip,mode="quick",ports="medium")
        node.setup()
        node.scan()
        for child in node.all().by_class(Network_Port):
            child.parent=self
            #print(child.port,child.name)
        node.destroy()

    #---------------------------------------------------------------


#==========================================================================

class Network_Area(Object):

#==========================================================================
    """
    un reseau est groupe de machines
    """
    HOSTS=String()
    SCAN=Function(TreeLogs())

    #HOSTS
    #---------------------------------------------------------------
    def scan(self,**args):

        return Network_Scan(parent=self,hosts=self.hosts,**args).call()

    #---------------------------------------------------------------




#==========================================================================


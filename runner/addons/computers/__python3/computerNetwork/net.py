# -*- coding: utf-8 -*-

from netifaces import interfaces, ifaddresses, AF_INET
#TODO:routes requieres import nmap


from atom import Atom,Atom_Link,TreeLogs
from mod_program import command,execute,Object


from dataModel import *
from .networktools import *
from .machines import *
#from localhost import ActiveConnections
#from networktools import PublicIp
#from network.machines import NetworkScan

#https://linoxide.com/linux-command/use-ip-command-linux/



#========================================================================

class Network_Interface(Object):

#========================================================================
    """
    interface réseau
    """

    TRAFFIC=Function()
    CONFIG=Function()
    INFOS=Function()
    START=Function()
    STOP=Function()
    STATUS=Function()
    SCAN=Function()

    @command
    def traffic(self):
        return "ifstat -t -i %s 0.5"%self.name

    @command
    def config(self):
        return "ifconfig"

    @command
    def infos(self):
        return "ip link show %s"%self.name

    @command
    def start(self):
        return "sudo ifconfig %s up"%self.name

    @command
    def stop(self):
        return "sudo ifconfig %s down"%self.name

    @command
    def status(self):
        return "iw %s link"%self.name

    def scan(self):
        #print( self.name )
        if self.isConnected():
            ip=self.adresses[0]
            hosts=ip
            if ip !="127.0.0.1":

                hosts=ip.split(".")
                hosts[3]="0-255"
                hosts=".".join(hosts)


            node=Network_Scan(hosts=hosts,mode="quick",ports="small")
            node.setup()
            node.scan()
            for child in node.all().by_class(Network_Machine):
                child.parent=self
            node.destroy()


    def scan_local(self,**args):

        if self.isConnected():
            ip=self.adresses[0]
            node= NetworkScan(hosts=ip,**args)()
            return node


    def isConnected(self):
        return self.adresses[0] is not None

#========================================================================

class Network_Interfaces(Object):

#========================================================================


    ROUTES=Function()
    #SCAN=Function(TreeLogs(),port=String())
    
    #CONNEXIONS=ActiveConnections()
    #PUBLIC_IP=PublicIp()

    #---------------------------------------------------------------
    @command
    def routes(self):
        return "nmap --iflist"

    #---------------------------------------------------------------
    #def scan(self,**args):
    #    return self.execOnElements("scan",Class=Computer_NetworkInterface,**args)


    #---------------------------------------------------------------
    def scan(self):
       
        self.children.by_class(Network_Interface).execute("destroy")

        for ifaceName in interfaces():
            addresses = [i['addr'] for i in ifaddresses(ifaceName).setdefault(AF_INET, [{'addr':None}] )]
            #print(addresses)
            Network_Interface(parent=self,name=ifaceName,adresses=addresses)


#========================================================================

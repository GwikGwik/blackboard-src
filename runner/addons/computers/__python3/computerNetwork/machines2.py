from dataModel import *
from workflow import Action
from atom import Atom,TreeLogs
from workflow_commands import command,execute


import socket,os
import urllib
import nmap
from .networktools import *

from IoNode import IoNode_Atom

import time
#==========================================================================

class Network2_Node(IoNode_Atom):

#==========================================================================

    SELECT=String(default="/")
    DELAY=Float(default=60.0)

    #----------------------------------------------------------------
    def onStart(self):
        pass
        
    #----------------------------------------------------------------
    def onDo(self):
        self.node=self.find(self.select)
        self.onScan()
        time.sleep(self.delay)

    #----------------------------------------------------------------
    def onStop(self):
        pass

    #----------------------------------------------------------------
    def onScan(self):
        pass

    #----------------------------------------------------------------

#==========================================================================

class Network2_Scanner(Network2_Node):

#==========================================================================
    parallelism="--max-hostgroup=5 --max-parallelism=10"

    scan_intense="T5 -A -v"
    scan_quick="-T5-F"
    scan_quick_plus="-sV -T5 -O -F --version-light"
    scan_quick_traceroute="-sn --traceroute"
    scan_ping="-sn"

    port_0='1-21'
    port_small='22-443'
    port_medium='22-12000'
    port_large='22-24000'
    port_all='1-65535'

    HOSTS=String(default='192.168.1.0-255')
    SCAN_OPTIONS=String(default="quick")
    PORTS_OPTIONS=String(default="small")

    #---------------------------------------------------------------
    def onScan(self):
        
        nm=self.scan(hosts=self.hosts,scan_options=self.scan_options,ports_options=self.ports_options)
        return self.parse(nm)

    #---------------------------------------------------------------
    def scan(self,hosts='192.168.1.0-255',ports_options=None,scan_options=None,**args):

        if scan_options is not None:
            arguments=getattr(self,"scan_"+scan_options)
        else:
            arguments=""

        arguments+=" "+self.parallelism

        if hasattr(self,"port_"+ports_options):
            port=getattr(self,"port_"+ports_options)

        nm = nmap.PortScanner()
        nm.scan(hosts,port, arguments=arguments)
        return nm

    #---------------------------------------------------------------
    def parse(self,nm,**args):

        for host in nm.all_hosts():

            if nm[host]['status']['state']=="up":

                data=dict()

                try:
                    data['adress']=nm[host].hostnames()[0]['name']
                except:
                    for k in nm[host]['addresses']:
                        data['adress']=nm[host]['addresses'][k]
                data.update(nm[host])
                #data.update(nm[host]['vendor'])
                #data.update(nm[host]['addresses'])

                print(data['adress'],host)
                if data['adress']:
                    name=data['adress']
                else:
                    name=host

                machine=self.get_machine(host,name=name,**data)

                self.parse_machine(machine,nm[host])



    #---------------------------------------------------------------
    def parse_machine(self,machine,host,**args):

        for proto in host.all_protocols():

            lport = host[proto].keys()
            #lport.sort()
            print(lport)
            for port in lport:

                infos=host[proto][port]
                if type(infos) == dict:
                    machine.get_port(port,proto=proto,**infos)
                else:
                    machine.get_port(port,proto=proto,infos=infos)

    #---------------------------------------------------------------
    def get_machine(self,ip,name=None,**args):
        for elt in self.node.children.by_class(Machine2):
            if elt.ip == ip:
                elt.update(args)
                return elt
        return Machine2(parent=self.node,ip=ip,**args)
        
    #---------------------------------------------------------------


#==========================================================================

class Machine2_Port(Atom):

#==========================================================================
    PORT=Integer()
    #---------------------------------------------------------------
    def connect(self):

        return 

    #---------------------------------------------------------------        


#===============================================================

class Machine2(Atom):
    
#===============================================================

    # local data
    #-----------------------------------------------------------

    FILE_PATH=String()
    VAR_PATH=String()

    #ssh
    #-----------------------------------------------------------
    SSH_PATH=String()
    SSH_PREFIX=String()

    #network
    IP=String()
    HOST=String()

    STATE=Function(TreeLogs())

    #---------------------------------------------------------------
    def onSetup(self):
        pass
        #Network_Scan(name="scan",parent=self,hosts=self.ip)
        #Machine_Whois(name="whois",parent=self,domain=self.host)
        #Machine_Ping(name="ping",parent=self,ip=self.ip)
        #Machine_Traceroute(name="traceroute",parent=self,domain=self.ip)

    #--------------------------------------------------------------
    def state(self):
        try:
            socket.gethostbyaddr(self.ip)
            return True
        except :
           return False

    #--------------------------------------------------------------
    def command(self,text=None):
        print(self.ssh_prefix,self.ssh_path,text)

    #--------------------------------------------------------------
    def sudo(self,command):
        self.command("sudo "+command)

    #---------------------------------------------------------------
    def get_port(self,port,name=None,**args):

        for elt in self.children.by_class(Machine2_Port):
            if elt.port == port:
                elt.update(args)
                return elt
        return self.append(name,cls=Machine2_Port,port=port,**args)
        
    #---------------------------------------------------------------


#==========================================================================

class Network2(IoNode_Atom):

#==========================================================================
    """
    un reseau est groupe de machines
    """
    HOSTS=String()
    SCAN=Function(TreeLogs())

    #HOSTS
    #---------------------------------------------------------------
    def scan(self,**args):

        return Network_Scan(parent=self,hosts=self.hosts,**args).call()

    #---------------------------------------------------------------



#==========================================================================



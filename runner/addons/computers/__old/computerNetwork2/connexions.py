
import socket
import string
from random import choice
import urllib


from netifaces import interfaces, ifaddresses, AF_INET

#========================================================================
def network_interfaces():    

#========================================================================

    for ifaceName in interfaces():
        addresses = [i['addr'] for i in ifaddresses(ifaceName).setdefault(AF_INET, [{'addr':'No IP addr'}] )]
        yield ifaceName,addresses


#========================================================================
def free_port(ip=''):    

#========================================================================
    """
    return a free port at ip adress
    """

    sock1 = socket.socket(socket.AF_INET, socket.SOCK_STREAM, 0)
    sock1.bind((ip, 0))
    sock1.listen(socket.SOMAXCONN)
    ipaddr1, port1 = sock1.getsockname()
    return ipaddr1,port1

#========================================================================
def new_password(length=64):

#========================================================================
    chars=string.letters + string.digits
    return ''.join([choice(chars) for i in range(length)])

#========================================================================
def format_for_args(ip,port,authkey=None):

#========================================================================

    return {"ip":ip,"port":port,"authkey":authkey}

#========================================================================
def get_connexion(ip="",authkey=False,**args):

#========================================================================

    if authkey:
        authkey=new_password(**args)
    else:
        authkey=None

    ip,port= free_port(ip=ip)
    return format_for_args(ip,port,authkey)

#========================================================================
def get_connexions(n,ip="",authkey=False):

#========================================================================

    for i in range(n):
        yield get_connexion(ip=ip,authkey=authkey)



#========================================================================


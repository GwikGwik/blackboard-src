


from workflow import Action
from workflow_commands import command,execute


import socket,os
import urllib.request
import nmap
from dataModel import *
#==========================================================================

class Network_PublicIp(Action):

#==========================================================================
    #IP
    #DONE

    #----------------------------------------------------------------
    
    def onCall(self):
        self.ip=None
        self.done=False
        texte = urllib.request.urlopen("http://www.monip.org").read()
        ip_public = texte.split("IP : ")[1].split("<")[0]
        self.ip= ip_public
           

        self.done=True
            
    #----------------------------------------------------------------


#=====================================================================

class Network_Machine_Whois(Action):

#=====================================================================
    #DOMAIN
    #---------------------------------------------------------------
    def onCall(self):
        import whois
        w   = whois.whois(self.domain)

        self.creation_date=w.creation_date
        self.domain_name=w.domain_name
        self.emails=w.emails
        self.expiration_date=w.expiration_date
        self.text=w.text


        """
        response = execute("whois " + domain)
        root=Element(response=response)
        return root
        """
    #---------------------------------------------------------------

#=====================================================================

class Network_Machine_Ping(Action):

#=====================================================================
    #IP

    #---------------------------------------------------------------
    def Call(self):
        response = execute("ping -c 1 " + self.ip)

        if response == 0:
            self.result= True
        else:
            self.result= False

    #---------------------------------------------------------------

#=====================================================================

class Network_Machine_Traceroute(Action):

#=====================================================================
    #DOMAIN
    #---------------------------------------------------------------
    def onCall(self):
        root=Element()
        for line in execute("tracepath "+self.domain):
            Element(parent=root,line=line)
        return root

        dest_addr = socket.gethostbyname(self.domain)
        port = 33434
        max_hops = 30
        icmp = socket.getprotobyname('icmp')
        udp = socket.getprotobyname('udp')
        ttl = 1
        while True:
            recv_socket = socket.socket(socket.AF_INET, socket.SOCK_RAW, icmp)
            send_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, udp)
            send_socket.setsockopt(socket.SOL_IP, socket.IP_TTL, ttl)
            recv_socket.bind(("", port))
            send_socket.sendto("", (domain, port))
            curr_addr = None
            curr_name = None
            try:
                _, curr_addr = recv_socket.recvfrom(512)
                curr_addr = curr_addr[0]
                try:
                    curr_name = socket.gethostbyaddr(curr_addr)[0]
                except socket.error:
                    curr_name = curr_addr
            except socket.error:
                pass
            finally:
                send_socket.close()
                recv_socket.close()

            if curr_addr is not None:
                curr_host = "%s (%s)" % (curr_name, curr_addr)
            else:
                curr_host = "*"
            Element(parent=self,ttl=ttl,host= curr_host)

            ttl += 1
            if curr_addr == dest_addr or ttl > max_hops:
                break


    #--------------------------------------------------------

#==========================================================================

class Network_Scan(Action):

#==========================================================================

    parallelism="--max-hostgroup=5 --max-parallelism=10"

    scan_intense="T5 -A -v"
    scan_quick="-T5-F"
    scan_quick_plus="-sV -T5 -O -F --version-light"
    scan_quick_traceroute="-sn --traceroute"
    scan_ping="-sn"

    port_0='1-21'
    port_small='22-443'
    port_medium='22-12000'
    port_large='22-24000'
    port_all='1-65535'

    HOSTS=String(default='192.168.1.0-255')
    ARGUMENTS=String(default="quick")

    #---------------------------------------------------------------
    def onCall(self):
        nm=self.scan(hosts=self.hosts,arguments=self.arguments)
        return self.parse(nm)

    #---------------------------------------------------------------
    def scan(self,hosts='192.168.1.0-255',port="small",arguments=None,**args):

        if arguments is not None:
            arguments=getattr(self,"scan_"+arguments)
        else:
            arguments=""

        arguments+=" "+self.parallelism

        if hasattr(self,"port_"+port):
            port=getattr(self,"port_"+port)

        nm = nmap.PortScanner()
        nm.scan(hosts,port, arguments=arguments)
        return nm

    #---------------------------------------------------------------
    def parse(self,nm,**args):

        for host in nm.all_hosts():

            if nm[host]['status']['state']=="up":

                data=dict(name=host)

                try:
                    data['adress']=nm[host].hostnames()[0]['name']
                except:
                    for k in nm[host]['addresses']:
                        data['adress']=nm[host]['addresses'][k]
                data.update(nm[host])
                #data.update(nm[host]['vendor'])
                #data.update(nm[host]['addresses'])
                machine=Network_Machine(parent=self,**data)

                #self.parse_machine(machine,nm[host])



    #---------------------------------------------------------------
    def parse_machine(self,machine,host,**args):

        for proto in host.all_protocols():

            lport = host[proto].keys()
            #lport.sort()
            print(lport)
            for port in lport:

                infos=host[proto][port]
                if type(infos) == dict:
                    self.NetworkMachinePort(parent=machine,port=port,proto=proto,**infos)
                else:
                    self.NetworkMachinePort(parent=machine,port=port,proto=proto,infos=infos)

    #---------------------------------------------------------------




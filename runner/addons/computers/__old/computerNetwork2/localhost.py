
from workflow import Action
from workflow_commands import command,execute



from .machines import Network_Area,Network_Machine,Network_Scan


import psutil


#==========================================================================

class Network_Localhost(Network_Machine):

#==========================================================================

    #---------------------------------------------------------------
    def scan(self,**args):

        return Network_Scan(hosts="127.0.0.1",**args).run()
    #---------------------------------------------------------------

#==========================================================================

class Network_Localhost_ActiveConnections(Action):

#==========================================================================

    OBJECT=Network_Area
    SUBOBJECT=Network_Machine
    SEPARATOR="/"

    #---------------------------------------------------------------
    def onSearchElements(self,**args):

        for process in psutil.process_iter():

            try:
                connections = process.connections(kind='all')

                for conn in connections:
                        yield ip,dict(conn)

            except:
                pass


    #---------------------------------------------------------------

#==========================================================================

class Network_Localhost_ActiveConnectionsX(Action):

#==========================================================================

    OBJECT=Net_Area
    SUBOBJECT=Net_Machine
    SEPARATOR="/"

    #---------------------------------------------------------------
    def onSearchElements(self,**args):

        for process in psutil.process_iter():

            try:
                connections = process.connections(kind='inet')
            except psutil.AccessDenied or psutil.NoSuchProcess:
                pass
            else:
                for connection in connections:

                    if connection.remote_address and connection.remote_address[0] not in remote_ips:
                        ip=connection.remote_address[0]
                        yield ip,dict(ip=ip,connection=connection)




    #---------------------------------------------------------------


#==========================================================================


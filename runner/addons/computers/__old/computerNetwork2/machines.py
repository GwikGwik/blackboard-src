from dataModel import Field,String,Function,ReturnFrame,Builder
from workflow import Action
from atom import Atom,TreeLogs
from workflow_commands import command,execute

import socket,os
import urllib
import nmap
from .networktools import *
#===============================================================

class Network_Machine_Source(Atom):
    
#===============================================================

    pass

#===============================================================

class Network_Machine_Package(Atom):
    
#===============================================================

    pass

#==========================================================================

class Network_Machine_Port(Atom):

#==========================================================================
    #---------------------------------------------------------------
    def connect(self):

        return 

    #---------------------------------------------------------------        


#===============================================================

class Network_Machine(Atom):
    
#===============================================================

    # local data
    #-----------------------------------------------------------

    FILE_PATH=String()
    VAR_PATH=String()

    DATA_FROM_SOURCE=Function()
    DATA_TO_DISTANT=Function()

    #ssh
    #-----------------------------------------------------------
    SSH_PATH=String()
    SSH_PREFIX=String()

    #network
    NET_IP=String()
    NET_HOST=String()

    DISTANT_PING=Function(TreeLogs())
    DISTANT_SCAN=Function(TreeLogs())


    DISTANT_WHOIS=Function(TreeLogs())
    DISTANT_TRACEROUTE=Function(TreeLogs())

    DISTANT_STATE=Function(TreeLogs())



    #--------------------------------------------------------------
    def distant_state(self):
        try:
            socket.gethostbyaddr(self.net_ip)
            return True
        except :
           return False

    #---------------------------------------------------------------
    def distant_scan(self,**args):

        return Network_Scan(parent=self,hosts=self.net_ip,**args).run()

        
    #---------------------------------------------------------------
    def distant_whois(self,**args):

        return Network_Machine_Whois(parent=self,domain=self.net_host,**args).call()
        
    #---------------------------------------------------------------
    def distant_ping(self,**args):

        return Network_Machine_Ping(parent=self,ip=self.net_ip,**args).call()

    #---------------------------------------------------------------
    def distant_traceroute(self,**args):

        return Network_Machine_Traceroute(parent=self,domain=self.net_ip,**args).call()

    #--------------------------------------------------------------
    def data_from_source(self):
        #return self.all().by_class(Blackboard_Package)
        pass
    #--------------------------------------------------------------
    def data_to_distant(self):
        #return self.all().by_class(Blackboard_Package)
        pass
    #--------------------------------------------------------------
    def command(self,command):
        print(self.ssh_prefix,self.ssh_path,command)

    #--------------------------------------------------------------
    def sudo(self,command):
        self.command("sudo "+command)






#==========================================================================

class Network_Area(Atom):

#==========================================================================
    """
    un reseau est groupe de machines
    """
    HOSTS=String()
    SCAN=Function(TreeLogs())

    #HOSTS
    #---------------------------------------------------------------
    def scan(self,**args):

        return Network_Scan(parent=self,hosts=self.hosts,**args).call()

    #---------------------------------------------------------------


#==========================================================================


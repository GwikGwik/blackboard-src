#####################################
les caractéristiques de l'ordinateur
#####################################


appeler l'objet
==================================

.. literalinclude:: computer.py
   :language: python

.. autoclass:: pylinux.hardware.computer.Computer

la classe Computer
==================================

.. autoclass:: pylinux.hardware.computer.ComputerInfos

renomer la machine
-----------------------

.. automethod:: pylinux.hardware.computer.Computer.rename

controler la machine
-----------------------

.. automethod:: pylinux.hardware.computer.Computer.shutdown
.. automethod:: pylinux.hardware.computer.Computer.cancelShutdown
.. automethod:: pylinux.hardware.computer.Computer.reboot

hardware
-----------------------

.. automethod:: pylinux.hardware.computer.Computer.cpus
.. automethod:: pylinux.hardware.computer.Computer.memory
.. automethod:: pylinux.hardware.computer.Computer.devices
.. automethod:: pylinux.hardware.computer.Computer.pci
.. automethod:: pylinux.hardware.computer.Computer.usb

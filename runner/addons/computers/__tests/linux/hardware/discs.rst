##################################################
les disques
##################################################


parcourir les disques
==================================

.. literalinclude:: computer.py
   :language: python

.. autoclass:: computer.files.discs.DiscManager


la classe Disc
==================================

.. autoclass:: computer.files.discs.Disc

voir les fichiers
-----------------------

.. automethod:: linux.hardware.discs.Computer_DiscgetFiles

montage / demontage
-----------------------

.. automethod:: linux.hardware.discs.Computer_Discmount
.. automethod:: linux.hardware.discs.Computer_Discunmount

la classe DiscElement
==================================

.. autoclass:: computer.files.discs.DiscElement

voir les infos
-----------------------

.. automethod:: computer.files.discs.DiscElement.infos

modif
-----------------------

.. automethod:: computer.files.discs.DiscElement.create
.. automethod:: computer.files.discs.DiscElement.remove
.. automethod:: computer.files.discs.DiscElement.copy



##################################################
aptitude
##################################################

la classe Aptitude
==================================

.. autoclass:: pylinux.installers.aptitude.Aptitude

rechercher dans les paquets
------------------------------

.. automethod:: pylinux.installers.aptitude.Aptitude.search
.. automethod:: pylinux.installers.aptitude.Aptitude.installed
.. automethod:: pylinux.installers.aptitude.Aptitude.aviable
.. automethod:: pylinux.installers.aptitude.Aptitude.cache

outils pour l'os
------------------------------

.. automethod:: pylinux.installers.aptitude.Aptitude.update
.. automethod:: pylinux.installers.aptitude.Aptitude.dist_upgrade
.. automethod:: pylinux.installers.aptitude.Aptitude.safe_upgrade
.. automethod:: pylinux.installers.aptitude.Aptitude.ull_upgrade
.. automethod:: pylinux.installers.aptitude.Aptitude.repare
.. automethod:: pylinux.installers.aptitude.Aptitude.clean
.. automethod:: pylinux.installers.aptitude.Aptitude.autoclean
.. automethod:: pylinux.installers.aptitude.Aptitude.cleanAlt


la classe AptitudePackage
==================================

.. autoclass:: pylinux.installers.aptitude.AptitudePackage

.. automethod:: pylinux.installers.aptitude.AptitudePackage.is_installed
.. automethod:: pylinux.installers.aptitude.AptitudePackage.infos
.. automethod:: pylinux.installers.aptitude.AptitudePackage.install
.. automethod:: pylinux.installers.aptitude.AptitudePackage.remove
.. automethod:: pylinux.installers.aptitude.AptitudePackage.download


la classe AptitudePackageGroup
==================================

.. autoclass:: pylinux.installers.aptitude.AptitudePackageGroup

.. automethod:: pylinux.installers.aptitude.AptitudePackage.infos
.. automethod:: pylinux.installers.aptitude.AptitudePackage.install
.. automethod:: pylinux.installers.aptitude.AptitudePackage.remove
.. automethod:: pylinux.installers.aptitude.AptitudePackage.download


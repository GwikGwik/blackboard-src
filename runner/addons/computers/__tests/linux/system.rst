##################################################
explorer le systeme
##################################################




.. autoclass:: pylinux.system.LinuxSystem

la machine
====================================

.. automethod:: pylinux.system.LinuxSystem.computer
.. automethod:: pylinux.system.LinuxSystem.server
.. automethod:: pylinux.system.LinuxSystem.discs
.. automethod:: pylinux.system.LinuxSystem.files


les composants
====================================

.. automethod:: pylinux.system.LinuxSystem.users
.. automethod:: pylinux.system.LinuxSystem.groups
.. automethod:: pylinux.system.LinuxSystem.processes
.. automethod:: pylinux.system.LinuxSystem.services
.. automethod:: pylinux.system.LinuxSystem.aptitude



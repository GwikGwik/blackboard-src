##################################################
gerer les services
##################################################

lister les services
=======================

.. literalinclude:: process.py
   :language: python

.. autoclass:: pylinux.arch.services.ServiceManager


la classe Service
=======================

.. autoclass:: pylinux.arch.services.Service

cycle de vie
----------------------------------

.. automethod:: pylinux.arch.services.Service.install
.. automethod:: pylinux.arch.services.Service.uninstall

.. automethod:: pylinux.arch.services.Service.activate
.. automethod:: pylinux.arch.services.Service.unactivate

.. automethod:: pylinux.arch.services.Service.start
.. automethod:: pylinux.arch.services.Service.stop
.. automethod:: pylinux.arch.services.Service.restart

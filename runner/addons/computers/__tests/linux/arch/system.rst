##################################################
explorer le systeme
##################################################




.. autoclass:: pylinux.system.LinuxSystem

le serveur
====================================

.. automethod:: pylinux.system.LinuxSystem.computer

les fichiers
====================================

.. automethod:: pylinux.system.LinuxSystem.discs
.. automethod:: pylinux.system.LinuxSystem.files

les éléments linux
====================================

.. automethod:: pylinux.system.LinuxSystem.users
.. automethod:: pylinux.system.LinuxSystem.groups
.. automethod:: pylinux.system.LinuxSystem.processes
.. automethod:: pylinux.system.LinuxSystem.services



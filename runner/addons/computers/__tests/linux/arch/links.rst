##################################################
lien entre les éléments
##################################################



User - Group
====================================

.. autoclass:: pylinux.arch.system.UserGroupRelation

User -Process
====================================

.. autoclass:: pylinux.arch.system.UserProcessRelation

User -File
====================================

.. autoclass:: pylinux.arch.system.UserFileRelation


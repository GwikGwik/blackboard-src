##################################################
utilisateurs et groupes
##################################################

les utilisateurs
==========================

.. automethod:: pylinux.arch.user.get_users

.. autoclass:: pylinux.arch.user.User

.. automethod:: pylinux.arch.user.User.__init__
.. automethod:: pylinux.arch.user.User.create
.. automethod:: pylinux.arch.user.User.remove


les groupes
==========================

.. automethod:: pylinux.arch.group.get_groups

.. autoclass:: pylinux.arch.group.Group

.. automethod:: pylinux.arch.group.Group.__init__
.. automethod:: pylinux.arch.group.Group.create
.. automethod:: pylinux.arch.group.Group.remove

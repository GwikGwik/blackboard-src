##################################################
voir les processus
##################################################


lister les processus
=======================

.. literalinclude:: process.py
   :language: python

.. autoclass:: pylinux.arch.process.LinuxProcesses

la classe LinuxProcess
====================================

.. autoclass:: pylinux.arch.process.LinuxProcess

.. automethod:: pylinux.arch.process.LinuxProcess.kill

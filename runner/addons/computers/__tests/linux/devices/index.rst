##################################################
les périphériques
##################################################


interactions utilisateur
==================================================

.. toctree::
   :glob:
   :maxdepth: 2

   mouse
   keyboard
   screen
   webcam
   sound
   
périphériques courants
==================================================

.. toctree::
   :glob:
   :maxdepth: 2

   cdrom
   internet
   scanner
   

périphériques techniques
==================================================
.. toctree::
   :glob:
   :maxdepth: 2

   serial
   arduino

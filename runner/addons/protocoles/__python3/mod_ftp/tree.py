#https://docs.python.org/3/library/ftplib.html
from dataModel import *
from SemanticNode import SemanticNode_Protocole
from ftplib import FTP
from mod_program import *
#=========================================================

class Ftp_Connection(Object):

#=========================================================

    URL=String()

    def connect(self):
        self.ftp = FTP(self.url)     # connect to host, default port
        self.ftp.login() 


    def getwelcome(self):
        return self.ftp.getwelcome()

    def return_data(self,path):
        self.ftp.cwd(path)
        return self.ftp.retrlines('LIST') 

    def disconnect(self):
        self.ftp.quit()

#=========================================================

class Ftp_Protocole(SemanticNode_Protocole):

#=========================================================
    PROTOCOLE="ftp"



#=========================================================

#https://github.com/jan-mue/geometer
#https://geometer.readthedocs.io/en/stable/quickstart.html
PIP3 "geometer"

APT "python3-scipy"
PIP3 "tftb"

########################################
# text
########################################

PIP3 "PyWavelets"
PIP3 "pyyaml"

# https://github.com/clips/pattern
# https://www.springboard.com/blog/data-mining-python-tutorial/
# https://analyticsindiamag.com/hands-on-guide-to-pattern-a-python-tool-for-effective-text-processing-and-data-mining/
PIP3 "pattern3"

# https://dataconomy.com/2015/01/python-packages-for-data-mining/
# https://code.tutsplus.com/tutorials/how-to-work-with-pdf-documents-using-python--cms-25726

########################################
# audio
########################################

APT "portaudio19-dev"
PIP3 "pyaudio"


PIP3 "pydub"
APT "ffmpeg" 
APT "libavcodec-extra"
PIP3 "Pygame"
PIP3 "numpy"
#APT "python3-pygame???"



########################################
# midi
########################################

#https://mido.readthedocs.io/en/latest/index.html#
#APT "libjack0"
#APT "libjack-dev"
PIP3 "mido"
PIP3 "python-rtmidi"

########################################
# osc
########################################

PIP3 "python-osc"

########################################
## opencv for video
########################################


APT "libavcodec-dev"
APT "libavformat-dev"
APT "libswscale-dev"
APT "libv4l-dev"
APT "libxvidcore-dev"
APT "libx264-dev"
APT "libgtk-3-dev"
APT "libatlas-base-dev"
APT "gfortran"
APT "python3-dev"
PIP3 "matplotlib"
#PIP3 "opencv-python"
APT "libopencv-dev"
APT "python-opencv"
APT "python3-opencv"
SUDO "pip install matplotlib --upgrade --ignore-installed six"


APT "python-tk"
APT "python-imaging-tk" #pour la gestion des images sous tkinter
APT "python3-tk" #pour la version 3.x de python. (La version 3.x comprend les widgets ttk)


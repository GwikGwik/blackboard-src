from dataModel import *
from IoNode import IoNode,Pipe
import pygame
import numpy as np
import time
#from scipy.io import wavfile
AUDIO_TYPE="audio"
#===========================================================

class Audio_Node(IoNode):

#===========================================================
    #----------------------------------------------------------------

    def onReceiveMessage(self,message):

        #if hasattr(self,"node") and self.node.get_busy()==True:
        #    self.node.stop()

        if message.data_type==AUDIO_TYPE:
            self.onReceiveAudio(message)
            
    #----------------------------------------------------------------
    def onReceiveAudio(self,message):
        pass
    #----------------------------------------------------------------
    
    def send_audio(self,sound,**args):

        self.send_to_pipe(data=sound,data_type=AUDIO_TYPE,**args)
        
    #----------------------------------------------------------------
    
    def send_file(self,path=None):

        data=pygame.mixer.Sound(path)
        self.parent.send_audio(data)
        
    #----------------------------------------------------------------

#===========================================================

class Audio_Sample(Audio_Node):

#===========================================================

    FILE_PATH=String()
    PLAY=Function()

    #----------------------------------------------------------------
    def onSetup(self):
        self.sound=pygame.mixer.Sound(self.file_path)
        return Audio_Node.onSetup(self)
    #----------------------------------------------------------------

    def play(self):

        self.send_to_pipe(data=self.sound,data_type=AUDIO_TYPE)


#===========================================================

class Audio_Sin(Audio_Node):

#===========================================================
    F=Integer(default=440)
    FS=Integer(default=8000)
    N=Integer(default=10024)
    T0_WAVE=Float(default=0)
    #----------------------------------------------------------------
    def onStart(self):
        self.t=self.n / self.fs
        print(self.t)
        self.next=time.time()

    #----------------------------------------------------------------

    def onDo(self):

        #print(time.time(),self.next)
        if time.time()>= self.next:

            t0=time.time()
            #print(self.f,self.t,t0-self.next)
            self.send_data()
            self.next=t0-0.7*self.t
    #----------------------------------------------------------------
    def send_data(self):

        t = np.arange(self.t0_wave,self.t0_wave+self.t,1/self.fs)
        x = 0.5 * np.sin(2*np.pi*self.f*t)   # 0.5 is arbitrary to avoid clipping sound card DAC
        self.x  = (x*32768).astype(np.int16)  # scale to int16 for sound card

        self.send_audio(self.x,fs=self.fs,T=self.t)
        self.t0_wave+=self.t
    #---------------------------------------------------------------------------

#===========================================================

class Audio_Channel(Audio_Node):

#===========================================================

    CHANNEL=Integer(default=0)
    QUEUED=Boolean(default=False)
    #----------------------------------------------------------------
    def onSetup(self):
        Audio_Node.onSetup(self)
        self.node= pygame.mixer.Channel(self.channel)

    #----------------------------------------------------------------
    def onReceiveAudio(self,message):
        
        if self.node.get_busy()==True:
            self.node.stop()
        #print(self.path(),message)
        if isinstance(message.data,pygame.mixer.Sound):
            sound=message.data
        else:
            sound = pygame.sndarray.make_sound(message.data)

        print(sound)
        if self.queued == True:
            self.node.queue(sound)
        else:
            self.node.stop()
            self.node.play(sound)

    #----------------------------------------------------------------

#===========================================================

class Audio_Mixer(Audio_Node):

#===========================================================

    CHANNELS=Integer(default=1)
    FS=Integer(default=8000)
    #----------------------------------------------------------------

    def onSetup(self):
        Audio_Node.onSetup(self)
        pygame.mixer.pre_init(self.fs, size=-16, channels=1)
        pygame.mixer.init()

        for i in range(self.channels):
            self.append(str(i),cls=Audio_Channel,channel=i,setup=True)

        #self.root.tree()
        #exit()

    #----------------------------------------------------------------

    def onCleanup(self):
        pygame.mixer.stop()

    #----------------------------------------------------------------

    def onReceiveAudio(self,message):
        
        sound = pygame.sndarray.make_sound(message.data)
        sound.play()

    #----------------------------------------------------------------

    def pause(self):
        pygame.mixer.pause()

    #----------------------------------------------------------------

    def unpause(self):
        pygame.mixer.unpause()
    #----------------------------------------------------------------

    def fadeout(self,t=0.0):
        pygame.mixer.fadeout(t)
    #----------------------------------------------------------------




from pydub import AudioSegment
from pydub.playback import play
from pydub.utils import mediainfo
import wave
import numpy as np
import matplotlib.pyplot as plt
from dataModel import *
from mod_program import *
from mod_curve import Curve
from atom import Atom
import time
from scipy import fftpack
import scaleogram as scg
import matplotlib.pyplot as plt
#===========================================================

class Audio_Part(Curve):

#===========================================================

    #------------------------------------------------------------
    def get_audio(self):
        for elt in self.ancestors:
            if isinstance(elt,Audio_File):
                return elt

    #------------------------------------------------------------
    def get_infos(self):
        node=self.get_audio()
        t=np.arange(0, node.t, 1/(2.0*node.frame_rate))
        wav=np.array(node.audio.get_array_of_samples())
        print(t.shape,wav.shape)
        return t,wav

    #------------------------------------------------------------
    def plot(self):
        x,y=self.get_infos()
        plt.plot(x,y)
        plt.show()
    #------------------------------------------------------------
#===========================================================

class Audio_FFT(Audio_Part):

#===========================================================
    #------------------------------------------------------------
    def get_infos(self):
        node=self.get_audio()
        wav=np.array(node.audio.get_array_of_samples())
        #fft=np.fft.fft(wav)
        #frequencies = np.abs(fft)
        sig_fft = fftpack.fft(wav)

        # And the power (sig_fft is of complex dtype)
        power = np.abs(sig_fft)**2

        # The corresponding frequencies
        sample_freq = fftpack.fftfreq(wav.size, d=1/(2*node.frame_rate))
        n=int(sample_freq.shape[0]/2)
        m=int(sample_freq.shape[0]/4)
        return sample_freq[:m],power[:m]
    #------------------------------------------------------------
#===========================================================

class Audio_Spectrum(Audio_Part):

#===========================================================
    #https://github.com/alsauve/scaleogram/blob/master/doc/El-Nino-Dataset.ipynb
    #------------------------------------------------------------
    def plot(self):
        wavelet='cmor0.7-1.5'
        node=self.get_audio()
        t=node.t
        time, signal=self.get_infos()
        fig, (ax1, ax2) = plt.subplots(2, 1, figsize=(14, 6))
        fig.subplots_adjust(hspace=0.3)
        #scales = np.logspace(1.2, 3.1, num=200, dtype=np.int32)
        scales=np.arange(1, 100,2, dtype=np.int32)
        #scales = np.arange(15,600, 4)
        ax = scg.cws(time, signal, scales, wavelet=wavelet,ax=ax2, cmap="jet", cbar=None, ylabel="Period [seconds]", xlabel="Time [seconds]",
        title='Example 1: scaleogram with linear period yscale')


#===========================================================

class Audio_File(Audio_Part):

#===========================================================

    FILE_PATH=String()
    FORMAT=String(default="wav")
    FADEIN=Float(default=None)
    FADEOUT=Float(default=None)
    T=Float(default=None)
    T_MIL=Float(default=None)
    T0=Float(default=0.0)
    FRAME_RATE=Integer(default=44100)

    #------------------------------------------------------------
    def get_audio(self):
        return self

    #----------------------------------------------------------------
    def onSetup(self):
        self.infos=mediainfo(self.file_path)
        self.audio=AudioSegment.from_file(self.file_path,format=self.format)
        self.t=self.audio.duration_seconds
        self.t_mil=len(self.audio)
        self.frame_rate=self.audio.frame_rate

        if self.fadein:
            self.set_fadein(t=self.fadein)

        if self.fadeout:
            self.set_fadeout(t=self.fadeout)

    #----------------------------------------------------------------
    def play(self):
        play(self.audio)
    #----------------------------------------------------------------
    def set_fadein(self,t=None):
        if t:
            self.audio=self.audio.fade_out(duration=int(t*1000))
    #----------------------------------------------------------------
    def set_fadeout(self,t=None):
        if t:
            self.audio=self.audio.fade_in(duration=int(t*1000))

    #------------------------------------------------------------

#===========================================================

class Audio_Playlist(Data):

#===========================================================

    SELECT=String()
    MIMETYPE=String()
    MIX=Float(default=0.0)
    FADEIN=Float(default=None)
    FADEOUT=Float(default=None)

    BUILD_SAMPLE=Function()
    PLAY=Function()
    SAVE=Function()

    FILENAME=String()
    #----------------------------------------------------------------
    def build_sample(self):
        self.t=None
        self.sound=None
        self.last=None

        i=0
        t0=0.0
        self.audio=None
        for elt in self.find(self.select).all().by_class("Disc_File"):
            print(elt,elt.mimetype())
            if elt.mimetype() == self.mimetype:
                sample=self.append(str(i)+"_"+elt.getPath().name(),file_path=elt.file_path,
                                cls=Audio_File,
                                format=self.format,
                                fadein=self.fadein,
                                fadeout=self.fadeout,
                                t0=t0)
                sample.setup()
                t0=float(t0+sample.t_mil-self.mix*1000)
                i+=1
                if self.audio:
                    self.audio = self.audio.append(sample.audio, crossfade=self.mix*1000)
                else:
                    self.audio=sample.audio

        #return Atom.onSetup(self)
    #----------------------------------------------------------------

    def play(self):
        print("play")
        play(self.audio)

    #----------------------------------------------------------------

    def save(self):
        file_handle = self.audio.export(self.filename, format="wav")

    #----------------------------------------------------------------

#===========================================================




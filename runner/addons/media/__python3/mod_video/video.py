from dataModel import *
from IoNode import IoNode
import cv2
#===============================================================================

class OpenCV_VideoRecord(IoNode):

#===============================================================================

    FILE_PATH=String()
    FPS=Integer()
    HEIGHT=Integer(default=400)
    WIDTH=Integer(default=600)
    FORMAT=String(default="MJPG")

    #---------------------------------------------------------------------------
    def onStart(self):
        #fourcc = cv2.VideoWriter_fourcc(*'XVID') # Be sure to use lower case
        #out = cv2.VideoWriter(output, fourcc, 20.0, (width, height))

        #fourcc = cv2.VideoWriter_fourcc('M','J','P','G')
        fourcc=cv2.VideoWriter_fourcc(*self.format)
        self.video = cv2.VideoWriter(self.file_path, fourcc,self.fps, (self.width, self.height))


    #---------------------------------------------------------------------------
    def onStop(self):

        self.video.release()

    #----------------------------------------------------------------

    def onReceiveMessage(self,message):

        img=message[1]
        frame=self.adapt_frame(img)
        print(message[0])
        self.video.write(frame)

    #----------------------------------------------------------------

    def adapt_frame(self,frame):

        try:
            height, width ,layers = frame.shape
        except:
            height, width = frame.shape
            layers=None

        if layers is None:
            frame=cv2.cvtColor(frame, cv2.COLOR_GRAY2RGB)

        frame = cv2.resize(frame, (self.width,self.height), interpolation = cv2.INTER_AREA)

        return frame
    #----------------------------------------------------------------
#===========================================================








#===========================================================

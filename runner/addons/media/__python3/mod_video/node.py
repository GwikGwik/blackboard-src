
from dataModel import *
from mod_program import *
from IoNode import IoNode,Pipe
import numpy as np
import cv2
from matplotlib import pyplot as plt
import time


#===============================================================================

class OpenCV_WebCam(IoNode):

#===============================================================================

    CAM=Integer(default=None)
    LOOP=Integer(default=100)
    DT=Float(default=0.1)
    NODE=Field(default=None)
    #---------------------------------------------------------------------------
    def onSetup(self):
        IoNode_Atom.onSetup(self)
        if self.node is not None:
            pass
        elif self.cam is not None:
            self.node = cv2.VideoCapture(self.cam)
        else:
            print("no web cam",self.cam)

    #---------------------------------------------------------------------------
    def onCleanup(self):
        if self.node:
            self.node.release()

    #---------------------------------------------------------------------------
    def onDo(self):

        i=0
        while i < self.loop:  
            time.sleep(self.dt)
            try:
                ret, frame = self.node.read()
                if ret==True:
                    self.send_video( frame)
                i+=1
            except:
                self.stop()

    #---------------------------------------------------------------------------
    def play(self):
        ret=False
        try:
            ret, frame = self.node.read()

        except:
            print("no cam",self.name)
        if ret==True:
            self.send_video( frame)

    #---------------------------------------------------------------------------
    def send_video(self,message):

        self.send_to_pipe(cam=str(self.cam),data=message)
    #---------------------------------------------------------------------------

#===============================================================================

class OpenCV_WebCamPlot(OpenCV_WebCam):

#===============================================================================
    #---------------------------------------------------------------------------
    def onDo(self):

        i=0
        while i < self.loop:  
            time.sleep(self.dt)
            try:
                ret, frame = self.node.read()
                if ret==True:
                    cv2.imshow(str(self.name),frame)
                i+=1
            except:
                self.stop()

    #---------------------------------------------------------------------------
#===============================================================================

class OpenCV_WebCamManager(Object):

#===============================================================================
    CAM=Integer(default=1)
    #---------------------------------------------------------------------------
    def onSetup(self):
        Object.onSetup(self)

        for i in range(self.cam):
            try:
                node = cv2.VideoCapture(i)
                if node.isOpened():
                    OpenCV_WebCam(parent=self,name=str(i),node=node)
            except:
                pass
    #----------------------------------------------------------------

#===============================================================================

class OpenCV_Window(IoNode):

#===============================================================================
    DT=Float(default="0.1")
    #---------------------------------------------------------------------------
    def onSetupX(self):
        #self.node=
        print("ok")
        cv2.namedWindow(self.name, cv2.WINDOW_NORMAL)
        IoNode_Atom.onSetup(self)

    #---------------------------------------------------------------------------
    def onCleanup(self):
        cv2.destroyAllWindows()

    #----------------------------------------------------------------

    def onReceiveMessage(self,message):


        #print("ok",message.data)
        gray = cv2.cvtColor(message.data, cv2.COLOR_BGR2GRAY)
        
        cv2.imshow(self.name,gray)
        #time.sleep(self.dt)
        #cv2.destroyAllWindows()
        #if cv2.waitKey(1) & 0xFF == ord('q'):
        #    self.root.stop()
        #plt.imshow(gray),plt.show()
        #self.send_output_message(message)
    #----------------------------------------------------------------

#===============================================================================

class OpenCV_FaceDetection(IoNode):

#===============================================================================
    #---------------------------------------------------------------------------
    def onStart(self):
        #self.node=
        self.face_cascade = cv2.CascadeClassifier('./../__etc/data/haarcascades/haarcascade_frontalface_default.xml')
        self.eye_cascade = cv2.CascadeClassifier('./../__etc/data/haarcascades/haarcascade_eye.xml')


    #---------------------------------------------------------------------------
    def onCleanup(self):
        cv2.destroyAllWindows()

    #----------------------------------------------------------------

    def onReceiveMessage(self,message):
        img=message.data
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        faces = self.face_cascade.detectMultiScale(gray, 1.3, 5)
        for (x,y,w,h) in faces:
            cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),2)
            roi_gray = gray[y:y+h, x:x+w]
            roi_color = img[y:y+h, x:x+w]
            eyes = self.eye_cascade.detectMultiScale(roi_gray)
            for (ex,ey,ew,eh) in eyes:
                cv2.rectangle(roi_color,(ex,ey),(ex+ew,ey+eh),(0,255,0),2)
                
        cv2.imshow(message[0],img)

        if cv2.waitKey(50) & 0xFF == ord('q'):
            self.root.stop()
        #plt.imshow(gray),plt.show()
        #self.send_output_message(message)
    #----------------------------------------------------------------

#===============================================================================



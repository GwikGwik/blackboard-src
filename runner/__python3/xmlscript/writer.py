from xml.etree.ElementTree import Element,tostring
#========================================================================

def tree_to_file(filename,node):
    
#========================================================================
    """
    write pytree to an file
    """
    
    f=open(filename,"w")
    f.write(tree_to_string(node))
    f.close()
     
#========================================================================

def tree_to_string(node):

#========================================================================
    """
    convert pytree to xml string
    """

    xmlnode=node_to_xml(node)
    return tostring(xmlnode, encoding="unicode")

#========================================================================

def node_to_xml(node):
    
#========================================================================

    """
    convert node to xml
    """


    attr=dict()
    done=list()

    #print attr
    for k,v in node.attributes():

        done.append(k)

        if v.field.record==True:

            attr[k]=str(v.access())

    #print attr
    for k,v in node.items():

        if k not in done:

            done.append(k)
            attr[k]=str(v)

    if 'text' in attr.keys():
        text=attr['text']
        del attr['text']
        if text is None:
            text="" 
    else:
        text=""



    tag=node.__class__.__name__
    
    root = Element(tag,attrib=attr)
    root.text=text


    for child in node.children:
        root.append(node_to_xml(child))
    return root

#========================================================================



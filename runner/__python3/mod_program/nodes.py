from atom import Atom,TreeLogs
from atom.variables import VariableManager
from dataModel import *
from dataModelAddons.tree import Tree
from dataModel import *

#==========================================================

class Code_Initializer(VariableManager):
    
#==========================================================

    ENV_DONE=False
    TREE_DONE=Boolean(default=False)
    INITIALIZE=Function(TreeLogs())
    CLEANING=Function(TreeLogs())
    #-------------------------------------------------------------
    def initialize(self):


        if Code_Initializer.ENV_DONE == False:

            self.add_environ()
            self.add_command_line()
            Code_Initializer.ENV_DONE=True

        if self.tree_done == False:
            self.tree_done=True
            self.processTree()

            yield self.children.by_class(Global).query(action="do")
            self.processTree()
            yield self.children.by_class(Setup).query(action="do")

    #-------------------------------------------------------------
    def cleaning(self):
        yield self.children.by_class(Cleanup).query(action="do")


#============================================================

class Tree_Condition(metaclass=ModelMeta):

#============================================================
    ACTIVE=Boolean(default=True)
    TEST=Function()

    def test(self):
        return self.active ==True and self.onTest()==True


    def onTest(self):
        """
        return True
        """
        return True

#===============================================================

class Data(Model,Tree,Network_Node):
    
#===============================================================
    """
    base node for data
    """
    VALUE=Field(set_f="onSet",get_f="onGet",del_f="onDel")
    DEFAULT=Field(default=None)
    
    #---------------------------------------------------

    def onInit(self):
        "call setup and name"
        self.autoname()
        self.onSetup()

    #---------------------------------------------------
    def onSetup(self):
        "do nothing"
        pass

    #---------------------------------------------------
    def onSet(self,value):

        return value
    #---------------------------------------------------
    def onGet(self,value):

        return value
    #---------------------------------------------------
    def onDel(self,value):
        self.destroy()
        return
    #---------------------------------------------------
    def add(self,cls=None,value=None,**args):

        if not cls:cls=Data
        cls=self.getClass(cls)
        #print(self.path(),cls,value,args)
        node=cls(parent=self,**args)

        if value is not None:
            node.value=value
        elif self.default is not None:
            node.value=self.default
        #exit()
        return node
    #---------------------------------------------------
    def set_data(self,name=None,cls=None,value=None,error=False,**args):

        node=self.find(name,error=error)

        if node:
            if value is not None:
                node.value=value
            return node
        else:
            return self.add(cls=cls,value=value,name=name,**args)
    #---------------------------------------------------
#==========================================================

class Code_Block(Model,Tree,Network_Node):

#==========================================================
    """
    basic element to build a code tree
    """
    #---------------------------------------------------

    def onInit(self):
        """
        set an automatic name if no name when instanciate object
        """
        self.autoname()



#===============================================================

class Object(Model,Tree,Network_Node,Tree_Initializer,Code_Initializer):
    
#===============================================================
    #IS_OPENED=Boolean(default=False)
    OPEN=Function()
    CLOSE=Function()
    #---------------------------------------------------

    def onInit(self):
        """
        set an automatic name if no name when instanciate object
        """
        self.autoname() 
    #---------------------------------------------------

    def onPreSetup(self):
        """
        set an automatic name if no name when instanciate object
        """
        return self.initialize() 
    #---------------------------------------------------

    def onPostCleanup(self):
        """
        """
        return self.cleaning()

    #-------------------------------------------------------------
    def is_ok(self):

        return True

    #-------------------------------------------------------------
    def open(self):
        if self.is_ok()==True:
            self.is_opened=True
            self.onOpen()


    #-------------------------------------------------------------
    def close(self):

        if self.is_opened==True:
            self.is_opened=False
            self.onClose()

    #-------------------------------------------------------------
    def onSetup(self):pass
    def onOpen(self):
        self.children.by_class(Object).execute("open")

    def onClose(self):
        self.children.by_class(Object).execute("close")
    def onCleanup(self):pass
    def onDestroy(self):pass
    #-------------------------------------------------------------

#===============================================================

class Code(Code_Block,Code_Initializer,Tree_Condition,Tree_Runner):
    
#===============================================================


    #-------------------------------------------------------------
    def onPreSetup(self):
        "code object can initialize global variables for all tree"
        return self.initialize()
    #---------------------------------------------------

    def onPostCleanup(self):
        """
        """
        return self.cleaning()
    #-------------------------------------------------------------
    def call(self):
        "test if ok before calling onCall"

        if  self.active==True and self.test()==True:
            return Tree_Runner.call(self)
    #-------------------------------------------------------------
    def onCall(self):
        "call children code"
        for elt in self.children.by_class(Code):
            yield elt.call()
    #-------------------------------------------------------------
    def onTest(self):
        """
        test all children conditions, return False for first wrong
        """
        for elt in self.children.by_class(Code_Condition):
            if elt.test() !=True:
                return False
        return True
    #-------------------------------------------------------------
#===============================================================

class Code_Condition(Code_Block,Tree_Condition):
    
#===============================================================
    COUNTER=Integer(default=0)
    #---------------------------------------------------
    def reset(self):
        "counter to zero"
        self.counter=0

    def onTest(self):
        """
        test all children conditions, return False for first wrong
        """
        for elt in self.children.by_class(Code_Condition):
            if elt.test() !=True:
                return False
        return True
    #---------------------------------------------------


#===============================================================

class Instruction(Code_Block):
    
#===============================================================
    DO   =Function(TreeLogs())

    def do(self):
        return self.onDo()

    def onDo(self):
        for elt in self.children.by_class(Code):
            yield elt.setup()
            if elt.is_instance("Node_Handler"):
                elt.set_node(self.parent)
            #print("SETUP",elt.path(),self.parent.path())
            yield elt.call()

#===============================================================

class Global(Instruction):
    
#===============================================================
    VALUE=String()


    def onDo(self):
        self.parent.setGlobal(self.name,self.value)


#===============================================================

class Setup(Instruction):
    
#===============================================================
    
    pass


#===============================================================

class Cleanup(Instruction):
    
#===============================================================

    pass





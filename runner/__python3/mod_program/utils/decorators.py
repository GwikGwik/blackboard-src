from dataModel import String,Integer,Field
import subprocess,shlex
from atom import Log_Message
#==========================================================================

def clean_lines(lines):

#==========================================================================

    for line in lines:
        #print(line)
        try:
            line=line.decode("utf-8")
            yield line.replace("\n","")
        except:
            yield line.replace(b"\n",b"")
#==========================================================================

class Log_CommandResult(Log_Message):

#==========================================================================
    COMMAND=String()
    CODE=Integer()
    ERR=Field()
    OUT=Field()

def get_pre_command(node):
    if "command_prefix" in node.keys():
        return node.command_prefix
    for elt in node.ancestors:
        if "command_prefix" in elt.keys():
            return elt.command_prefix
    return ""
#==========================================================================

def get_result(text,returncode,out=None,err=None):

#===================================================================
    root= Log_CommandResult(command=text,code=returncode)
    Log_Message(parent=root,name="out",text=out.decode("utf-8"))
    Log_Message(parent=root,name="err",text=err.decode("utf-8"))
    return root
#==========================================================================

def execute(text,stdin=None):

#===================================================================

    #commandargs = shlex.split(text)
    #text=get_pre_command(node)+" "+text
    if stdin is not None:
        proc = subprocess.Popen(text, shell=True,stdin=subprocess.PIPE,stderr=subprocess.PIPE,stdout=subprocess.PIPE)
        out,err=proc.communicate(input=str.encode(stdin))
        return get_result(text,proc.returncode,out=out,err=err)
        #out=list(clean_lines(out.split(b"/n") ))
        #err=list(clean_lines(err.split(b"/n") ))
        #return Log_CommandResult(command=text,code=returncode,out=out,err=err,input=stdin)
        #print(returncode,out,err)

    else:
        proc = subprocess.Popen(text, shell=True,stderr=subprocess.PIPE,stdout=subprocess.PIPE)
        #proc.wait()

        #out=list(clean_lines(proc.stdout.readlines() ))
        #err=list(clean_lines(proc.stderr.readlines() ))

        #print(returncode,out,err)
        #return Log_CommandResult(command=text,code=returncode,out=out,err=err)
        out,err=proc.communicate()
        return get_result(text,proc.returncode,out=out,err=err)
#===================================================================

def command(function):

#==========================================================================


    def CommandFunction(*args,**kwargs):

        text=function(*args,**kwargs)
        return execute(text)


    return CommandFunction

#===================================================================




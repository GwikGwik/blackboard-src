from dataModel import *
from .handler import *
from ..nodes import Tree_Condition,Object
#============================================================

class Action_Execution(Node_Handler):

#============================================================

    #-------------------------------------------------
    def onNextCall(self,node,**args):
        #print("call",self.path(),node.path())
        return self.onProcessElement(node,**args)                

    #-------------------------------------------------
    def onTestNode(self,node):

        for test in self.children.by_class(Node_Condition):
            if test.onTestNode(node) !=True:
                return False
        return True 
    #-------------------------------------------------
    def onProcessElement(self,node,**args):

        if self.onTestNode(node)==True:
            for child in self.children.by_class("Code"):

                if isinstance(child,Action):
                    child.set_node(node)
                    yield child.call()                
                else:
                    yield child.call()  


#============================================================

class Function(Object,Action_Execution):

#============================================================

    def onNextCall(self,node):
        print(self.path(),node.name)
        return self.onProcessElement(node)                

#============================================================

class Module(Object,Action_Execution):

#============================================================

    pass
#============================================================

class Action(Code,Action_Execution):

#============================================================
    #-------------------------------------------------
    def onTest(self):
        return self.onTestNode(self.get_node())  

    #-------------------------------------------------
    def onCall(self):

        return self.onNextCall(self.get_node())
    
    #-------------------------------------------------
  

#============================================================

class Call(Action):

#============================================================
    URL=String()

    #-------------------------------------------------
    def onNextCall(self,node):
        x=self.find(self.url)
        if x.onTestNode(node):
            return x.onNextCall(node)
        else:
            print("X"*15,"error when callling",self.url)
    
    #-------------------------------------------------
#==================================================================

class Echo(Action):

#==================================================================
    """

    """
    def onNextCall(self,node):
        print(self.text%node)#.get_infos())

#============================================================

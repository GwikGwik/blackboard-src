

from .handler import Node_Condition
from dataModel import *

#============================================================

class IsInstance(Node_Condition):

#============================================================

    CLS=String()


    #-------------------------------------------------
    def onTestNode(self,node):
        return node.is_instance(self.cls)
    
    #-------------------------------------------------
#============================================================

class Avoid(Node_Condition):

#============================================================

    ATTR=String()
    VALUE=String()
    #-------------------------------------------------
    def onTestNode(self,node):

        if self.attr in node.keys():
            if node[self.attr] != self.value:
                return True
        return False
    
    #-------------------------------------------------
#============================================================

class HasAttribute(Node_Condition):

#============================================================

    ATTR=String()
    #-------------------------------------------------
    def onTestNode(self,node):

        return hasattr(node,self.attr )
    
    #-------------------------------------------------
#============================================================

class IsEqual(Node_Condition):

#============================================================

    ATTR=String()
    VALUE=String()
    #-------------------------------------------------
    def onTestNode(self,node):

        return (node[self.attr] == self.value )
    
    #-------------------------------------------------
#============================================================

class IsTrue(Node_Condition):

#============================================================

    TEXT=String()
    CONDITION=String(default=True)
    #-------------------------------------------------
    def onTestNode(self,node):

        return (eval(self.text%node)==self.condition )
    
    #-------------------------------------------------
#============================================================

class IsNotNull(Node_Condition):

#============================================================

    ATTR=String()

    #-------------------------------------------------
    def onTestNode(self,node):

        if self.attr in list(node.keys()):
            return (node[self.attr] != None )
        return False
    #-------------------------------------------------

#============================================================


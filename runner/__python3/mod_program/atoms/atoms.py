
from .controls import Action
from dataModel import *


#============================================================

class New(Action):

#============================================================

    CLS=Field(default="Atom")

    #-------------------------------------------------
    def onNextCall(self,node):
        cls=getClass(self.cls)
        inst=cls(parent=node,**dict(self.getNonAttributeDict()))
        inst.query("setup")
        #return self.processChildren(inst)
    
    #-------------------------------------------------
#============================================================

class Set(Action):

#============================================================

    ATTR=String()
    VALUE=String()

    #-------------------------------------------------
    def onNextCall(self,node):
        node[self.attr]=self.onSet(node)

    #-------------------------------------------------
    def onSet(self,node):
        print(node.path(),self.value)
        return self.value%dict(node)
    
    #-------------------------------------------------

#============================================================

class Query(Action):

#============================================================

    ACTION=String()

    #-------------------------------------------------
    def onNextCall(self,node):
        #print( dict(self.getNonAttributeDict()) )
        #print( getattr(node,self.action)) 
        d=dict(self.getNonAttributeDict())
        return node.query(action=self.action,**d)#,self.getNonAttributeDict()))
    #-------------------------------------------------
#============================================================

class Update(Action):

#============================================================

    #-------------------------------------------------
    def onNextCall(self,node):
        for k,v in self.onIterData(node):
            node[k]=v

    #-------------------------------------------------
    def onIterData(self,node):
        pass
    
    #-------------------------------------------------
#============================================================

class UpdateChildren(Action):

#============================================================
    CLS_DEFAULT="Object"
    #-------------------------------------------------
    def onSetup(self):
        self.cls_obj=getClass(self.__class__.CLS_DEFAULT)

    #-------------------------------------------------
    def onNextCall(self,node):

        for k,v in self.onIterData(node):
            self.add(node,k,**v)

    #-------------------------------------------------
    def add(self,node,k,**v):
        child=node.find(k)
        if child is None:
            self.cls_obj(parent=node,name=k,**v)
        else:
            child.update(v)

    #-------------------------------------------------
    def onIterData(self,node):
        pass
    
    #-------------------------------------------------
#============================================================

class UpdateTree(UpdateChildren):

#============================================================
    CLS_DEFAULT="Object"
    GROUP_DEFAULT="Object"
    SEPARATOR_DEFAULT='/'
    #-------------------------------------------------
    def onSetup(self):
        self.cls_obj=getClass(self.__class__.CLS_DEFAULT)
        self.cls_group=getClass(self.__class__.GROUP_DEFAULT)
    #-------------------------------------------------
    def add(self,node,k,**v):
            lst=k.split(self.__class__.SEPARATOR_DEFAULT) 
            name=lst[-1]
            root=self.__class__.SEPARATOR_DEFAULT.join(lst[:-1])
            print(root,name)
            parent=node.append(root,cls=self.cls_group)
            UpdateChildren.add(self,parent,name,**v)
    
    #-------------------------------------------------

#============================================================



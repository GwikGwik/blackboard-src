# -*- coding: utf-8 -*-
from dataModel import *
from atom import treelogs
from .controls import Action
from .controls import Tree_Condition
from .controls import Node_Condition
#===================================================================

class Iterator(Action):
    
#===================================================================

    """
    exemple ::

       it=Iterator(select=node)
       for elt in it():
           ...
        
    recursive behavior if children ::

       Iterator(parent=it)
       Iterator(parent=it)
       for elt in it():
           ...
    """
    #----------------------------------------------------------------
    def onTest(self):
        return True


    #----------------------------------------------------------------
    def __iter__(self):

        node=self.get_node()
        return self.iter(node)

    #----------------------------------------------------------------
    def iter(self,node):

        for elt in self.onIter(node):
            if self.onTestNode(elt)==True:
                yield elt

    #----------------------------------------------------------------

    def onIter(self,node):
        """
        return selected node
        """
        yield node

    #-------------------------------------------------
    def onNextCall(self,node):
        """
        for each elt process children action
        """
        for elt in self.iter(node):
            yield self.onProcessElement(elt)
        

    #-------------------------------------------------

    def testSize(self,app):
        self.selected_node=app
        return self.size() >0

 
    #-------------------------------------------------

    def size(self):
        """
        number of returned objects
        """
        return len(list(self.__iter__()))

    #TODO: bof supprimer les deux fonctions
    #----------------------------------------------------------------
    @treelogs
    def execute_on_all(self,method=None,**args):
        for elt in self:
            yield elt.query(action=method,**args)

  #-------------------------------------------------

    def get_elements(self,app):
        self.selected_node=app
        return self.__iter__()

    #-------------------------------------------------


#====================================================================




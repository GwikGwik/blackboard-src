
from dataModel import *
from atom import treelogs,Atom
from ..nodes import Code,Code_Block,Tree_Condition
from dataModelAddons.tree import Tree


#============================================================

class Node_Handler(metaclass=ModelMeta):

#============================================================

    """
    default behavior : pass selected_node to children and process children
    """
    SELECT=String(default=None)
    SELECTED_NODE=Field(record=False,default=None)

    #-------------------------------------------------
    def set_node(self,node):
        self.selected_node=node

    #-------------------------------------------------
    def get_node(self,error=True):
        
        #with path, searching relatively to self
        if self.select is not None:
            self.selected_node = self.find(path=self.select)

        #return or error
        if self.selected_node is not None:
            return self.selected_node
            
        elif error ==True:
            raise Exception(self.__class__.__name__,self.path(),"no path",self.select)

#============================================================

class Node_Condition(Code_Block,Tree_Condition):

#============================================================

    #-------------------------------------------------
    def get_handler(self):

        for elt in self.ancestors:
            if isinstance(elt,Handler)==True:
                return elt


    #-------------------------------------------------
    def test(self):
        return self.onTestNode(self.parent.get_node())

    #-------------------------------------------------
    def onTestNode(self,node):
        return True


    #-------------------------------------------------

#============================================================


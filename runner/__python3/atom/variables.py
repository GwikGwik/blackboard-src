import sys,os
from dataModel import *
#==========================================================

class VariableManager(metaclass=ModelMeta):
    
#==========================================================
    SHORTCUTS_LIST=dict()
    GLOBALS_LIST=dict()
    FORBIDEN_KEYWORDS=["class","def","list"]

    SHORTCUTS=Function()
    GLOBALS=Function()
    ADD_ENVIRON=Function()
    ADD_COMMAND_LINE=Function()

    #---------------------------------------------------------
    #SHORTCUTS    
    #---------------------------------------------------------
    def shortcuts(self):
        keys= list(VariableManager.SHORTCUTS_LIST.keys())
        keys.sort()
        for elt in keys:
            yield elt,VariableManager.SHORTCUTS_LIST[elt]
    #---------------------------------------------------------
    def getShortcut(self,name):
        if name not in VariableManager.SHORTCUTS_LIST.keys():
            raise Exception("no shortcut",name,self.path())
        return VariableManager.SHORTCUTS_LIST[name]
    #---------------------------------------------------------

    def setShortcut(self,name,value):
        VariableManager.SHORTCUTS_LIST[name]=value
            
    #---------------------------------------------------------
    #GLOBALS
    #---------------------------------------------------------

    def globals(self):
        keys= list(VariableManager.GLOBALS_LIST.keys())
        keys.sort()
        for elt in keys:
            yield elt,VariableManager.GLOBALS_LIST[elt]
    
    #---------------------------------------------------------

    def getGlobal(self,name):
        return VariableManager.GLOBALS_LIST[name]
    
    #---------------------------------------------------------
    #add variables
    #---------------------------------------------------------

    def setGlobal(self,name,value):
        try:
            value=eval(value)
        except:
            pass
        VariableManager.GLOBALS_LIST["$"+name]=value
 
    #---------------------------------------------------------

    def add_environ(self):
        
        for k,v in os.environ.items():
            self.setGlobal(k,v)

    #---------------------------------------------------------
    def add_command_line(self):

        i=0
        
        for elt in sys.argv:

            if elt.find('=')<0:
                name=str(i)
                value=elt
                i+=1

            else:
                name,value=elt.split('=')
                
            try:
                value=eval(value)
            except:
                pass

                self.setGlobal(name,value)

    #---------------------------------------------------------
    #modify nodes
    #---------------------------------------------------------
    def processTree(self):
        self.processNode(self)
        for elt in self.all():
            self.processNode(elt)
    
    #---------------------------------------------------------

    def processNode(self,node):
        #print(node.path())

        for k,v in node.items():
            #print("    ",k,v)
            result=self.processVariable(v)
            if result is not None:
                node[k]=result


    #---------------------------------------------------------
    def processVariable(self,string):

        if type(string) != str:
            return
        
        result= self.processString(string)

        if result in VariableManager.FORBIDEN_KEYWORDS:
            return Exception("keyword forbidenn %s"%(result))
        

        #try:
            #result=eval(result)
        #except:
            #pass

        return result
            
            
    #---------------------------------------------------------
    def processString(self,string):

        for k,v in VariableManager.GLOBALS_LIST.items():
            if k in string:
                string=string.replace(k,str(v) )
        return string
    #---------------------------------------------------------



#==========================================================

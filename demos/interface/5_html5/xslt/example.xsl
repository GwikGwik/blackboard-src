<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:output method="text"/>

  <xsl:template match="/">
    <h1>text</h1>
    <p>Article - <xsl:value-of select="/Article/Title"/></p>
    <p>Authors: <xsl:apply-templates select="/Article/Authors/Author"/></p>
  </xsl:template>

  <xsl:template match="Author">
    - <xsl:value-of select="." />
  </xsl:template>

</xsl:stylesheet>

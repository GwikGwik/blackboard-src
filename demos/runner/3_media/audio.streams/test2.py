import sounddevice
from wave import x,fs
sounddevice.play(x,fs)  # releases GIL
sleep(1)  # NOTE: Since sound playback is async, allow sound playback to finish before Python exits


from content import *

root=Atom()

data=DataSet(parent=root,name="data")

vector=data.List(name="list",cls_model="Number")
print("ok")
for i in range(10):
    vector.value=i
print(vector.value)
#vector.show()


#Dict
d=data.Dict(name="list",cls_model="Number")
d.value=dict(a=1,b=2,c="toto")
print(d.value)


vector=data.Vector(name="list",cls_model="Number")

vector.value=list(range(10))
vector.tree()
print(vector.value)

print(data.I)


vector=data.RGB(name="color")

vector.value=(1.0,0.5,0.0)
vector.tree()
print(vector.value)

vector=data.RGBA(name="color")

vector.value=(1.0,0.5,0.0,0.7)
vector.tree()
print(vector.value)



vector=data.FixedVector(name="fixe",size=5)

vector.value=list(range(10))
vector.tree()
print(vector.value)


vector=data.List(name="listpoints",cls_model="Vector2d",size=5)

vector.value=list(range(10))
#vector.tree()
print(vector.value)


vector=data.List(name="listpoints",cls_model="Vector3d",size=5)

vector.value=list(range(10))
#vector.tree()
print(vector.value)

Number(value=10.0)
print(List(value=[0,1,2]).value)
List(value=[0,1,2]).tree()


print(RGB(value=[0,0,0.5]).value)
RGB(value=[0,0,0.5]).tree()

data.tree()
print(data.value)

setData(data.value).tree()

from tkinter import *
from belfrywidgets import TabbedNoteBook

def _closeit(name):
    print("Close tab %s" % name)
    return True  # Return True to allow closing tab.

tk = Tk()
tnb = TabbedNoteBook(tk, width=640, height=480)
tnb.pack_propagate(False)  # Keep noteboox from shrinking to fit contents.
tnb.pack(side=TOP, fill=BOTH, expand=1)

pane1 = tnb.add_pane(
    'one', 'First Pane',
    closecommand=lambda: _closeit('one')
)
lbl1 = Label(pane1, text="This is a label.")
lbl1.pack(side=TOP, fill=BOTH, expand=1)

pane2 = tnb.add_pane(
    'two', 'Second Pane',
    closecommand=lambda: _closeit('two')
)
lbl2 = Label(pane2, text="This is a second label.")
lbl2.pack(side=TOP, fill=BOTH, expand=1)

pane3 = tnb.add_pane(
    'three', 'Third Pane',
    closecommand=lambda: _closeit('three')
)
lbl3 = Label(pane3, text="This is a third label.")
lbl3.pack(side=TOP, fill=BOTH, expand=1)

lbl = tnb.pane_label('two')
lbl.config(text="Tab 2")

tk.mainloop() 

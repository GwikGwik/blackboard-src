from belfrywidgets import CollapsiblePane
from tkinter import *
tk = Tk()
cp = CollapsiblePane(
    tk,
    text="Click Here to Collapse",
    visible=True,
    collapsible=True,
)
cp.pack(side=TOP, fill=BOTH, expand=1, padx=5, pady=5)
lbl1 = Label(cp.holder, text="This is a label.")
lbl2 = Label(cp.holder, text="This is another label.")
lbl1.pack(side=TOP)
lbl2.pack(side=TOP)
tk.mainloop()
 

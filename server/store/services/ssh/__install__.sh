
#ssh server
#=====================================


APT "openssh-server"

#documentation
#=====================================
WEB "https://doc.ubuntu-fr.org/ssh"

#TUTO et DOC
#---------------------------------------
WEB "http://www.octetmalin.net/linux/tutoriels/ssh-installer-configurer-infrastructure-client-serveur.php"
WEB "http://www.octetmalin.net/linux/tutoriels/ssh-installer-configurer-infrastructure-client-serveur.php"
WEB "http://www.octetmalin.net/linux/tutoriels/ssh-fichier-authorized_keys.php"
WEB "http://www.octetmalin.net/linux/tutoriels/ssh-agent-commande-ssh-add-gestion-des-cles-et-passphrase-en-memoire.php"
WEB "http://www.octetmalin.net/linux/tutoriels/ssh-fichier-etc-sshd_config-configuration-machine-serveur.php"
WEB "http://www.octetmalin.net/linux/tutoriels/ssh-fichier-etc-ssh_config-configuration-machine-client.php"

WEB "https://help.ubuntu.com/community/SSH"
WEB "https://help.ubuntu.com/community/SSH/OpenSSH/Keys"
WEB "https://help.ubuntu.com/community/SSH/OpenSSH/Configuring"

WEB "http://manpages.ubuntu.com/manpages/trusty/en/man5/sshd_config.5.html"

# sans mot de passe
#---------------------------------------
WEB "http://korben.info/login-ssh-sans-mot-de-passe.html"
WEB "http://www.git-attitude.fr/2010/09/13/comprendre-et-maitriser-les-cles-ssh/"

EXPOSE "ssh" "22"
LINUX_SERVICE "ssh"
LINUX_LOG "/var/log/auth.log"

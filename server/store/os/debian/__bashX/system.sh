
#=============================================================

function APT_UPDATE() {

#=============================================================

    #Script_paragraph "APT_UPDATE"
    ROOT "apt-get -y update"


}
#=============================================================

function APT_UPGRADE() {

#=============================================================

    #Script_paragraph "APT_UPGRADE"
    ROOT "apt-get -y upgrade"

}

#=============================================================

function APT_DIST_UPGRADE() {

#=============================================================

    #Script_paragraph "APT_DIST_UPGRADE"
    ROOT "apt-get -y dist-upgrade"

}
#=============================================================

function APT_RECONFIGURE() {

#=============================================================
    #Script_paragraph "APT_RECONFIGURE"
    ROOT "dpkg --configure -a"

}
#=============================================================

function APT_AUTOREMOVE() {

#=============================================================
    #Script_paragraph "APT_AUTOREMOVE"
    ROOT "apt -y autoremove"

}
#=============================================================

function APT_CLEAN_INSTALL() {

#=============================================================

    APT_RECONFIGURE
    APT_UPDATE
    APT_UPGRADE
    APT_DIST_UPGRADE
    APT_AUTOREMOVE
}
 
#=============================================================



#=============================================================

function PPA_FROM_URL() {

#=============================================================
    local ppa_name="$1"
    local ppa_key="$2"
    shift 2
    local ppa_url="$@"

    SYSTEMFILE_ADD "/etc/apt/sources.list.d/$ppa_name.list" "deb $ppa_url"
    RUN "curl -sL $ppa_key | sudo apt-key add -"
    APT_UPDATE

}
#=============================================================

function PPA() {

#=============================================================

    EXTERNAL_PACKAGE "ppa" $@
    ROOT "add-apt-repository -y ppa:$1"
    APT_UPDATE
}

#=============================================================

function DEB() {

#=============================================================

    EXTERNAL_PACKAGE "deb" $@

    url=$1
    name=$(basename $url)

    if [ ! -f $DEB_PATH/$name ]
    then
        RUN "wget -O $DEB_PATH/$name $url"
    fi
    
    ROOT "dpkg -i $DEB_PATH/$name"



}
#=============================================================

function APT() {

#=============================================================


    EXTERNAL_PACKAGE "apt" "$@"

    ROOT "apt-get -y install $@"

}
#=============================================================

function APT_REMOVE() {

#=============================================================

    #Script_paragraph "APT_REMOVE $@"
    ROOT "apt-get -y autoremove --purge $1"
}

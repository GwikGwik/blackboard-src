function SWARM_NODE() {

    NEW "$1/cloud.docker.node" "cls=Program_Thread"
    NEW "$1/cloud.docker.local" "cls=Program_Thread"
}

function SWARM_MASTER() {

    SWARM_NODE $1
    NEW "$1/cloud.docker.master" "cls=Program_Thread"
}


function BLACKBOARD_NODE() {

    NEW "$1/cloud.blackboard.node" "cls=Program_Thread"
    NEW "$1/cloud.blackboard.os" "cls=Program_Thread"
}

function BLACKBOARD_MASTER() {

    BLACKBOARD_NODE $1
    NEW "$1/cloud.blackboard.master" "cls=Program_Thread"
}

function BLACKBOARD_STORE() {

    NEW "$1/cloud.blackboard.repository" "cls=Program_Thread"
}

function BLACKBOARD_INTERFACE() {

    NEW "$1/cloud.blackboard.$2" "cls=Program_Thread"
}

function SSH_CLIENT() {

    NEW "$1/cloud.ssh.client"  "cls=Program_Thread"
}

function SSH_SERVER() {

    SSH_CLIENT $1
    NEW "$1/cloud.ssh.server"  "cls=Program_Thread"
}

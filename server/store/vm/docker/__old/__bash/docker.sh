
function WEBSITE() {
    # $1 : nom
    # $2 : image
    LIST "WEBSITE $@"
    #NEW "$Object_url/run.on-network/" "path=websites.$1&clsX=Docker_Compose"

    local Docker_path="$BLACKBOARD_VAR/__servers/$Object_name/$1"
    #DIR "$Docker_path"
    #RSYNC_MIRROR "$BLACKBOARD_STORES/web.websites/$2" "$Docker_path"
    
}

function DOCKER() {
    # $1 : nom
    # $2 : image
    LIST "DOCKER $@"
    #NEW "$1" "cls=Docker_Compose"

    local Docker_path="$COMPUTER_VAR/docker/$1"
    DIR "$Docker_path"
    #DIR "$Docker_path"
    #RSYNC_MIRROR "$BLACKBOARD_STORES/vm.docker.images/$2" "$Docker_path"
    
}
function DOCKER2() {
    ELEMENT "$1"
    INSTANCE  "$1" "service"
    ELEMENT  "$3/services/docker.images/$2"
    CONNECT "require" "$1" "$3/services/docker.images/$2"
    CONNECT "runOn" "$1" "$3/services/docker"
    #APP "vm.docker_images/$3"
    #OBJECT_ELEMENT "$2"
    #REQUIRE "require" "$Object_url/$2" "$STORE_PATH/vm.docker_images/$3" 
    #REQUIRE "runOn" "$Object_url/$2" "$Object_url/$1" 
}

function VIRTUALISATION() {
        echo ELEMENT "$computer_name/services/$1"
    #APP "$BLACKBOARD_INSTALL_OS.services/$1"
}



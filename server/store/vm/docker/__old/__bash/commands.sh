
#==================================================================================

function DOCKER_SERVICE() {

#==================================================================================

    local Service_image=$1
    local Service_name=$2
    
    shift 2

    MESSAGE "DOCKER_SERVICE $@"
    RUN "docker run -d --name $Service_name $Service_image $@"


} 

#==================================================================================

function DOCKER_SCRIPT() {

#==================================================================================

    local Service_image=$1
    local Service_name=$2
    local Script_path=$3

    shift 3
    
    MESSAGE "DOCKER_SCRIPT $@"
    RUN "docker run --name $Service_name -i $@ $Service_image /bin/bash < $Script_path"


} 


#==================================================================================

function DOCKER_REMOVE() {

#==================================================================================

    docker rm -f -v $1
    
}
#==================================================================================

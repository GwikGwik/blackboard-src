Node_run
GLOBAL "DOCKER_TEMPLATES" "$Teamworks_path/src/store/docker"
GLOBAL "DOCKER_MACHINES" "$(Node_run_path)/machines"

function DOCKER_MACHINE_NEW() {

    local VM_NAME="$1"
    local VM_PATH="$DOCKER_MACHINES/$1"

    local TEMPLATE_NAME="$2"
    local TEMPLATE_PATH="$DOCKER_TEMPLATES/$2"
    
    DIR "$VM_PATH"

    rsync -zvr $TEMPLATE_PATH/ $VM_PATH
    cd $VM_PATH && docker-compose up

}

function DOCKER_MACHINE() {

    local VM_NAME="$1"
    local VM_PATH="$DOCKER_MACHINES/$1"

    shift 1
    
    DIR_CHECK "$VM_PATH"
    FILE_CHECK "$VM_PATH/docker-compose.yml"

    cd $VM_PATH && docker-compose $@

}
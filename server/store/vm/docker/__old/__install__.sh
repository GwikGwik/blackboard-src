#https://docs.docker.com/compose/compose-file
PROGRAM "docker"
PROGRAM "docker-compose"
STORE "$INSTALL_PLUGINS/vm.docker"

APT "apt-transport-https"
APT "ca-certificates"
APT "curl"
APT "gnupg2"
APT "software-properties-common"

#curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -

#sudo add-apt-repository \
#   "deb [arch=amd64] https://download.docker.com/linux/debian \
#   $(lsb_release -cs) \
#   stable"


APT "docker-compose"

#sudo groupadd docker
#sudo usermod -aG docker $USER
#sudo apt-get install docker-ce docker-ce-cli containerd.io

#https://docs.docker.com/install/linux/docker-ce/debian/

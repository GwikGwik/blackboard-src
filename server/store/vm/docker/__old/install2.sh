
ICON "http://blog.xebialabs.com/wp-content/uploads/2015/09/docker.png"

#sttelites
# trop bien
LINK "https://access.redhat.com/blogs/1169563/posts/1448083"
LINK "https://github.com/rocker-org/rocker/wiki/managing-users-in-docker"
LINK "http://panamax.io/"

#DOCKER
#======================================
#https://docs.docker.com/engine/installation/linux/ubuntu


APT "apt-transport-https"
APT "ca-certificates"
APT "curl"
APT "software-properties-common"
    
RUN "curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -"
ROOT "apt-key fingerprint 0EBFCD88"

ROOT "add-apt-repository \"deb [arch=armhf] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable\""
   

APT_UPDATE
APT "docker-ce"

LINUX_GROUP "docker"
USER_ADD_GROUP "\$USER" docker

#DOCKER
#========================================================

#echo "deb http://get.docker.io/ubuntu docker main" >  "/etc/apt/sources.list.d/docker.list"

#sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 36A1D7869245C8950F966E92D8576A8BA88D21E9
#sudo apt-get update
#sudo apt-get install lxc-docker



LINUX_SERVICE "docker"

APT "apt-transport-https"
APT "ca-certificates"
APT "curl"
APT "gnupg2"
APT "software-properties-common"

APT "docker-compose"

sudo groupadd docker
sudo usermod -aG docker $USER
sudo apt-get install docker-ce docker-ce-cli containerd.io


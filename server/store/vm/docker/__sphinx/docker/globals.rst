
#############################
variables globales
#############################

docker-compose
=============================

les variables
-----------------------------------

voir https://docs.docker.com/compose/environment-variables/

écrire avec les variables :

   web:
     image: "webapp:${TAG}"

déclarer une variale :

   web:
     environment:
       - DEBUG=1

utiliser une variable :

   web:
     environment:
       - DEBUG



le fichier .env
-----------------------------------

voir https://docs.docker.com/compose/environment-variables/


les variable importantes
-----------------------------------

voir https://docs.docker.com/compose/reference/envvars/

* COMPOSE_PROJECT_NAME : changer le nom du projet
* COMPOSE_FILE : changer le nom du fichier conf



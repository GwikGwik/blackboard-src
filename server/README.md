# blackboard -programme principal

## mise en place de l'environnement

- python : les répertoires __python3 dans $PYTHONPATH
- bash : les répertoires __bin dans $PATH

## accès aux commandes

au demarrage :

   export MAIN_PATH="/path/for/files"
   source "$MAIN_PATH/src/main/setup.sh"

ensuite :

   @help #liste des commandes



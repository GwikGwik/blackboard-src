var DEBUG=false;
DEBUG=true;
var DEBUG_LEVEL=4;
var CLOCK = new Date();
var DT=100;
var ip="http://192.168.1.14";
var js_list=[];
var js_i=0;
//========================================================

function from_xml(parent,xmlnode){

//========================================================
    //console.log(xmlnode.attributes["name"].value);
    //console.log(xmlnode.tagName,xmlnode.attributes);
    var cls=eval(xmlnode.tagName);

    var data={};
    data["text"]=true;
    for (var i = 0; i < xmlnode.attributes.length; i++) {
        var attrib = xmlnode.attributes[i];
        data[attrib.name]=attrib.value;
    }


    if(data.text==true){
            data["text"]=xmlnode.textContent;
    }
    if(data.text=="false"){
            data["text"]="";
    }



    //console.log(cls,parent);
    var node=new cls(parent,data)
    //console.log(node);
    //console.log(cls);
    //var node=new cls(parent,xmlnode.attributes["name"].value,xmlnode.attributes["url"].value)

    for (x of xmlnode.children ) {
        from_xml(node,x);
    }
    //console.log(cls,node.path(),node.children.size());
    return node
}

//========================================================
var COUNT=0;

//========================================================

class Base {

//========================================================
/*

*/

    //----------------------------------------------------
    constructor(name){
    //----------------------------------------------------
        this.msg("NEW",{},1);
        if( name) {
            this.name = name;
        }else{
            this.name = "NODE_"+COUNT;
            COUNT=COUNT+1;
        }

    }
    //----------------------------------------------------
    destroy(){
    //----------------------------------------------------
        this.msg("DESTROY",{},1);
    }
    //----------------------------------------------------
    getClass(){
    //----------------------------------------------------
        return this.constructor.name;
    }
    //----------------------------------------------------
    msg(tag,data,level=9){
    //----------------------------------------------------
        if ( DEBUG && level > DEBUG_LEVEL ) {
            var sep="  -  ";
            var l0=15;
            var string=tag;
            if (string.length>l0){
                string=string.slice(0,l0);
            }else{
                string=string+" ".repeat(l0-string.length);
            }
            string=sep+string+sep;
            console.log(level,string,this.getClass(),this.path(),data);
        }

    }
    //----------------------------------------------------
    tree(level=1){
    //----------------------------------------------------

            var sep="   | ";
   
            var string=sep.repeat(level)+this.getClass()+" - "+this.name;
            console.log(string);
            this.children.execute('tree',level+1)


    }
    //----------------------------------------------------

}
//========================================================
//========================================================

class Store {

//========================================================
/*
    crée une arborescence parent/enfants pour gerer les objets
    utiliser par héritage pour creer différents objets
    imbricables
*/

    //----------------------------------------------------
    constructor(lst=null){
    //----------------------------------------------------
        if (lst){
            this.elements=lst;
        }else{
            this.elements=[];
        }

        this.STORE=Store;

    }
    //----------------------------------------------------
    destroy(){
    //----------------------------------------------------
        this.execute("destroy");
        this.elements=[];
    }
    //----------------------------------------------------
    destroy_class(cls){
    //----------------------------------------------------
        this.by_class(cls).destroy();

    }

    //----------------------------------------------------
    empty(){
    //----------------------------------------------------
        return (this.elements.length == 0);
    }
    //----------------------------------------------------
    size(){
    //----------------------------------------------------
        return this.elements.length;
    }

    //----------------------------------------------------
    first(){
    //----------------------------------------------------
        if (this.empty() ){
            return null;
        }else{
            return this.elements[0];
        }
    }
    //----------------------------------------------------
    iter(){
    //----------------------------------------------------
        return this.elements;
    }
    //----------------------------------------------------
    append(elt){
    //----------------------------------------------------
        //console.log(this.elements,elt);
        if( elt instanceof Store){

            this.elements=this.elements.concat(elt.elements); 
        }else if(Array.isArray(elt)){
            this.elements=this.elements.concat(elt); 
        }else{

            this.elements.push(elt);
        }

    }
    //----------------------------------------------------
    remove(elt){
    //----------------------------------------------------
        var result=new this.STORE();
        for(var i in this.elements) {
            if (this.elements[i]!=elt){
                result.append(this.elements[i]);  
            }
        }
        this.elements=result;

    }
    //----------------------------------------------------
    by_class(cls){
    //----------------------------------------------------
        var result=new this.STORE();

        for(var i in this.elements) {
            if( this.elements[i] instanceof cls ){
                result.append(this.elements[i]); 
            }
        }

        return result;

    }
    //----------------------------------------------------
    by_attr(attr){
    //----------------------------------------------------
        var result=new this.STORE();

        for(var i in this.elements) {
            if( attr in this.elements[i] ){
                result.append(this.elements[i]); 
            }
        }

        return result;

    }
    //----------------------------------------------------
    by_attr_value(attr,value){
    //----------------------------------------------------
        var result=new this.STORE();

        for(var i in this.elements) {
            if( attr in this.elements[i] ){
                if( this.elements[i][attr]==value ){
                    result.append(this.elements[i]); 
                }
            }
        }

        return result;

    }

    //----------------------------------------------------
    execute(action,...args){
    //----------------------------------------------------

        var result=new this.STORE();
        var lst=this.by_attr(action).iter();

        for(var i in lst) {
            //console.log(action,lst[i].path(),args);
            var r=lst[i][action](...args);
            result.append([lst[i],r]); 

        }

        return result;

    }

    //----------------------------------------------------
    sorted(){
    //----------------------------------------------------
        var elements = this.iter();
        var sortedArray = [];
        var result = [];

        // Push each JSON Object entry in array by [key, value]
        for(var i in elements)
        {
            sortedArray.push([elements[i].name, elements[i]]);
        }

        // Run native sort function and returns sorted array.
        sortedArray=sortedArray.sort();

        for(var i in sortedArray)
        {
            result.push( sortedArray[i][1]);
        }
        return result;

    }
    //----------------------------------------------------

}
//========================================================



//========================================================

class Atom  extends Base{

//========================================================
/*
    crée une arborescence parent/enfants pour gerer les objets
    utiliser la classe par héritage pour creer différents objets
    imbricables
*/

    //----------------------------------------------------
    constructor(parent, name){
    //----------------------------------------------------

        super(name);
        //console.log("new",this.constructor.name,name);

        this.parent = null;
        this.children = new Store();
        this.inputs =  new LinkStore();
        this.outputs = new LinkStore();

        this.attach(parent);
    }
    //----------------------------------------------------
    destroy(){
    //----------------------------------------------------
        //console.log("destroy",this.path());

        this.inputs.destroy();
        this.outputs.destroy();
        this.children.destroy();

        this.detach();

    }
    //----------------------------------------------------
    attach(parent){
    //----------------------------------------------------
        this.detach();
        if( parent) {
            this.parent = parent;
            parent.children.append(this);
        }
    }
    //----------------------------------------------------
    detach(){
    //----------------------------------------------------
        if (this.parent){
            this.parent.children.remove(this);
            this.parent=null;
        }
    }


    //----------------------------------------------------
    root(){
    //----------------------------------------------------
    //Renvoie la racine de l’arbre

        if(this.parent){
            return this.parent.root();
        }else{
            return this;
        }
    }
    //----------------------------------------------------
    path(){
    //----------------------------------------------------

        if(this.parent){
            return this.parent.path()+"/"+this.name;
        }else{
            return "";
        }
        
    }
    //----------------------------------------------------
    query(path,action,...args){
    //----------------------------------------------------
       var node=this.search_path(path);
        return node[action](...args);
       
    }

    //----------------------------------------------------
    all(limit=null){
    //----------------------------------------------------
        var result=new Store();
        result.append(this);
        var lst= this.children.sorted();

        if( limit==null ){

            for(var i in lst) {
                result.append(lst[i].all()); 
            }
        }else if(limit>0 ){
            for(var i in lst) {
                result.append(lst[i].all(limit=limit-1)); 
            }
        }

        return result;

    }
    //----------------------------------------------------
    by_class(cls){
    //----------------------------------------------------

        return this.all().by_class(cls);
    }
    //----------------------------------------------------
    by_attr(...args){
    //----------------------------------------------------

        return this.all().by_attr(...args);
    }
 
    //----------------------------------------------------
    child_by_name(name){
    //----------------------------------------------------
        return this.children.by_attr_value("name",name).first();

    }
    //----------------------------------------------------
    search_path(path){
    //----------------------------------------------------
        if(path[0]=="/"){
            var result=this.root();
        }else{
            var result=this;
        }

        var lst=path.split("/");

        for(var i in lst) {
            if( lst[i] ==".."){
                    result=result.parent;
            }else if( lst[i] !="." && lst[i] !=""){
                var node=result.child_by_name(lst[i]);
                if(  node ){
                    result=node;
                }else{
                    console.log("no path",this.path(),path);
                    return;
                }
            }
        }

        return result;

    }
    //----------------------------------------------------
    create_path(path,cls){
    //----------------------------------------------------
        //console.log("create path",path);
        if(path[0]=="/"){
            var result=this.root();
        }else{
            var result=this;
        }

        var lst=path.split("/");

        for(var i in lst) {

            if(lst[i] !="" && lst[i] !="." ){
                result=result.get_child(lst[i],cls);
            }
        }
        //console.log("create path",result);
        return result;

    }
    //----------------------------------------------------
    get_child(name,cls,...args){
    //----------------------------------------------------
        //console.log("get_child",name);
        var node=this.child_by_name(name);
        if(  node ){
            var result=node;
        }else{
            var result=new cls(this,name,...args) ;
        }


        return result;

    }
   //----------------------------------------------------
    links(limit=null){
    //----------------------------------------------------
        var result=new LinkStore();
        result.append(this.outputs);

        if( limit==null ){
            var lst= this.children.iter();
            for(var i in lst) {
                result.append(lst[i].links()); 
            }
        }else if(limit>0 ){
            var lst= this.children.iter();
            for(var i in this.children) {
                result.append(this.children[i].links(limit=limit-1)); 
            }
        }

        return result;

    }
    //----------------------------------------------------
    links_by_tag(tag,...args){
    //----------------------------------------------------
        var result=new LinkStore();
        var links=this.all_links(...args);

        for(var i in links) {
            if( links[i].tag==tag ){
                result.append(links[i]); 
            }
        }

        return result;

    }


    //----------------------------------------------------
    objects(){
    //----------------------------------------------------

        return this.outputs.objects();
    }
    //----------------------------------------------------
    subjects(tag=null){
    //----------------------------------------------------
        return this.inputs.subjects();
    }
    //----------------------------------------------------
    link(list,tag=null,cls=Link){
    //----------------------------------------------------

        for(var obj in list) {
            new cls(this,obj,tag=tag);
        }
    }
    //----------------------------------------------------
    print(){
    //----------------------------------------------------
        var nodes=this.all().elements;
        for(var i in nodes ) {
            console.log(nodes[i].path());
        }
    }
    //----------------------------------------------------
}
//========================================================

//========================================================

class Link extends Base  {

//========================================================
    //----------------------------------------------------
    constructor(subject, object, tag=null){
    //----------------------------------------------------
        super(name);
            //console.log(subject,object);
            this.subject = subject;
            this.object = object;
            this.tag = tag;
            this.subject.outputs.append(this);
            this.object.inputs.append(this);
    }
    //----------------------------------------------------
    destroy(){
    //----------------------------------------------------

        this.subject.outputs.remove(this);
        this.object.inputs.remove(this);
        super.destroy();
    }
    //----------------------------------------------------
}
//========================================================

        
//========================================================

class Query {

//========================================================
    /*
    object to handle query
    with an optional output format option.

    list of formats :
    * json
    * xml
    * obj -> creer les objets js
    
    basic usage returns text:
        var q=new Query(url,callback);

    usage:
        var q=new Query(url,callback,format);

    cyclic behavior, dt in millis:
        var q=new Query(url,callback,format,dt);

    callback function :
        function callback(data){
            ...
        }
    */

    //----------------------------------------------------
    constructor(obj,url,callback,format=null,dt=null){
    //----------------------------------------------------


        //attributes
        this.obj=obj;
        this.component_cb=callback;
        this.url=url;
        this.dt=dt;

        //request
        this.request = new XMLHttpRequest();
        this.data;
        this.interval;
        this.format=format;

        //query
        if ( dt ) {
            this.cycle_start(dt);

        }else{
            this.query();
        }

    }

    //----------------------------------------------------

    cycle_start(dt) {

    //----------------------------------------------------

          this.interval = setInterval(this.query.bind(this), this.dt);

    }
    //----------------------------------------------------

    cycle_stop() {

    //----------------------------------------------------

        clearInterval(this.interval);

    }
    //----------------------------------------------------

    query() {

    //----------------------------------------------------

          this.request.open('GET', this.url, true);
          this.request.onreadystatechange = this.request_cb.bind(this);
          this.request.send(null);

    }
    //----------------------------------------------------

    request_cb(e) {

    //----------------------------------------------------

        if (this.request.readyState === 4) {
            if (this.request.status === 200) {

                this.data = this.request.responseText;
                //console.log(this.data);

                if(this.format=="json"){
                    this.data=JSON.parse (this.data);
                }
                if(this.format=="xml"){

                      var parser = new DOMParser();

                      var xmlDoc = parser.parseFromString(this.data,"text/xml");
                      this.data=xmlDoc.documentElement;
                }
                if(this.format=="obj"){

                      var parser = new DOMParser();

                      var xmlDoc = parser.parseFromString(this.data,"text/xml");
                      this.data=from_xml(null,xmlDoc.documentElement);
                }

                console.log("loading",this.url,"done");
                this.component_cb.bind(this.obj)(this.data);
                return

            }
        console.warn("error",this.url);
        }

    }

    //----------------------------------------------------


}
//========================================================




//========================================================

class LinkStore extends Store  {

//========================================================

    //----------------------------------------------------
    constructor(...args){
    //----------------------------------------------------

        super(...args);
        this.STORE=LinkStore;
    }
    //----------------------------------------------------
    objects(){
    //----------------------------------------------------
        var result=new Store();

        for(var i in this.elements) {
            result.append(this.elements[i].object);
        }
        return result;
    }
    //----------------------------------------------------
    subjects(){
    //----------------------------------------------------
        var result=new Store();

        for(var i in this.elements) {

                result.append(link.subject);
        }
        return result;
    }
    //----------------------------------------------------
    objects_by_tag(tag){
    //----------------------------------------------------
        var result=new Store();

        for(var i in this.elements) {
            if(this.elements[i].tag==tag){
                result.append(this.elements[i].object);
            }
        }
        return result;
    }
    //----------------------------------------------------
    subjects_by_tag(tag){
    //----------------------------------------------------
        var result=new Store();

        for(var i in this.elements) {
            if(this.elements[i].tag==tag){
                result.append(link.subject);
            }
        }
        return result;
    }
    //----------------------------------------------------
}
//========================================================

//========================================================

class Message extends Atom {

//========================================================
    //----------------------------------------------------
    constructor(parent,tag,data){
    //----------------------------------------------------
        super(parent,null);
        data["t"]=CLOCK.getTime();
        this.data=data;
        this.tag=tag;
    }
    //----------------------------------------------------
}
//========================================================

class Bd_Object extends Atom {

//========================================================
    // modules have lifecycle
    //  elt.setup()
    /* 
    objets serialisables
    */
    //----------------------------------------------------
    constructor(parent,data){
    //----------------------------------------------------
        if(data["name"]){
            super(parent,data["name"]);
        } else {
            super(parent,null);
        }
        this.__data=data;
        this.__setup_done=false;

    }
    //----------------------------------------------------
    add_variable(name,def=null){
    //----------------------------------------------------

        this[name]=def;
        if(this.__data[name] ) {
            this[name]=this.__data[name];
        }
    }
    //----------------------------------------------------
    setup(){
    //----------------------------------------------------

        if(this.__setup_done==false){

            this.msg("SETUP",{},2);
            this.onSetup(this.__data);
            this.ls_objects().execute("setup");
            this.__setup_done=true;
            this.onSetupDone();
        }
    }
    //----------------------------------------------------
    onSetup(data){
    //----------------------------------------------------

    }
    //----------------------------------------------------
    onSetupDone(){
    //----------------------------------------------------


    }
    //----------------------------------------------------
    waitSetup(){
    //----------------------------------------------------


        if(this.isLoaded()==true){
            this.msg("SETUP_CHILDREN",{},2);
            this.objects().execute("setup");
            //this.onSetupDone();
            this.__setup_done=true;
            this.msg("SETUP_DONE",{},2);
        }else{
            this.msg("SETUP_WAIT",{},1);
            setTimeout(this.waitSetup.bind(this),DT);
        }
    }
    //----------------------------------------------------
    isLoaded(){
    //----------------------------------------------------

        return true;

    }
    //----------------------------------------------------
    msgXX(tag,data){
    //----------------------------------------------------
        if ( DEBUG ) {
            //new Message(this,tag,data);
            console.log(tag,this.path(),data);
        }

    }
    //----------------------------------------------------
    msgs(){
    //----------------------------------------------------
        return this.children.by_class(Message);
    }
    //----------------------------------------------------
    ls_objects(){
    //----------------------------------------------------
        return this.children.by_class(Bd_Object);
    }
    //----------------------------------------------------

}
//========================================================

//========================================================

class Bd_Data extends Bd_Object {

//========================================================


    //----------------------------------------------------
    onSetup(data){
    //----------------------------------------------------
        super.onSetup(data);
        this.add_variable("url");
        this.add_variable("text");
        this.add_variable("type");
    }
    //----------------------------------------------------
 
}
//========================================================
//========================================================

class Bd_Action extends Bd_Object {

//========================================================


    //----------------------------------------------------
    make(data){
    //----------------------------------------------------

        this.msg("MAKE",data,5);
        return this.onMake(data);
        this.msg("MAKE_DONE",data,5);

    }
    //----------------------------------------------------
    onMake(data){
    //----------------------------------------------------
        return;

    }
    //----------------------------------------------------
    executeEvent(event,callback,data,r=false){
    //----------------------------------------------------
        this.msg("EVENT",{'event':event},4);
        var node = new Event_Handler(this,event,callback.bind(this),data,r);
        node.start();
        return node;
    }
    //----------------------------------------------------
    LoadFile(url,callback,format=null,dt=null,data=null){
    //----------------------------------------------------
        this.msg("EVENT",{'url':url},4);
        var node = new LoadFile(this,callback.bind(this),url,format,dt,data=data);
        node.start();
        return node;
    }
    //----------------------------------------------------
}
//========================================================

//========================================================

class Bd_Main extends Bd_Action {

//========================================================
    //----------------------------------------------------
    constructor(tag,command,content){
    //----------------------------------------------------

        super(null, {});

        this.tag=tag;
        this.content=content;
        this.command=command;
        this.baseurl=content.substring(0, content.lastIndexOf("/"));
        // site map
        new Query(this,content,this.onLoadContent.bind(this),"obj");
        new Query(this,command,this.onLoadCommand.bind(this),"obj");

        this.i=0;

    }

    onLoadContent(data){data.name="data";this.content=data;data.attach(this);this.onLoadFile(data);}
    onLoadCommand(data){data.name="__main__";this.command=data;data.attach(this);this.onLoadFile(data);}
    //----------------------------------------------------

    onLoadFile(data){

    //----------------------------------------------------
        this.i+=1;
        //data.attach(this);
        if(this.i==2){this.setup();}

    }
    //----------------------------------------------------

    onSetup(){

    //----------------------------------------------------

        this.executeEvent("setup",this.onSetupEventDone,{},true);
    }
    //----------------------------------------------------

    onSetupEventDone(){

    //----------------------------------------------------
        this.children.execute("setup");

    }
    //----------------------------------------------------

    run(){

    //----------------------------------------------------
        var node=this.search_path("__main__");
        if(node !=null){
            //console.log("make");
            node.make();
        }
    }
    //----------------------------------------------------

}
//========================================================

class Run extends Bd_Action {

//========================================================
    //----------------------------------------------------
    constructor(url,parent=null,callback=null){
    //----------------------------------------------------

        super(null, {});
        this.url=url;
        this.node=null;
        this.node_parent=parent;
        this.callback=this.run;
     
        // site map
        new Query(this,url,this.onLoadCommand.bind(this),"obj");
    }

    //----------------------------------------------------
    onLoadCommand(node){
    //----------------------------------------------------
        //data.name="__main__";
        this.node=node;
        //this.node.attach(this.node_parent);
        //data.attach(this);
        this.node.setup();
        //this.node.make();

        if(this.callback){
            this.callback(node);
        }
    }
    //----------------------------------------------------

    onSetup(){

    //----------------------------------------------------

        this.executeEvent("setup",this.onSetupEventDone,{},true);
    }
    //----------------------------------------------------

    onSetupEventDone(){

    //----------------------------------------------------

        this.run();  

    }
    //----------------------------------------------------

    run(){

    //----------------------------------------------------
        this.children.execute("setup");
        var node=this.node.search_path("__main__");
        if(node !=null){
            console.log("make");
            node.make();
        }else{
            console.log("fail");   
        }
    }
    //----------------------------------------------------


}
//========================================================




//========================================================

class Task extends Atom {

//========================================================
/*
gère l'execution des évènements de la classe parent
démarre l'execution avec la fonction start
appelle la fonction callback quand c'est fini'
*/
    //----------------------------------------------------
    constructor(parent,callback,args){
    //----------------------------------------------------

        super(parent,null);
        if(args==null){args={};}
        this.msg("EVENT_NEW",args,3);
        this.time_start=CLOCK.getTime();

        this.callback=callback;//fonction à appeler quand fini
        this.args=args;//arguments pour callback
        this.is_done=false;
        this.has_success=true;
    }

    //----------------------------------------------------
    start(){
    //----------------------------------------------------
        this.msg("EVENT_START",this.args,3);
        this.onStart();
        this.waitEnded();
    }
    //----------------------------------------------------
    onStart(){
    //----------------------------------------------------
            this.done();
    }
    //----------------------------------------------------
    done(){
    //----------------------------------------------------
        this.is_done= true;

    }
    //----------------------------------------------------
    is_finish(){
    //----------------------------------------------------
        return this.is_done;

    }

    //----------------------------------------------------
    waitEnded(){
    //----------------------------------------------------

        if(this.is_finish()==true){
            this.msg("EVENT_DONE",this.args,3);
            if(this.has_success==true){
                this.onProcessResult();
                this.callback(this.args);
            }
        }else{
             setTimeout(this.waitEnded.bind(this),DT);
        this.msg("EVENT_WAIT",{},2);
        }
    }
    //----------------------------------------------------
    onProcessResult(){
    //----------------------------------------------------

    }
    //----------------------------------------------------

}
//========================================================

        
//========================================================

class LoadFile extends Task {

//========================================================
    /*
    object to handle query
    with an optional output format option.

    list of formats :
    * json
    * xml
    * obj -> creer les objets js
    
    basic usage returns text:
        var q=new Query(url,callback);

    usage:
        var q=new Query(url,callback,format);

    cyclic behavior, dt in millis:
        var q=new Query(url,callback,format,dt);

    callback function :
        function callback(data){
            ...
        }
    */

    //----------------------------------------------------
    constructor(parent,callback,url,format=null,dt=null,data=null){
    //----------------------------------------------------

        super(parent,callback,data);

        //attributes
        this.url=url;
        this.dt=dt;
        this.format=format;

        //request
        this.request = new XMLHttpRequest();
        this.response;
        this.interval;
    }

    //----------------------------------------------------
    onStart(){
    //----------------------------------------------------


        //query
        if ( this.dt !=null) {
            this.cycle_start();

        }else{
            this.query();
        }

    }

    //----------------------------------------------------

    cycle_start() {

    //----------------------------------------------------

          this.interval = setInterval(this.query.bind(this), this.dt);

    }
    //----------------------------------------------------

    cycle_stop() {

    //----------------------------------------------------

        clearInterval(this.interval);

    }
    //----------------------------------------------------

    query() {

    //----------------------------------------------------

          this.request.open('GET', this.url, true);
          this.request.onreadystatechange = this.request_cb.bind(this);
          this.request.send(null);

    }
    //----------------------------------------------------

    request_cb(e) {

    //----------------------------------------------------

        if (this.request.readyState === 4) {
            if (this.request.status === 200) {

                this.response = this.request.responseText;
                //console.log(this.data);

                this.msg("LOAD",{'msg':this.url},4);
                this.done();
                return;

            }
            this.msg("ERROR",{'msg':this.url});
            this.has_success=false;
            this.done();
            return;
        }

    }
    //----------------------------------------------------
    onProcessResult(){
    //----------------------------------------------------
        this.msg("LOADED",{'msg':this.url}),7;
        if(this.format=="json"){
            this.response=JSON.parse (this.response);
        }
        else if(this.format=="xml"){

              var parser = new DOMParser();

              var xmlDoc = parser.parseFromString(this.response,"text/xml");
              this.response=xmlDoc.documentElement;
        }
        else if(this.format=="obj"){

              var parser = new DOMParser();

              var xmlDoc = parser.parseFromString(this.response,"text/xml");
              this.response=from_xml(null,xmlDoc.documentElement);
        }else {
            this.msg("ERROR_FORMAT",{'msg':this.url});
        }
        this.args["response"]=this.response;
    }
    //----------------------------------------------------


}
//========================================================





//========================================================

class Do extends Bd_Action {

//========================================================
    //----------------------------------------------------
    onSetup(data){
    //----------------------------------------------------

        super.onSetup(data);
        this.finish=false;
        this.add_variable("event",null);

    }

    //----------------------------------------------------

    onMake(data){

    //----------------------------------------------------
        this.done();

    }
    //----------------------------------------------------
    done(){
    //----------------------------------------------------

        this.finish=true;

    }
    //----------------------------------------------------
    reset(){
    //----------------------------------------------------
        this.finish=false;

    }
    //----------------------------------------------------
}
//========================================================



//========================================================

class ScriptNode extends Bd_Action {

//========================================================
    //----------------------------------------------------

    onSetupDone(){

    //----------------------------------------------------

        this.executeEvent("setup",this.onSetupEventDone,{},true);
    }
    //----------------------------------------------------

    onSetupEventDone(){

    //----------------------------------------------------
        this.setup();
        //this.make();

    }
    //----------------------------------------------------
    isLoaded(){
    //----------------------------------------------------

        return true;

    }
    //----------------------------------------------------
    onMakeX(data){
    //----------------------------------------------------

            this.msg("MAKE",{'node':this.children},4);
            this.onMakeFirst(data.selected);
            this_make_event=this.executeEvent("call",this.onMakeDone,data);


    }
    //----------------------------------------------------
    onMakeFirst(selected){
    //----------------------------------------------------
            this.msg("MAKE FIRST",{},4);

    }
    //----------------------------------------------------
    onMakeDone(data){
    //----------------------------------------------------
            this.msg("MAKE DONE",{});

    }
    //----------------------------------------------------
    call_part(name){
    //----------------------------------------------------

    return this.parent.call_part(name);

    }
    //----------------------------------------------------

}
//========================================================

//========================================================

class Script extends ScriptNode {

//========================================================
    //----------------------------------------------------
    onSetup(data){
    //----------------------------------------------------
        this.msg("SCRIPT_SETUP",{});
        this.add_variable("exec",data,"__main__");
        this.add_variable("select",null);
        this.add_variable("tag",null);
    }
    //----------------------------------------------------

    onSetupDone(){

    //----------------------------------------------------

        this.executeEvent("setup",this.onSetupEventDone,{},true);
    }
    //----------------------------------------------------

    onSetupEventDone(){

    //----------------------------------------------------
        this.setup();
        //this.make();

    }
    //----------------------------------------------------
    onMake(){
    //----------------------------------------------------

        var node=this.search_path("__main__");
        console.log(node);
        if(node !=null){
            this.msg("SCRIPT_MAKE",{'node':node.path()});
            node.make()

        /*
            //node.make();
            var selected=null;

            if(this.select!=null){
                selected=this.search_path(this.select);
            }else{
                selected=this.root().content;
            }
            var div=$(this.tag);
            console.log(node,div,selected);
            node.draw(div,selected);*/
        }else{
            console.log(this.path(),"NO MAIN TO START");
        }


    }
    //----------------------------------------------------
    onMakeCall(selected){
    //----------------------------------------------------
        var node=this.search_path("__main__");
        //node.executeEvent("call",this.onMakeDone.bind(this));
        node.make(selected);
    }
    //----------------------------------------------------
    onMakeDone(data){
    //----------------------------------------------------
        this.msg("SCRIPT_DONE",data);
        this.executeEvent("cleanup",this.onCleanupDone,{},true);
    }
    //----------------------------------------------------
    onCleanupDone(){
    //----------------------------------------------------

    }
    //----------------------------------------------------
    call_part(name){
    //----------------------------------------------------

    return this.search_path(name).make();

    }
    //----------------------------------------------------

}
//========================================================



//========================================================

class Print extends Do {

//========================================================
    //----------------------------------------------------
    onSetup(data){
    //----------------------------------------------------
        super.onSetup(data);
        this.add_variable("text","..");
    }
    //----------------------------------------------------
    onMake(data){
    //----------------------------------------------------
        console.log("<<<<<<<<<<< "+this.path()+"   >>>>>>>>"+this.text);
        this.done();
    }
    //----------------------------------------------------
}
//========================================================

class Call extends Do {

//========================================================
    //----------------------------------------------------
    onSetup(data){
    //----------------------------------------------------
        super.onSetup(data);
        this.add_variable("select","..");
        this.add_variable("event","call");
        this.add_variable("r",false);
    }
    //----------------------------------------------------
    onMake(data){
    //----------------------------------------------------

        var node=this.search_path(this.select);
        node.executeEvent(this.event,this.onCallDone.bind(this));
    }
    //----------------------------------------------------
    onCallDone(obj,event){
    //----------------------------------------------------

        this.done();

    }
    //----------------------------------------------------
}
//========================================================

class Load_Xml extends Do {

//========================================================
    //----------------------------------------------------
    onSetup(data){
    //----------------------------------------------------
        super.onSetup(data);
        this.add_variable("url");
        this.add_variable("node","..");
        this.add_variable("nodename",null);
        this.add_variable("format","obj");
    }
    //----------------------------------------------------
    onMake(){
    //----------------------------------------------------
        this.LoadFile(this.get_url(),this.onFileLoaded,this.format);

    }
    //----------------------------------------------------
    onFileLoaded(data){
    //----------------------------------------------------
        var node=data.response;
        node.attach(this.search_path(this.node));
        if(this.nodename!=null){
            node.name=this.nodename;
        }
        node.setup();
        if ('executeEvent' in node){
            node.executeEvent("setup",this.onSetupDone,true);
        }
        this.done();

    }
    //----------------------------------------------------

    get_url(){

    //----------------------------------------------------

        if(this.url.startsWith('/') || this.url.startsWith('.')){
            return this.url;
        }else{
            return this.root().url+"/"+this.url;
        }
    }
    //----------------------------------------------------

}
//========================================================




//========================================================

class Event_Handler extends Task {

//========================================================
/*
gère l'execution des évènements de la classe parent
démarre l'execution avec la fonction start
appelle la fonction callback quand c'est fini'
*/
    //----------------------------------------------------
    constructor(parent,event,callback,args,r=false){
    //----------------------------------------------------

        super(parent,callback,args);

        this.event=event;
        this.r=r;//recursif

        if( r==true ){
            this.actions= parent.all().by_class(Do).by_attr_value("event",event);

        }else{
            this.actions= parent.children.by_class(Do).by_attr_value("event",event);
        }
        //console.log(this.actions,this.actions.size());
        //parent.tree();
    }
    //----------------------------------------------------
    onStart(){
    //----------------------------------------------------

        this.actions.execute("reset");
        this.actions.execute("make",this.args);

    }
    //----------------------------------------------------
    is_finish(){
    //----------------------------------------------------
        //console.log(this.actions.by_attr_value("finish",false).size());
        if( this.actions.by_attr_value("finish",false).size()>0){
            this.done();
            return false;
        }else{
            return true;
        }
    }
    //----------------------------------------------------

}
//========================================================
//========================================================

class SceneNode extends ScriptNode {

//========================================================

    //----------------------------------------------------
    onSetup(data){
    //----------------------------------------------------

        super.onSetup(data);
        this.url=null;
    }
    //----------------------------------------------------

    drawScene(name,div,selected){

    //----------------------------------------------------
    
    return this.parent.drawScene(name,div,selected);

    }
    //----------------------------------------------------

    draw(div,selected){

    //----------------------------------------------------
        //this.msg("DRAW",{div:div,selected:selected},5);

        div=this.onDraw(div,selected);
        this.children.by_class(SceneElement).execute("draw",div,selected);
        this.onDrawDone(div,selected);
        //this.msg("DRAW_DONE",{div:div,selected:selected},5);
        return div;
    }
    //----------------------------------------------------

    onDraw(div,selected){

    //----------------------------------------------------
        //renvoie la div pour afficher les SceneElement enfants
        return div;

    }
    //----------------------------------------------------
    onDrawDone(div,selected){
    //----------------------------------------------------
        //renvoie la div pour afficher les SceneElement enfants
        return div;

    }
    //----------------------------------------------------

    get_html(name,args,tag=null,parent=null,text=null){

    //----------------------------------------------------
        var local_arguments=null;

        if(args!=null){
            local_arguments=args;
        }else{
            local_arguments={};
        }

        if(tag){
            local_arguments["id"]=tag;
        }

        var self=jQuery('<'+name+'/>',local_arguments);
        if(text){self.append(text);}

        if(parent){
            self.appendTo(parent);
        }

        return self;
    }
    //----------------------------------------------------

    get_url(selected){

    //----------------------------------------------------
        var url=null;

        if(this.url){
            url= this.url;
        }else{
            url= selected.url;
        }
        console.log("okok",url,selected);
        if(url.startsWith('http')){
            //url= url;

        } else if(url.startsWith('/')|| url.startsWith('.')){
            //url= url;
        }else{
            //url= this.root().url+url;
        }


        return url;
    }
    //----------------------------------------------------

}
//========================================================
//========================================================

class SceneElement extends SceneNode {

//========================================================

}
//========================================================

//========================================================

class SceneGroup extends SceneNode {

//========================================================
    //----------------------------------------------------
    onSetup(data){
    //----------------------------------------------------

        super.onSetup(data);

        this.div=null;
        this.add_variable("part","body");
        this.add_variable("select",null);
    }
    //----------------------------------------------------
     onMake(data){
    //----------------------------------------------------
        var selected=null;

        if(this.select!=null){
            selected=this.search_path(this.select);
        }else{
            selected=this.root().content;
        }

        if(selected==null){
            console.log("NO NODE SELECTED",this.path());
            return;
        }
        //console.log("SELECTED",selected.path());
        this.div=$(this.root().tag);
        console.log(this.root().tag,$(this.root().tag));
        var div=null;
        if(this.div!=null){
           div=this.div;
        }else{
            div=$(this.part)[0];
        }
        this.drawScene("__main__",div,selected);
        //return div;
    }
    //----------------------------------------------------
    drawScene(name,div,selected){
    //----------------------------------------------------

        var node=this.search_path(name);
        if(node){
            console.log("draw",name,selected,div);
            return node.draw(div,selected);
        }else{
            console.log("draw not found",name,selected,div);
        }

    }
    //----------------------------------------------------

}
//========================================================



//========================================================

class Scene extends SceneNode {

//========================================================

    //----------------------------------------------------
    onDraw(div,selected){

    //----------------------------------------------------
        return div;
    }
    //----------------------------------------------------
    drawScene(name,div,selected){
    //----------------------------------------------------

        var node=this.parent.search_path(name);
        if(node){
            return node.draw(div,selected);
        }else{
            console.log("draw not found",name,selected,div);
        }

    }
    //----------------------------------------------------

}

//========================================================
//========================================================

class Call_Scene extends SceneElement {

//========================================================
    //----------------------------------------------------
    onSetup(data){
    //----------------------------------------------------
        this.add_variable("part",null);
        this.add_variable("scene");
        this.add_variable("select");
    }

    //----------------------------------------------------

    onDraw(div,selected){

    //----------------------------------------------------
        var parent;
        if(this.part){
            var parent=$("#"+this.part);
        }else{
            var parent=div;
        }
        if(this.select){
            var node=selected.search_path(this.select);
            if (node==null){console.log("error path",this.select);}
        }else{
            var node=selected;
        }
        $("#"+this.part).empty();
        //console.log("Call_Scene",this.scene,selected.name,parent);
        this.drawScene(this.scene,parent,node);

    }
    //----------------------------------------------------
}
//========================================================

//========================================================

class CallFromData extends SceneElement {

//========================================================
    // appeler une scene stockée dans Bd_Data.url
    //----------------------------------------------------
    onSetup(data){
    //----------------------------------------------------
        this.add_variable("part",null);
        this.add_variable("select");
    }

    //----------------------------------------------------

    onDraw(div,selected){

    //----------------------------------------------------
        var parent;
        if(this.part){
            var parent=$("#"+this.part);
        }else{
            var parent=div;
        }
        if(this.select){
            var node=this.parent.search_path(this.select);
        }else{
            var node=selected;
        }

        parent.empty();
        //console.log("Call_Scene",this.scene,selected.name,parent);
        this.drawScene(selected.url,parent,node);

    }
    //----------------------------------------------------
}
//========================================================

//========================================================

class ForChildren extends SceneElement {

//========================================================
    //----------------------------------------------------
    onSetup(data){
    //----------------------------------------------------

        super.onSetup(data);
        this.add_variable("attr",null);
        this.add_variable("value",null);
        this.add_variable("cls",null);
    }
    //----------------------------------------------------
    draw(div,selected){

    //----------------------------------------------------
        var lst=[];

      //console.log(this.path(),"################################",selected);
      if(this.cls!=null){
            var cls=eval(this.cls);

            lst=selected.children.by_class(cls);
        }else{
            lst=selected.children;
        }
      console.log(this.path(),"################################",lst.size());
        if(this.attr!=null){
            lst=lst.by_attr_value(this.attr,this.value);
        }
        lst=lst.sorted();
        for(var i in lst) {

            super.draw(div,lst[i]);
        }
        return div;
    }
    //----------------------------------------------------

}
//========================================================


//========================================================

class SelectFirst extends SceneElement {

//========================================================
    //----------------------------------------------------
    onSetup(data){
    //----------------------------------------------------

        super.onSetup(data);
        this.add_variable("attr",null);
        this.add_variable("value",null);
    }
    //----------------------------------------------------
    draw(div,selected){

    //----------------------------------------------------
        var node;
        if(this.attr!=null){
            node=selected.children.by_attr_value(this.attr,this.value).first();
        }else{
            node=selected.first();

        }

        if(node){
            super.draw(div,node);
        }
    }
    //----------------------------------------------------

}
//========================================================
//========================================================

class SelectPath extends SceneElement {

//========================================================
    //----------------------------------------------------
    onSetup(data){
    //----------------------------------------------------

        super.onSetup(data);
        this.add_variable("value");
    }
    //----------------------------------------------------
    draw(div,selected){

    //----------------------------------------------------
        var node=selected.search_path(this.value);

        if(node!=null){
            return super.draw(div,node);
        }
    }
    //----------------------------------------------------

}

//========================================================

//========================================================

class SelectXml extends SceneElement {

//========================================================
    //----------------------------------------------------
    onSetup(data){
    //----------------------------------------------------

        super.onSetup(data);
        this.add_variable("target",null);
    }
    //----------------------------------------------------
    draw(div,selected){
    //----------------------------------------------------
        var node=null;

        if(this.target!=null){
            node=selected.search_path(this.target);
        }else{
            node=selected;
        }
        if(node!=null){
            this.LoadFile(this.get_url(node),this.onFileLoaded.bind(this),"obj",null,{div:div,selected:node});
        }
    }
    //----------------------------------------------------
    onFileLoaded(data){
    //----------------------------------------------------
        //console.log(data);
        //data.response.attach(data.args.node);
        data.response.setup();
        super.draw(data.div,data.response);

    }
    //----------------------------------------------------

}
//========================================================

//========================================================

class SelectXmlScript extends SceneElement {

//========================================================
    //----------------------------------------------------
    draw(div,selected){
    //----------------------------------------------------


        this.LoadFile(this.get_url(selected),this.onFileLoaded.bind(this),"obj",null,{div:div,selected:selected});
    }
    //----------------------------------------------------
    onFileLoaded(data){
    //----------------------------------------------------
        console.log("onFileLoaded",data,data.selected)
        //data.response.attach(this.root());
        data.response.setup();
        data.response.drawScene("__main__",data.div,data.selected);
        data.response.destroy()
    }
    //----------------------------------------------------

}
//========================================================
//========================================================

class TestAttr extends SceneElement {

//========================================================
    //----------------------------------------------------
    onSetup(data){
    //----------------------------------------------------

        super.onSetup(data);
        this.add_variable("attr");
        this.add_variable("value");
    }
    //----------------------------------------------------
    draw(div,selected){

    //----------------------------------------------------

        if(selected.__data[this.attr]==this.value){
            super.draw(div,selected);
        }
    }
    //----------------------------------------------------

}
//========================================================

class TestClass extends SceneElement {

//========================================================
    //----------------------------------------------------
    onSetup(data){
    //----------------------------------------------------

        super.onSetup(data);
        this.add_variable("cls");
    }
    //----------------------------------------------------
    draw(div,selected){

    //----------------------------------------------------

        var cls=eval(this.cls);
        if(selected instanceof cls){
            super.draw(div,selected);
        }
    }
    //----------------------------------------------------

}
//========================================================


        
//========================================================

class Sequence extends SceneElement {

//========================================================

    //----------------------------------------------------
    onSetup(){
    //----------------------------------------------------

        super.onSetup();
        this.add_variable("dt",1.0);
        this.add_variable("empty",false);

    }

    //----------------------------------------------------
    draw(div,selected){

    //----------------------------------------------------
        this.msg("SEQ_START",{},5);
        this.OnDrawNext(div,selected,0) ;
  
    }
    //----------------------------------------------------

    cycle_stop() {

    //----------------------------------------------------

        clearInterval(this.interval);

    }
    //----------------------------------------------------

    OnDrawNext(div,selected,counter) {

    //----------------------------------------------------

        if(this.interval){
            this.cycle_stop();
        }

        if(this.empty){
            div.empty();
        }


        var lst=this.children.by_class(SceneElement).sorted();


        if(counter<lst.length){
            this.msg("SEQ_DRAW",counter,6);
            var node=lst[counter]
            node.draw(div,selected);
            counter+=1;

            this.interval = setInterval(this.OnDrawNext.bind(this), this.dt*1000,div,selected,counter);
        }else{
            this.msg("SEQ_STOP",counter,6);
        }
    }
    //----------------------------------------------------


}
//========================================================




//========================================================

class SelectLocal extends SceneElement {

//========================================================
    //----------------------------------------------------
    onSetup(data){
    //----------------------------------------------------

        super.onSetup(data);
        this.add_variable("value");
    }
    //----------------------------------------------------
    draw(div,selected){

    //----------------------------------------------------
        var node=this.search_path(this.value);

        if(node!=null){
            super.draw(div,node);
        }
    }
    //----------------------------------------------------

}

//========================================================


//========================================================

class HtmlHandler extends Do {

//========================================================
    //----------------------------------------------------
    onSetup(data){
    //----------------------------------------------------
        super.onSetup(data);
        this.add_variable("part");
    }

    //----------------------------------------------------

    onMake(data){

    //----------------------------------------------------

        this.msg("HtmlHandler",data,6);
        var parent=null;
        var selected=null;

        if(this.part!=null){
            parent=$("#"+this.part);
            selected=data.selected;
        }else{
            parent=data.div;
            selected=data.selected;
        }

        this.onDraw(parent,selected);

    }

    //----------------------------------------------------
    onDraw(div,selected){

    }
    //----------------------------------------------------
}
//========================================================


//========================================================

class Draw extends HtmlHandler {

//========================================================
    //----------------------------------------------------
    onSetup(data){
    //----------------------------------------------------
        super.onSetup(data);
        this.add_variable("scene");

    }

    //----------------------------------------------------

    onDraw(div,selected){

    //----------------------------------------------------
        this.msg("Draw",{div:div,selected:selected},6);
        div.empty();
        console.log("draw",this.scene,selected,div);
        this.parent.drawScene(this.scene,div,selected);

    }
    //----------------------------------------------------
}
//========================================================

//========================================================

class HtmlAnimate extends HtmlHandler {

//========================================================
    //----------------------------------------------------
    onSetup(data){
    //----------------------------------------------------

        super.onSetup(data);
        this.add_variable("key");
        this.add_variable("value");
        this.add_variable("t","slow");
    }
    //----------------------------------------------------

    onDraw(div,selected){

    //----------------------------------------------------

        var data={};
        data[this.key]=this.value;
        div.animate(data,this.t);

    }
    //----------------------------------------------------
}
//========================================================


//========================================================

class HtmlClass extends HtmlHandler {

//========================================================
    //----------------------------------------------------
    onSetup(data){
    //----------------------------------------------------

        super.onSetup(data);
        this.add_variable("value");
        this.add_variable("remove",false);
        this.add_variable("toggle",false);
    }
    //----------------------------------------------------

    onDraw(div,selected){

    //----------------------------------------------------

        if(this.remove){
            div.removeClass(this.value);

        }else if(this.toggle){
            div.toggleClass(this.value);
        }else{    
            div.addClass(this.value);
        }
    }
    //----------------------------------------------------
}
//========================================================

//========================================================

class HtmlCss extends HtmlHandler {

//========================================================
    //----------------------------------------------------
    onSetup(data){
    //----------------------------------------------------

        super.onSetup(data);
        this.add_variable("key");
        this.add_variable("value");

    }
    //----------------------------------------------------

    onDraw(div,selected){

    //----------------------------------------------------

        div.css(this.key,this.value);

    }
    //----------------------------------------------------
}
//========================================================

//========================================================

class HtmlEffect extends HtmlHandler {

//========================================================
    //----------------------------------------------------
    onSetup(data){
    //----------------------------------------------------

        super.onSetup(data);
        this.add_variable("action","fadeIn");
        this.add_variable("t","slow");
    }
    //----------------------------------------------------

    onDraw(div,selected){

    //----------------------------------------------------

        div[this.action](this.t);

    }
    //----------------------------------------------------
}
//========================================================
//========================================================

class HtmlMethod extends HtmlHandler {

//========================================================
    //----------------------------------------------------
    onSetup(data){
    //----------------------------------------------------

        super.onSetup(data);
        this.add_variable("action","show");

    }
    //----------------------------------------------------

    onDraw(div,selected){

    //----------------------------------------------------

        div[this.action]();

    }
    //----------------------------------------------------
}
//========================================================


//========================================================

class SelectPart extends SceneElement {

//========================================================
    //----------------------------------------------------
    onSetup(data){
    //----------------------------------------------------

        this.add_variable("part");

    }
    //----------------------------------------------------
    draw(div,selected){

    //----------------------------------------------------
        div=$('#'+this.part);
        div.empty();
        
        super.draw(div,selected.search_path(this.value));

    }
    //----------------------------------------------------
}

//========================================================


//========================================================

class ExecHtml extends SceneElement {

//========================================================
    //----------------------------------------------------
    onSetup(data){
    //----------------------------------------------------

        this.add_variable("part",null);

    }
    //----------------------------------------------------
    onDraw(div,selected){

    //----------------------------------------------------
        this.msg("ExecHtml",{div:div,selected:selected},1);
        if(this.part!=null){div=$(this.part); }     
        this.all().by_class(HtmlHandler).execute("make",{div:div,selected:selected});
        return div;
    }
    //----------------------------------------------------
}

//========================================================


//========================================================

class Div extends SceneElement {

//========================================================

    //----------------------------------------------------
    onSetup(){
    //----------------------------------------------------
        this.html=null;
        this.add_variable("active",false);
        this.add_variable("balise","div");
        this.add_variable("recursive_event",false);
        this.args={};
        var cls=this.constructor.name;
        if(this.__data["class"] ) {
            this.args={class:cls+" "+this.__data["class"],};
        }else{
            this.args={class:cls,};
        }


    }
    //----------------------------------------------------
    draw(div,selected){
    //----------------------------------------------------

        this.html= super.draw(div,selected);
        if( this.active){
            this.activate(this.html,selected);
        }
        return this.html;
    }

    //----------------------------------------------------
    onDraw(div,selected){
    //----------------------------------------------------

      return this.get_html(this.balise,this.args,null,div);

    }
    //----------------------------------------------------

    activate(div,selected){

    //----------------------------------------------------
        //console.log("activate",this.path(),selected);
        this.children.by_class(Do).by_attr_value("event","init").execute("make",{div:div,selected:selected});
        div.on("click",{div:div,selected:selected},this.onClick.bind(this));
        div.on("mouseenter",{div:div,selected:selected},this.onMouseEnter.bind(this));
        div.on("mouseleave",{div:div,selected:selected},this.onMouseLeave.bind(this));
     }
    //----------------------------------------------------

    onClick(event){
    //----------------------------------------------------
        this.msg("ON_CLICK",event.data,5);
        this.executeEvent("click",this.onClickDone,event.data,this.recursive_event);
    }
    //----------------------------------------------------

    onMouseEnter(event){
    //----------------------------------------------------
        this.msg("ON_MOUSE_ENTER",event.data,5);
        this.executeEvent("enter",this.onMouseEnterDone,event.data,this.recursive_event);
    }
    //----------------------------------------------------

    onMouseLeave(event){
    //----------------------------------------------------
        this.msg("ON_MOUSE_LEAVE",event.data,5);
        this.executeEvent("leave",this.onMouseLeaveDone,event.data,this.recursive_event);
    }
    //----------------------------------------------------

    onClickDone(event){}
    onMouseEnterDone(event){}
    onMouseLeaveDone(event){}
    //----------------------------------------------------

}
//========================================================

//========================================================

class Part extends Div {

//========================================================
    //----------------------------------------------------
    onSetup(data){
    //----------------------------------------------------

        super.onSetup(data);

        this.tag=null;
        if(data["name"] ) {
            this.tag=data["name"];
        }

    }
    //----------------------------------------------------
    onDraw(div){
    //----------------------------------------------------

      return this.get_html("div",this.args,this.tag,div);

    }
    //----------------------------------------------------
}
//========================================================
//========================================================

class Text extends SceneElement {

//========================================================
    //----------------------------------------------------
    onSetup(data){
    //----------------------------------------------------

        this.add_variable("text");

    }
    //----------------------------------------------------

    onDraw(div,selected){

    //----------------------------------------------------

        div.append(this.text);
        return div;

    }
    //----------------------------------------------------
}

//========================================================


//========================================================

class ClassAttr extends SceneElement {

//========================================================
    //----------------------------------------------------
    onSetup(data){
    //----------------------------------------------------



    }
    //----------------------------------------------------

    onDraw(div,selected){

    //----------------------------------------------------

        //console.log(selected.path(),this.attr,selected[this.attr]);
        div.append(selected.constructor.name);
        return div;

    }
    //----------------------------------------------------
}

//========================================================


//========================================================

class TextAttr extends SceneElement {

//========================================================
    //----------------------------------------------------
    onSetup(data){
    //----------------------------------------------------

        this.add_variable("attr");

    }
    //----------------------------------------------------

    onDraw(div,selected){

    //----------------------------------------------------

        //console.log(selected.path(),this.attr,selected[this.attr]);
        div.append(selected[this.attr]);
        return div;

    }
    //----------------------------------------------------
}

//========================================================


//========================================================

class Media extends Div {

//========================================================

    //----------------------------------------------------
    onSetup(data){
    //----------------------------------------------------

        super.onSetup(data);
        this.add_variable("url",null);
    }
    //----------------------------------------------------

    onDraw(div,selected){

    //----------------------------------------------------
        div=super.onDraw(div,selected);
        //console.log(this.get_url(selected));
        return this.onDrawElement(div,selected);
   }
    //----------------------------------------------------

    onDrawElement(div,selected){

    //----------------------------------------------------

        return div;
   }

    //----------------------------------------------------
}
//========================================================

class BdImage extends Media {

//========================================================

    //----------------------------------------------------

    onDrawElement(div,selected){

    //----------------------------------------------------

        var img=jQuery('<img/>',{class:"w3-image",src:this.get_url(selected)});
        img.appendTo(div);
        return img;

   }
    //----------------------------------------------------
}
//========================================================

class Video extends Media {

//========================================================

    //----------------------------------------------------

    onDrawElement(div,selected){

    //----------------------------------------------------
        console.log(this.get_url(selected));
        var content=jQuery('<video controls/>',{style:"width:100%;height:auto;"});
        content.appendTo(div);
        var img=jQuery('<source/>',{src:this.get_url(selected)});
        img.appendTo(content);
        return img;

   }
    //----------------------------------------------------
}

//========================================================

class Audio extends Media {

//========================================================

    //----------------------------------------------------

    onDrawElement(div,selected){

    //----------------------------------------------------

        var img=jQuery('<audio controls/>',{src:this.get_url(selected)});
        img.appendTo(div);
        var elt=jQuery('<source/>',{src:this.get_url(selected)});
        elt.appendTo(img);
        return img;

   }
    //----------------------------------------------------
}

//========================================================

class DirectLink extends Media {

//========================================================

    //----------------------------------------------------

    onDrawElement(div,selected){

    //----------------------------------------------------

        //
        var img=jQuery('<a href="'+this.get_url(selected)+'"></a>',{});
        img.appendTo(div);
        return img;

   }
    //----------------------------------------------------
}
//========================================================

class Iframe extends Media {

//========================================================

    //----------------------------------------------------

    onDrawElement(div,selected){

    //----------------------------------------------------

        //
        var img=jQuery('<iframe src="'+this.get_url(selected)+'" style="width:100%;height:80vh;"></iframe>',{});
        img.appendTo(div);
        return img;

   }
    //----------------------------------------------------
}
//========================================================

class HtmlPage extends Media {

//========================================================

    //----------------------------------------------------

    onDrawElement(div,selected){

    //----------------------------------------------------

        var data=jQuery('<div/>');
        data.load(this.get_url(selected),this.onLoadFile.bind(this));
        data.appendTo(div);
        return data;
    }
    //----------------------------------------------------

    onLoadFile(data){

    //----------------------------------------------------



    }
    //----------------------------------------------------

}
//========================================================

class Request extends Media {

//========================================================
    //----------------------------------------------------
    onDraw(div,selected){
    //----------------------------------------------------
        this.div=div;
        new Query(this,this.get_url(selected),this.onFileLoaded.bind(this),"json");
    }
    //----------------------------------------------------
    onFileLoaded(data){
    //----------------------------------------------------

        console.log(data);

    }
    //----------------------------------------------------

}
//========================================================




//========================================================

class Icon extends Div {

//========================================================
    //----------------------------------------------------
    onSetup(data){
    //----------------------------------------------------

        super.onSetup(data);

    }
    //----------------------------------------------------

    onDraw(div,selected){

    //----------------------------------------------------

        var elt= this.get_html("div",this.args,null,div,this.get_text(selected));
        return elt;

    }
    //----------------------------------------------------

    get_text(selected){
    //----------------------------------------------------
        return selected.text;
    }
    //----------------------------------------------------

}
//========================================================

//========================================================

class Icon_Awesome extends Icon {

//========================================================
    //----------------------------------------------------
    onSetup(data){
    //----------------------------------------------------

        super.onSetup(data);
        this.add_variable("icon");
    }
    //----------------------------------------------------

    onDraw(div,selected){

    //----------------------------------------------------

        var elt= this.get_html("div",this.args,null,div,this.get_text(selected));
        return elt;

    }
    //----------------------------------------------------

    get_text(selected){
    //----------------------------------------------------
        return '<i class="fa fa-'+this.icon+'" aria-hidden="true"></i>';
    }
    //----------------------------------------------------
}
//========================================================

//========================================================

class Icon_Text extends Icon {

//========================================================
    //----------------------------------------------------
    onSetup(data){
    //----------------------------------------------------

        super.onSetup(data);
        this.add_variable("text");
    }
    //----------------------------------------------------

    get_text(selected){
    //----------------------------------------------------
        return this.text;
    }
    //----------------------------------------------------
}
//========================================================
//========================================================

class Icon_Image extends Icon {

//========================================================
    //----------------------------------------------------
    onSetup(data){
    //----------------------------------------------------

        super.onSetup(data);
 

        this.src=null;
        if(data["src"] ) {
            this.src=ORIGIN+"/"+data["src"];
        }

    }
    //----------------------------------------------------

    onDraw(div,selected){

    //----------------------------------------------------

        var img=jQuery('<img/>',{src:this.src});
        img.appendTo(parent);
        return img;

   }

    //----------------------------------------------------
}

//========================================================

//========================================================

class Layers extends Div {

//========================================================
    //----------------------------------------------------
    onSetup(){
    //----------------------------------------------------

        super.onSetup();
        this.args["class"]=this.args["class"]+" w3-container";

    }
    //----------------------------------------------------

    onDraw(div,selected){

    //----------------------------------------------------

        var elt= this.get_html("div",this.args,null,div);
        elt.css("width","100%");
        elt= this.get_html("div",{class:"w3-display-container"},null,elt);//
        elt.css("width","100%");
        return elt;

    }
   //----------------------------------------------------

    activate(div,selected){

    //----------------------------------------------------

        var lst=this.children.by_class(Layer).iter();
        this.msg("LAYER_ACTIVATE",lst,5);
         for(var i in lst ) {
            lst[i].children.by_class(Do).by_attr_value("event","init").execute("make",{div:lst[i].obj,selected:selected});
        }

        div.on("click",{div:div,selected:selected},this.onClick.bind(this));
        div.on("mouseenter",{div:div,selected:selected},this.onMouseEnter.bind(this));
        div.on("mouseleave",{div:div,selected:selected},this.onMouseLeave.bind(this));
     }
    //----------------------------------------------------

    onClick(event){
    //----------------------------------------------------
        this.msg("LAYER_CLICK",event.data,5);

        var lst=this.children.by_class(Layer).iter();
         for(var i in lst ) {
            lst[i].executeEvent("click",this.onClickDone, {div:lst[i].obj,selected:event.data.selected} );
        }

    }
    //----------------------------------------------------

    onMouseEnter(event){
    //----------------------------------------------------
        this.msg("LAYER_ENTER",event.data,5);
        var lst=this.children.by_class(Layer).iter();
         for(var i in lst ) {
            lst[i].executeEvent("enter",this.onClickDone, {div:lst[i].obj,selected:event.data.selected} );
        }
    }
    //----------------------------------------------------

    onMouseLeave(event){
    //----------------------------------------------------
        this.msg("LAYER_LEAVE",event.data,5);
        var lst=this.children.by_class(Layer).iter();
         for(var i in lst ) {
            lst[i].executeEvent("leave",this.onClickDone, {div:lst[i].obj,selected:event.data.selected} );
        }
    }
    //----------------------------------------------------



}
//========================================================

//========================================================

class Layer extends Div {

//========================================================
    //----------------------------------------------------
    onSetup(){
    //----------------------------------------------------

        super.onSetup();
        this.add_variable("url",null);
        this.args["class"]=this.args["class"]+" w3-display-topleft";//fullsize

    }
    //----------------------------------------------------

    onDraw(div,selected){

    //----------------------------------------------------
        this.msg("LAYER_SELECT",selected,5);

        var url;
        if("url" in this){
            url=this.url;

        }else{
            url=selected.url;
        }
        this.obj=this.get_html("div",this.args,null,div);
        var img=jQuery('<img/>',{class:"w3-image",src:url});
        img.appendTo(this.obj);
        //img.css("position","absolute");
        this.obj.css("width","100%");
        //img.css("top","0");
        //img.css("left","0");
        //img.css("float","right");
        return this.obj;

   }

    //----------------------------------------------------

    onDrawElementXX(div,selected){

    //----------------------------------------------------
        this.obj=super.onDrawElement(div,selected);
        img=jQuery('<img/>',{class:"w3-image",src:this.get_url(selected)});
        img.appendTo(this.obj);
        return this.obj;

   }
    //----------------------------------------------------

}
//========================================================


//========================================================

class Slideshow extends Div {

//========================================================
    //----------------------------------------------------
    onSetup(data){
    //----------------------------------------------------

        super.onSetup(data);
        this.add_variable("t",2000);
        this.add_variable("cls");
    }
    //----------------------------------------------------

    onDrawDone(div){

    //----------------------------------------------------

        this.object=w3.slideshow(this.cls,this.t);


    }
    //----------------------------------------------------

}
//========================================================



//========================================================

class Curve extends Div {

//========================================================
    //----------------------------------------------------
    onSetup(data){
    //----------------------------------------------------

        super.onSetup(data);
        this.add_variable("format");
    }
    //----------------------------------------------------

    onDraw(div,selected){

    //----------------------------------------------------

                console.log(selected.text);
        if (this.format=="json"){
            var items=JSON.parse (selected.text);

        }else if (this.format=="csv"){
            var items=[];
            var value;
            var lst=selected.text.split(";");
            for (var i in lst){
                if(lst[i]!=""){
                    value=lst[i].split(",");
                    console.log(value);
                    items.push({ x: parseFloat(value[0]), y:parseFloat(value[1]) });
                }
            }
        }

        var dataset = new vis.DataSet(items);
        var options = {
        sort: false,
        sampling:false
        };

//        style:'points'
        this.graph2d = new vis.Graph2d(div[0], dataset, options);

    }
    //----------------------------------------------------

}
//========================================================






//========================================================

class Curve3d extends Div {

//========================================================
    //----------------------------------------------------
    onSetup(data){
    //----------------------------------------------------

        super.onSetup(data);
        this.add_variable("format");
    }
    //----------------------------------------------------

    onDraw(div,selected){

    //----------------------------------------------------

      var data = null;


    function custom(x, y, t) {
      return Math.sin(x/50 + t/10) * Math.cos(y/50 + t/10) * 50 + 50;
    }


      data = new vis.DataSet();

      var steps = 25;
      var axisMax = 314;
      var tMax = 31;
      var axisStep = axisMax / steps;

      for (var t = 0; t < tMax; t++) {
        for (var x = 0; x < axisMax; x+=axisStep) {
          for (var y = 0; y < axisMax; y+=axisStep) {
            var value = custom(x, y, t);
            data.add([
              {x:x,y:y,z:value,filter:t,style:value}
            ]);
          }
        }
      }

      // specify options
      var options = {
        width:  '600px',
        height: '600px',
        style: 'surface',
        showPerspective: true,
        showGrid: true,
        showShadow: false,
        // showAnimationControls: false,
        keepAspectRatio: true,
        verticalRatio: 0.5,
        animationInterval: 100, // milliseconds
        animationPreload: true
      };

//        style:'points'
        this.graph2d = new vis.Graph3d(div[0], data, options);

    }
    //----------------------------------------------------

}
//========================================================




//========================================================

class Controller extends Bd_Object {

//========================================================

    //----------------------------------------------------
    connect(){
    //----------------------------------------------------

        return true;

    }

    //----------------------------------------------------
    disconnect(){
    //----------------------------------------------------

        return true;

    }
    //----------------------------------------------------
}
//========================================================

class ControllerPart extends Bd_Object {

//========================================================


    //----------------------------------------------------
    onReceiveEvent(event){
    //----------------------------------------------------

        return true;

    }
    //----------------------------------------------------
}
//========================================================


//========================================================

class Keyboard extends Controller {

//========================================================

    //----------------------------------------------------
    connect(){
    //----------------------------------------------------

        document.addEventListener('keydown', this.onKeyPressed.bind(this));
        document.addEventListener('keyup', this.onKeyReleased.bind(this));

    }

    //----------------------------------------------------
    disconnect(){
    //----------------------------------------------------

        return true;

    }
    //----------------------------------------------------
    onKeyPressed(event){
    //----------------------------------------------------
        this.children.by_class(Key).execute("onPressed",event);
    }

    //----------------------------------------------------
    onKeyReleased(event){
    //----------------------------------------------------
        this.children.by_class(Key).execute("onRelease",event);
    }
    //----------------------------------------------------
}
//========================================================

class Key extends ControllerPart {

//========================================================

    //----------------------------------------------------
    onSetup(data){
    //----------------------------------------------------

        super.onSetup(data);
        this.add_variable("text",null);

    }
    //----------------------------------------------------
    onTest(event){
    //----------------------------------------------------

        if ( event.code == this.text){
            return true;
        }else{
            return false;
        }

    }
    //----------------------------------------------------
    onPressed(event){
    //----------------------------------------------------

          if ( this.onTest(event)==true){

            
            }

    }
    //----------------------------------------------------
    onRelease(event){
    //----------------------------------------------------

          if ( this.onTest(event)==true){

            
            }

    }
    //----------------------------------------------------
}
//========================================================


//========================================================

class Mouse extends Controller {

//========================================================

    //----------------------------------------------------
    connect(){
    //----------------------------------------------------
document.addEventListener('', );
document.addEventListener('', );
document.addEventListener('', mouseWheel);

        document.addEventListener('mousedown', this.onMousePressed.bind(this));
        document.addEventListener('mouseup', this.onMouseReleased.bind(this));
        document.addEventListener('wheel', this.onWheel.bind(this));

    }

    //----------------------------------------------------
    disconnect(){
    //----------------------------------------------------

        return true;

    }
    //----------------------------------------------------
    onMousePressed(event){
    //----------------------------------------------------
        this.children.by_class(MouseButton).execute("onPressed",event);
    }

    //----------------------------------------------------
    onMouseReleased(event){
    //----------------------------------------------------
        this.children.by_class(MouseButton).execute("onRelease",event);
    }
    //----------------------------------------------------
    onWheel(event){
    //----------------------------------------------------
        this.children.by_class(MouseWheel).execute("onWheel",event);
    }
    //----------------------------------------------------
}
//========================================================

class MouseButton extends ControllerPart {

//========================================================

    //----------------------------------------------------
    onSetup(data){
    //----------------------------------------------------

        super.onSetup(data);
        this.add_variable("text",null);

    }
    //----------------------------------------------------
    onTest(event){
    //----------------------------------------------------

        if ( event.button == this.text){
            return true;
        }else{
            return false;
        }

    }
    //----------------------------------------------------
    onPressed(event){
    //----------------------------------------------------

          if ( this.onTest(event)==true){

            
            }

    }
    //----------------------------------------------------
    onRelease(event){
    //----------------------------------------------------

          if ( this.onTest(event)==true){

            
            }

    }
    //----------------------------------------------------
}
//========================================================

class MouseWheel extends ControllerPart {

//========================================================

    //----------------------------------------------------
    onSetup(data){
    //----------------------------------------------------

        super.onSetup(data);
        //this.add_variable("text",null);

    }

    //----------------------------------------------------
    onWheel(event){
    //----------------------------------------------------

         if(event.deltaY>0){

        }else{

        }

    }
    //----------------------------------------------------

}

//========================================================


//========================================================

class PlayerSequence{

//========================================================
    //----------------------------------------------------
    constructor(){
    //----------------------------------------------------
        this.sequence=[];
        this.i=-1;
        this.makelist();
    }
    //----------------------------------------------------
    makelist(){
    //----------------------------------------------------
        return;
    }
    //----------------------------------------------------
    getNode(){
    //----------------------------------------------------
        return this.sequence[this.i];
    }
    //----------------------------------------------------
    addNode(node){
    //----------------------------------------------------
        this.sequence.push(node);
    }
    //----------------------------------------------------
    reset(){
    //----------------------------------------------------

        this.i=-1;

    }

    //----------------------------------------------------
    next(n=1){
    //----------------------------------------------------
        this.i+=n;
        if(this.i>=this.sequence.length){this.i=0;}
        return this.getNode();

    }
    //----------------------------------------------------
    previous(n=1){
    //----------------------------------------------------
        this.i-=n;
        if(this.i<0){this.i=this.sequence.length-1;}
        return this.getNode();
    }
    //----------------------------------------------------

}

//========================================================

class PlayerHandler extends Bd_Object {

//========================================================
    //----------------------------------------------------
    onSetup(data){
    //----------------------------------------------------
        super.onSetup(data);
        this.add_variable("attr");
        this.add_variable("value");
        this.add_variable("scene");
        this.add_variable("tag");

    }
    //----------------------------------------------------
    test(node){
    //----------------------------------------------------

        if(node[this.attr]==this.value){
            return true;
        }
        return false;
    }
    //----------------------------------------------------
    process(node){
    //----------------------------------------------------

        var scene = this.search_path(this.scene);
        $("#"+this.tag).empty();
        scene.draw($("#"+this.tag),node);
    }
    //----------------------------------------------------

}
//========================================================

class Player extends Bd_Object {

//========================================================
    //----------------------------------------------------
    onSetup(data){
    //----------------------------------------------------
        super.onSetup(data);
        this.add_variable("t_loop",8000);
        this.add_variable("select");
        this.sequence=new PlayerSequence();
        this.selectors=this.children.by_class(PlayerHandler).iter();


        this.current_node=null;
        this.paused=true;


    }
    //----------------------------------------------------
    post_setup(){
    //----------------------------------------------------
        var node;
        var select=this.search_path(this.select);

        if(! select ){
            console.log("select not exists",this.select);
            return;
        }
        var elements=select.all().iter();
        for(var i = 0, size = elements.length; i < size ; i++){
             node=elements[i];
             if (this.get_selector(node)!=false ) {
                console.log(node.path());
                this.sequence.addNode(node);
            }
        }

        console.log(this.sequence.sequence.length);
    }
    //----------------------------------------------------
    get_selector(node){
    //----------------------------------------------------

        for(var i = 0, size = this.selectors.length; i < size ; i++){
             if (this.selectors[i].test(node) == true ) {
                return this.selectors[i];
            }
        }
        return false;

    }
    //----------------------------------------------------
    process(node){
    //----------------------------------------------------
        console.log(this.sequence.i,node.path());
        this.current_node=node;
        var selector=this.get_selector(node);
        if(selector){
            selector.process(node);
        }
    }

    //----------------------------------------------------
    reset(){
    //----------------------------------------------------
        this.sequence.reset();
        this.next();
    }
    //----------------------------------------------------
    start(){
    //----------------------------------------------------

        if(this.paused){
            this.paused=false;
            this.loop();
        }
    }
    //----------------------------------------------------
    stop(){
    //----------------------------------------------------

        this.paused=true;
    }
    //----------------------------------------------------
    loop(){
    //----------------------------------------------------

        if(this.paused==false){
            this.next();
            setTimeout(this.loop.bind(this), this.t_loop);
        }
    }
    //----------------------------------------------------
    next(n=1){
    //----------------------------------------------------
        this.process(this.sequence.next(n));

    }
    //----------------------------------------------------
    previous(n=1){
    //----------------------------------------------------
        this.process(this.sequence.previous(n));
    }
    //----------------------------------------------------

}
//========================================================

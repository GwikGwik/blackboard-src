import xmlscript,os
from dataModel import *
from mod_program import *

#=========================================================

class Project_DocumentManager(Object):

#=========================================================

    def onSetup(self):
        self.open()

    def open(self):

        for name in os.listdir(self.file_path): 
            if name.endswith(".xml"): 
                self.create(name)

    def close(self):

        for f in self.children.by_class(Project_Document):
            f.save()
            f.close()

    def save(self):

        for f in self.children.by_class(Project_Document):
            f.save()


    def create(self,name):

        obj=self.find(name[:-4])
        if obj is None:

            obj=Project_Document(parent=self,name=name[:-4],file_path=os.path.join(self.file_path,name))
            obj.setup()
            return obj
        else:
            print( "file already exists : "+name )




#=========================================================

class Project_Document(Object):

#=========================================================

    def onSetup(self):

        self.is_opened=False
        #self.tree = None
        #self.root = None
        self.open()

    def create(self):
        pass

    def open(self):
        self.is_opened=True
        node=xmlscript.tree_from_file(self.file_path,name="data")
        for elt in node.children:
            elt.parent=self 

    def save(self):
        if self.is_opened == True:
            self.tree.write(self.path, xml_declaration=True, encoding='utf-8')

    def close(self):
        self.is_opened=False
        self.tree = None
        self.root = None

    def remove(self):
        self.cleanup()
        self.destroy()
        #os.remove(self.path) 



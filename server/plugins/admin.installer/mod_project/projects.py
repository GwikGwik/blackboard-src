from dataModel import *
from mod_program import Object
from atom import Atom_Link
import os,xmlscript

class Project_IsInstance(Atom_Link):pass
class Project_hasPlugin(Atom_Link):pass

#=======================================================================

class Project_Package(Object):

#=======================================================================

    def onSetup(self):
        for elt in self.inputs.by_class(Project_IsInstance):
            if elt.source.active != True:
                self.active=False

        for elt in self.inputs.by_class(Project_IsInstance):
            elt.source.build_package(self)
    

    def ping(self):
        return False

#=======================================================================

class Project_Configuration(Project_Package):

    IP=String()

    def ping(self):
        return False

class Project_Server(Project_Package):

    IP=String()

    def ping(self):
        return False

class Project_Service(Project_Package):

    def ping(self):
        return False

class Project_Port(Project_Package):

    def ping(self):
        return False

class Project_Device(Project_Package):

    def ping(self):
        return False

class Project_Place(Project_Package):

    def ping(self):
        return False

class Project_User(Project_Package):

    def ping(self):
        return False

class Project_Group(Project_Package):

    def ping(self):
        return False

#=======================================================================

class Project_Resource(Object):

#=======================================================================

    def onSetup(self):
        pass

    def build_package(self,pkg):
        pass
#=======================================================================

class Project_Protocole(Object):

#=======================================================================

    def onSetup(self):
        pass


    def build_package(self,elt):
        if "source" in elt.keys():
            path=elt.source.split("://")[1]
            node=self.find(path,error=False)
            if node is None:
                node=self.append(path,cls=Project_Resource)
            Project_IsInstance(name="is_instance",source=node,target=elt)

#=======================================================================

class Project_FileResource(Project_Resource):

#=======================================================================

    def onSetup(self):
        if "file_path" in self.keys():
            if os.path.exists(self.file_path)!=True:
                self.active=False

    def has_element(self,name):
        if self.active==True and "file_path" in self.keys():
            if os.path.exists(self.file_path+"/"+name):
                return self.file_path+"/"+name

    def build_package(self,pkg):
        if self.has_element("__instance__.xml"):
            print("__instance__",pkg.path())
            source=xmlscript.tree_from_file(self.has_element("__instance__.xml"))
            for elt in source.children:
                elt.parent=pkg

#=======================================================================

class Project_FileProtocole(Project_Protocole):

#=======================================================================

    def onSetup(self):
        if "source" in self.keys():
            if os.path.exists(self.source)==True:
                self.build_sources(self,self.source)

    def build_sources(self,node,path):
        for elt in os.listdir(path):
            elt_path=os.path.join(path,elt)
            if os.path.isdir(elt_path) and not elt.startswith("__") and not elt.startswith("."):
                elt_node=Project_FileResource(parent=node,name=elt,file_path=elt_path)
                self.build_sources(elt_node,elt_path)

    def build_package(self,elt):
        if "source" in elt.keys():
            path=elt.source.split("://")[1]
            node=self.find(path)
            if node is None:
                elt.active=False
                elt_path=os.path.join(self.source,path)
                node=self.append(path,cls=Project_FileResource,file_path=elt_path)
                node.setup()
            if node.active !=True:
                 elt.active=False
            Project_IsInstance(name="is_instance",source=node,target=elt)
            node.build_package(elt)




#=======================================================================

class Project_Manager(Object):

#=======================================================================

    def build(self):
        print("ok")

#=======================================================================

class Project_PluginManager(Project_Manager):

#=======================================================================

    def build(self):
        for elt in self.root.all().by_class(Project_FileResource):
            if elt.has_element("__plugin__.xml"):
                #print("plugins",elt.path())
                Project_hasPlugin(source=self,target=elt)
#=======================================================================

class Project_MainManager(Object):

#=======================================================================

    def onPostSetup(self):
        self.protocoles=dict()
        for elt in self.all().by_class(Project_Protocole):
            self.protocoles[elt.name]=elt

        for elt in self.get_firsts(cls=Project_Package):
            self.link_package(elt)

        for elt in self.root.all().by_class(Project_Manager):
            elt.build()

    def link_package(self,elt):
        if "source" in elt.keys():
            protocole=elt.source.split("://")[0]
            protocole = self.protocoles[protocole]
            protocole.build_package(elt)

        for elt2 in elt.get_firsts(cls=Project_Package,root=False):
            self.link_package(elt2)

#=======================================================================


import sqlite3
from flask import Flask, render_template, request, url_for, flash, redirect
from werkzeug.exceptions import abort

import xmlscript,os
from mod_project import *

DEBUG=False
UPLOAD_FOLDER = '/path/to/the/uploads'
ALLOWED_EXTENSIONS = {'txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'}
filename="./app.xml"

xmlscript.import_all(os.environ["HOME"],DEBUG=DEBUG)
#source=Source("./data")
#source.open()
source=xmlscript.tree_from_file(filename)
source.setup()
app = Flask(__name__)
app.config['SECRET_KEY'] = 'your secret key'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

#---------------------------------------------------------
@app.route('/')
def index():
#---------------------------------------------------------
    data = request.args
    if 'template' in data:
        template = data['template'].strip()
    else:
        template='index.html'
    return render_template(template, node=source)

#---------------------------------------------------------
@app.route('/save', methods=('POST',))
def save():
#---------------------------------------------------------

    source.save()
    return redirect(url_for('index'))

#---------------------------------------------------------
@app.route('/close', methods=('POST',))
def close():
#---------------------------------------------------------

    source.close()
    return redirect(url_for('index'))


#---------------------------------------------------------
@app.route('/create', methods=('GET', 'POST'))
def create():
#---------------------------------------------------------

    if request.method == 'POST':
        content = request.form['content']

        if not content:
            flash('Title is required!')
        else:
            node = source.create(content)
            return redirect(url_for('index'))

    return render_template('create.html')

#---------------------------------------------------------
@app.route('/<path:path>')
def read(path):
#---------------------------------------------------------
    data = request.args
    if 'template' in data:
        template = data['template'].strip()
    else:
        template='tree/list.html'

    node = source.find(path)
    if node is not None:
        return render_template(template, node=node,template=template)
    else:
        abort(404)

#---------------------------------------------------------
@app.route('/<path:path>/edit', methods=('GET', 'POST'))
def edit(path):
#---------------------------------------------------------

    node = source.find(path)

    if request.method == 'POST':
        content = request.form['content']

        if not content:
            flash('Title is required!')
        else:
            conn = get_db_connection()
            conn.execute('UPDATE posts SET title = ?, content = ?'
                         ' WHERE id = ?',
                         (title, content, id))
            conn.commit()
            conn.close()
            return redirect(url_for('index'))

    return render_template('edit.html', node=node)

#---------------------------------------------------------
@app.route('/<path:path>/delete', methods=('POST',))
def delete(path):
#---------------------------------------------------------

    obj=source.find(name)
    if obj is not None:
        obj.remove() 
        return redirect(url_for('index'))
    else:
        abort(404)

#---------------------------------------------------------

@app.route('/download_form')
def download_form():
	return render_template('download.html')
#---------------------------------------------------------
@app.route('/download')
def download_file():
	#path = "html2pdf.pdf"
	#path = "info.xlsx"
	path = "simple.docx"
	#path = "sample.txt"
	return send_file(path, as_attachment=True)
#---------------------------------------------------------
def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

#---------------------------------------------------------
@app.route('/upload_file', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            return redirect(url_for('uploaded_file',
                                    filename=filename))
    return '''
    <!doctype html>
    <title>Upload new File</title>
    <h1>Upload new File</h1>
    <form method=post enctype=multipart/form-data>
      <input type=file name=file>
      <input type=submit value=Upload>
    </form>
    '''

#---------------------------------------------------------

